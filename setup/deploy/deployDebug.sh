#!/bin/sh

echo "Building and installing SnailLife"
cd ../../cmd/snaillifecli;
go build
GOBIN=$GOPATH/bin/snaillifecli go install
cd ../../
cp -R config $GOPATH/bin/snaillifecli