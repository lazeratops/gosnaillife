package tests

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	"gitlab.com/drakonka/gosnaillife/common/domain/world"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/tests"
	"os"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	ret := 1
	defer func() {
		os.Exit(ret)
	}()
	defer testing2.TimeElapsed("World Test", testing2.ModerateThreshold)()
	ret = m.Run()
}

func TestGetAllLivingSnails(tT *testing.T) {
	testCases := []struct {
		name      string
		seedFunc  func() (*tests.TestUtil, databases.Database, error)
		wantCount int
		wantErr   error
	}{
		{
			name:      "ReturnOneLiving",
			seedFunc:  seedOneLivingSnail,
			wantCount: 1,
			wantErr:   nil,
		},
		{
			name:      "ReturnNoLiving",
			seedFunc:  seedNoLivingSnails,
			wantCount: 0,
			wantErr:   nil,
		},
		{
			name:      "ReturnThreeLiving",
			seedFunc:  seedThreeLivingSnails,
			wantCount: 3,
			wantErr:   nil,
		},
	}

	for _, tc := range testCases {
		tT.Run(tc.name, func(tT *testing.T) {
			app := env.NewApplication()
			tu, db, err := tc.seedFunc()
			app.Database = db
			defer tu.DeleteAllTestDBs()
			if err != nil {
				tT.Fatal(testing2.TCFailedMsg("WorldTest", tc.name, fmt.Sprintf("failed seed func: %v", err)))
				return
			}
			repo, err := snail.NewRepo(db)
			if err != nil {
				tT.Fatal(testing2.TCFailedMsg("WorldTest", tc.name, fmt.Sprintf("failed getting repo: %v", err)))
				return
			}
			sr := repo.(*snail.Repo)
			livingSnails, err := sr.GetAllLivingSnails()
			testErr := testing2.TestErrorType(tc.wantErr, err)
			if testErr != nil {
				tT.Errorf("Test case %s failed: %v", tc.name, testErr)
				return
			}
			if len(livingSnails) != tc.wantCount {
				tT.Error(testing2.TCFailedMsg("WorldTest", tc.name, fmt.Sprintf("expected %d results, got %d", tc.wantCount, len(livingSnails))))
				return
			}
		})
	}
}

func seedOneLivingSnail() (*tests.TestUtil, databases.Database, error) {
	tu := tests.NewTestUtil()
	db, err := tu.CreateTestDB("world_test")
	if err != nil {
		return nil, nil, err
	}
	err = tu.RunDbMigrations(false, "")
	if err != nil {
		return tu, db, err
	}
	err = tu.Seed("world_test", "snails", []map[string]string{
		{
			"name":       "Dead Snail",
			"death_date": time.Now().UTC().Format("2006-01-02 03:04:05"),
		},
		{
			"name": "Alive Snail",
		},
	})
	return tu, db, err
}

func seedNoLivingSnails() (*tests.TestUtil, databases.Database, error) {
	tu := tests.NewTestUtil()
	db, err := tu.CreateTestDB("world_test")
	if err != nil {
		return nil, nil, err
	}
	err = tu.RunDbMigrations(false, "")
	if err != nil {
		return tu, db, err
	}
	err = tu.Seed("world_test", "snails", []map[string]string{
		{
			"name":       "Dead Snail",
			"death_date": time.Now().UTC().Format("2006-01-02 03:04:05"),
		},
	})
	return tu, db, err
}

func seedThreeLivingSnails() (*tests.TestUtil, databases.Database, error) {
	tu := tests.NewTestUtil()
	db, err := tu.CreateTestDB("world_test")
	if err != nil {
		return nil, nil, err
	}
	err = tu.RunDbMigrations(false, "")
	if err != nil {
		return tu, db, err
	}
	err = tu.Seed("world_test", "snails", []map[string]string{
		{
			"name":       "Dead Snail",
			"death_date": time.Now().UTC().Format("2006-01-02 03:04:05"),
		},
		{
			"name": "Alive Snail 0",
		},
		{
			"name": "Alive Snail 1",
		},
		{
			"name": "Alive Snail 2",
		},
	})
	return tu, db, err
}

func TestStartWorld(tT *testing.T) {
	testCases := []struct {
		name               string
		seedFunc           func() (*tests.TestUtil, databases.Database, error)
		makeWorldFunc      func(quitChan chan struct{}) (*world.World, error)
		pauseSeconds       int
		runDurationSeconds int
		wantErr            error
	}{
		{
			name:     "BadWorldInit",
			seedFunc: seedOneLivingSnail,
			wantErr:  errors.New("This world does not have a quit channel. Initiate the world with NewWorld and give it a quit channel."),
		},
		{
			name:               "StartAndPause",
			seedFunc:           seedOneLivingSnail,
			makeWorldFunc:      world.NewWorld,
			pauseSeconds:       5,
			runDurationSeconds: 10,
			wantErr:            nil,
		},
	}

	for _, tc := range testCases {
		tT.Run(tc.name, func(tT *testing.T) {
			app := env.NewApplication()
			tu, db, err := tc.seedFunc()
			app.Database = db
			defer tu.DeleteAllTestDBs()
			if err != nil {
				tT.Fatal(testing2.TCFailedMsg("WorldTest", tc.name, fmt.Sprintf("failed seed func: %v", err)))
				return
			}
			var w *world.World
			quitChan := make(chan struct{})
			if tc.makeWorldFunc != nil {
				w, err = tc.makeWorldFunc(quitChan)
				if err != nil {
					tT.Fatal(testing2.TCFailedMsg("WorldTest", tc.name, fmt.Sprintf("failed make world func: %v", err)))
					return
				}
			} else {
				w = &world.World{}
			}
			err = w.Start(app)
			if err == nil {
				if tc.pauseSeconds > 0 {
					w.Pause()
					time.Sleep(time.Duration(tc.pauseSeconds) * time.Second)
					w.Resume()
				}
				time.Sleep(time.Duration(tc.runDurationSeconds) * time.Second)
			}
			close(quitChan)
			testErr := testing2.TestErrorType(tc.wantErr, err)
			if testErr != nil {
				tT.Errorf("Test case %s failed: %v", tc.name, testErr)
			}

		})
	}
}
