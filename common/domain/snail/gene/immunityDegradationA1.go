package gene

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"reflect"
)

type ImmunityDegradationA1 struct {
	organism.GeneBase
}

func (g *ImmunityDegradationA1) Init() error {
	g.Id = "immunity_degradation_a1"
	g.DominanceType = organism.Complete

	var e *ImmunityDegradationA1Expresser
	g.Expresser = reflect.TypeOf(e).Elem()
	g.PossibleAlleles = g.getVariations()
	return nil
}

func (g *ImmunityDegradationA1) GetGeneId() string {
	return g.Id
}

func (g *ImmunityDegradationA1) getVariations() organism.Alleles {
	// Variations are "Suppressed", "Standard", and "Enhanced"

	// Standard
	standardA := Allele{
		GeneId: g.Id,
		Key:    "S",
	}
	standardA.Modifier = 1.00
	standardA.DominanceLevel = 2

	suppressedA := Allele{
		GeneId: g.Id,
		Key:    "s",
	}
	suppressedA.Modifier = -2.00
	suppressedA.DominanceLevel = 1

	enhancedA := Allele{
		GeneId: g.Id,
		Key:    "e",
	}
	enhancedA.Modifier = -0.5
	enhancedA.DominanceLevel = 0
	return organism.Alleles{&standardA, &suppressedA, &enhancedA}
}

type ImmunityDegradationA1Expresser interface {
	ExpressImmunityDegradationA1(o *organ.Organ) error
}
