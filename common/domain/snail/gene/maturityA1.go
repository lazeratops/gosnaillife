package gene

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"reflect"
)

type MaturityA1 struct {
	organism.GeneBase
}

func (g *MaturityA1) Init() error {
	g.Id = "size_a1"
	g.DominanceType = organism.Co

	var e *MaturityA1Expresser
	g.Expresser = reflect.TypeOf(e).Elem()
	g.PossibleAlleles = g.getVariations()
	return nil
}

func (g *MaturityA1) GetGeneId() string {
	return g.Id
}

func (g *MaturityA1) getVariations() organism.Alleles {
	// Variations are "Big" and "Small"
	bigA := Allele{
		GeneId: g.Id,
		Key:    "M",
	}
	bigA.Modifier = 1.00
	bigA.DominanceLevel = 1

	smallA := Allele{
		GeneId: g.Id,
		Key:    "m",
	}
	smallA.Modifier = 0.05
	smallA.DominanceLevel = 0

	return organism.Alleles{&bigA, &smallA}
}

type MaturityA1Expresser interface {
	ExpressMaturityA1(o *organ.Organ) error
}
