package organ

import error2 "gitlab.com/drakonka/gosnaillife/common/error"

type kidney struct {
	SnailBaseOrganKind
}

func NewKidney() Organ {
	t := kidney{}
	err := t.Init()
	if err != nil {
		error2.HandleErr(err, "NewEye")
	}
	o := newOrgan(&t)
	return o
}

func (t *kidney) Init() error {
	t.IdealQuant = 1
	t.Id = "kidney"
	return nil
}
