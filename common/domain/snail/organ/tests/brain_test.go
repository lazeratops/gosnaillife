package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type brainTest struct {
}

func newBrainTest() organTest {
	test := brainTest{}
	return &test
}

func (t *brainTest) prep() error {
	return nil
}

func (t *brainTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Brain",
			f:    organ.NewBrain,
			want: nil,
		},
	}
}

func (t *brainTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
