package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type tentacleTest struct {
}

func newTentacleTest() organTest {
	test := tentacleTest{}
	return &test
}

func (t *tentacleTest) prep() error {
	return nil
}

func (t *tentacleTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Tentacle",
			f:    organ.NewTentacle,
			want: nil,
		},
	}
}

func (t *tentacleTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
