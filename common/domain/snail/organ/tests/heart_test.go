package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type heartTest struct {
}

func newHeartTest() organTest {
	test := heartTest{}
	return &test
}

func (t *heartTest) prep() error {
	return nil
}

func (t *heartTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Heart",
			f:    organ.NewHeart,
			want: nil,
		},
	}
}

func (t *heartTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
