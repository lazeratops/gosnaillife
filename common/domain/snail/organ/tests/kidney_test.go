package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type kidneyTest struct {
}

func newKidneyTest() organTest {
	test := kidneyTest{}
	return &test
}

func (t *kidneyTest) prep() error {
	return nil
}

func (t *kidneyTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Kidney",
			f:    organ.NewKidney,
			want: nil,
		},
	}
}

func (t *kidneyTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
