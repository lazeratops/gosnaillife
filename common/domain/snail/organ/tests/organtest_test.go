package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type organTest interface {
	prep() error
	getNewOrganTest() []newOrganTestCase
	getAdditionalTests() []func(t *testing.T)
}

type newOrganTestCase struct {
	name string
	f    func() organ.Organ
	want error
}
