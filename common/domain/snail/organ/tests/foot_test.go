package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type footTest struct {
}

func newFootTest() organTest {
	test := footTest{}
	return &test
}

func (t *footTest) prep() error {
	return nil
}

func (t *footTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Foot",
			f:    organ.NewFoot,
			want: nil,
		},
	}
}

func (t *footTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
