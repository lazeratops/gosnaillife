package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type dartTest struct {
}

func newDartTest() organTest {
	test := dartTest{}
	return &test
}

func (t *dartTest) prep() error {
	return nil
}

func (t *dartTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Dart",
			f:    organ.NewDart,
			want: nil,
		},
	}
}

func (t *dartTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
