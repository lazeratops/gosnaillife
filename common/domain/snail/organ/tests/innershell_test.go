package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type innerShellTest struct {
}

func newInnerShellTest() organTest {
	test := innerShellTest{}
	return &test
}

func (t *innerShellTest) prep() error {
	return nil
}

func (t *innerShellTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "InnerShell",
			f:    organ.NewInnerShell,
			want: nil,
		},
	}
}

func (t *innerShellTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
