package organ

import error2 "gitlab.com/drakonka/gosnaillife/common/error"

type mucusgland struct {
	SnailBaseOrganKind
}

func NewMucusGland() Organ {
	t := mucusgland{}
	err := t.Init()
	if err != nil {
		error2.HandleErr(err, "NewEye")
	}
	o := newOrgan(&t)
	return o
}

func (t *mucusgland) Init() error {
	t.IdealQuant = 1
	t.Id = "mucus_gland"
	return nil
}
