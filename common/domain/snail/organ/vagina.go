package organ

type vagina struct {
	SnailBaseOrganKind
}

func NewVagina() Organ {
	t := vagina{}
	t.Init()
	o := newOrgan(&t)
	return o
}

func (t *vagina) Init() error {
	t.IdealQuant = 1
	t.Id = "vagina"
	return nil
}
