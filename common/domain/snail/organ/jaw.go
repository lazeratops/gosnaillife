package organ

import error2 "gitlab.com/drakonka/gosnaillife/common/error"

type jaw struct {
	SnailBaseOrganKind
}

func NewJaw() Organ {
	t := jaw{}
	err := t.Init()
	if err != nil {
		error2.HandleErr(err, "NewEye")
	}
	o := newOrgan(&t)
	return o
}

func (t *jaw) Init() error {
	t.IdealQuant = 1
	t.Id = "jaw"
	return nil
}
