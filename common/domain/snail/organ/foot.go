package organ

import error2 "gitlab.com/drakonka/gosnaillife/common/error"

type foot struct {
	SnailBaseOrganKind
}

func NewFoot() Organ {
	t := foot{}
	err := t.Init()
	if err != nil {
		error2.HandleErr(err, "NewEye")
	}
	o := newOrgan(&t)
	return o
}

func (t *foot) Init() error {
	t.IdealQuant = 1
	t.Id = "foot"
	return nil
}
