package organ

import error2 "gitlab.com/drakonka/gosnaillife/common/error"

type brain struct {
	SnailBaseOrganKind
}

func NewBrain() Organ {
	t := brain{}
	err := t.Init()
	if err != nil {
		error2.HandleErr(err, "NewBrain")
	}
	o := newOrgan(&t)
	return o
}

func (t *brain) Init() error {
	t.IdealQuant = 1
	t.Id = "brain"
	return nil
}
