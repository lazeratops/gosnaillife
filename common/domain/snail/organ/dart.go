package organ

import error2 "gitlab.com/drakonka/gosnaillife/common/error"

type dart struct {
	SnailBaseOrganKind
}

func NewDart() Organ {
	t := dart{}
	err := t.Init()
	if err != nil {
		error2.HandleErr(err, "NewDart")
	}
	o := newOrgan(&t)
	return o
}

func (t *dart) Init() error {
	t.IdealQuant = 1
	t.Id = "dart"
	return nil
}
