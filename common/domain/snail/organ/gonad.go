package organ

import error2 "gitlab.com/drakonka/gosnaillife/common/error"

type gonad struct {
	SnailBaseOrganKind
}

func NewGonad() Organ {
	t := gonad{}
	err := t.Init()
	if err != nil {
		error2.HandleErr(err, "NewEye")
	}
	o := newOrgan(&t)
	return o
}

func (t *gonad) Init() error {
	t.IdealQuant = 1
	t.Id = "gonad"
	return nil
}
