-- +goose Up
CREATE TABLE organs (
  organ_id int UNSIGNED NOT NULL PRIMARY KEY,
  organ_type_id int UNSIGNED NOT NULL,
  snail_id int UNSIGNED NOT NULL,
  max_efficiency smallint UNSIGNED DEFAULT 100,
  current_efficienty_perc tinyint UNSIGNED DEFAULT 100,
  weight_mg smallint UNSIGNED,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  last_check_date TIMESTAMP DEFAULT NOW()
);

-- +goose Down
DROP TABLE organs;