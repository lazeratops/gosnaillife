package tests

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"testing"
)

func TestPutInJar(t *testing.T) {
	testCases := []struct {
		name     string
		jarId    int
		jarWidth uint16
		jarDepth uint16
		err      error
	}{
		{
			name:     "SuccessfulJarPut",
			jarId:    50,
			jarWidth: 100,
			jarDepth: 1055,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s := snail.Snail{}
			err := s.PutInJar(tc.jarId, tc.jarWidth, tc.jarDepth)

			hw := int(tc.jarWidth / 2)
			hd := int(tc.jarDepth / 2)
			tcerr := testing2.TestErrorType(tc.err, err)
			if tcerr != nil {
				t.Error(testing2.TCFailedMsg("LocationTest", tc.name, tcerr))
			} else if s.PosX < -hw || s.PosX > hw {
				ctx := fmt.Sprintf("expected %d > PosX (%d) < %d", -hw, s.PosX, hw)
				t.Error(testing2.TCFailedMsg("LocationTest", tc.name, ctx))
			} else if s.PosY < -hd || s.PosY > hd {
				ctx := fmt.Sprintf("expected %d > PosY (%d) < %d", -hd, s.PosY, hd)
				t.Error(testing2.TCFailedMsg("LocationTest", tc.name, ctx))
			} else if s.CurrentJarId != tc.jarId {
				ctx := fmt.Sprintf("CurrentJarId(%d) should be %d", s.CurrentJarId, tc.jarId)
				t.Error(testing2.TCFailedMsg("LocationTest", tc.name, ctx))
			}
		})
	}
}
