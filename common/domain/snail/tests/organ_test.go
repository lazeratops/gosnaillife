package tests

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/gene"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/tests"
	"math"
	"math/rand"
	"os"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	rand.Seed(time.Now().UnixNano())
	err := common.InitLog("")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	ret := m.Run()
	os.Exit(ret)
}

func TestOrganMaturity(t *testing.T) {
	testCases := []struct {
		name         string
		maturityRate float64
	}{
		{
			name:         "RandomMaturityRate",
			maturityRate: -1.00,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s, err := snail.GenerateRandomSnail(0, nil)
			if err != nil {
				t.Fatal(testing2.TCFailedMsg("TestOrganMaturity", tc.name, err))
				return
			}
			heart := s.GetOrgan("heart")
			now := time.Now()
			heart.LastCheck = now.Add(-(1 * (time.Hour * 24)))
			maturity := heart.MaturityLevel
			rate := heart.MaturityRate
			heart.UpdateMaturity(func() time.Time { return now })
			newMaturity := heart.MaturityLevel
			roundedMaturity := math.Round(float64(newMaturity)*1000) / 1000
			wantedMaturity := math.Round(float64(maturity+rate)*1000) / 1000
			if roundedMaturity != wantedMaturity {
				msg := fmt.Sprintf("Expected %f maturity, got %f", maturity+rate, newMaturity)
				t.Error(testing2.TCFailedMsg("TestOrganMaturity", tc.name, msg))
			}

		})
	}

}

func TestOrganUpdate(t *testing.T) {
	testCases := []struct {
		name                string
		organ               organ.Organ
		lastOrganCheck      time.Time
		organOwner          snail.Snail
		expectedOrganWeight float64
		gene                organism.Gene
		updateDb            bool
		err                 error
	}{
		{
			name:                "OuterShellUpdateNoDb",
			organ:               organ.NewOuterShell(),
			organOwner:          snail.Snail{Id: 1},
			lastOrganCheck:      time.Now().Add(-(1 * (time.Hour * 24))),
			gene:                &gene.SizeA1{},
			expectedOrganWeight: 1.00,
			updateDb:            false,
			err:                 nil,
		},
		{
			name:                "OrganShellUpdateHalfDayNoDb",
			organ:               organ.NewOuterShell(),
			organOwner:          snail.Snail{Id: 1},
			lastOrganCheck:      time.Now().Add(-(1 * (time.Hour * 24))),
			gene:                &gene.SizeA1{},
			expectedOrganWeight: 1.00,
			updateDb:            false,
			err:                 nil,
		},
		{
			name:                "OuterShellUpdateWithDb",
			organ:               organ.NewOuterShell(),
			organOwner:          snail.Snail{Id: 1},
			lastOrganCheck:      time.Now().Add(-(1 * (time.Hour * 12))),
			gene:                &gene.SizeA1{},
			expectedOrganWeight: 0.5,
			updateDb:            true,
			err:                 nil,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.organOwner.InitOrganism()
			if err != nil {
				t.Fatal(err)
			}
			tc.organ.SetOwner(&tc.organOwner)
			tc.organ.SnailId = tc.organOwner.Id
			tc.organ.SnailId = tc.organOwner.Id
			tc.organ.LastCheck = tc.lastOrganCheck
			tc.organOwner.Organs = append(tc.organOwner.Organs, &tc.organ)
			err = tc.gene.Init()
			if err != nil {
				t.Fatal(err)
			}
			g := tc.gene.(*gene.SizeA1)
			g.Alleles = g.PossibleAlleles
			tc.organOwner.Genes()[tc.gene.GeneId()] = tc.gene

			if tc.updateDb {
				tu := tests.NewTestUtil()
				db, err := tu.CreateTestDB("organ_test")
				if err != nil {
					t.Fatal(err)
				}
				err = tu.RunDbMigrations(false, "")
				if err != nil {
					t.Fatalf(testing2.TCFailedMsg("OrganUpdate Test", tc.name, err))
				}
				defer tu.DeleteTestDB(true, "organ_test")
				tc.organ.Repo, err = organ.NewRepo(db)
				if err != nil {
					t.Fatalf(testing2.TCFailedMsg("OrganUpdate Test", tc.name, err))
				}
				tc.organOwner.Repo, err = snail.NewRepo(db)
				if err != nil {
					t.Fatalf(testing2.TCFailedMsg("OrganUpdate Test", tc.name, err))
				}
				_, err = tc.organ.Save()
				if err != nil {
					t.Fatalf(testing2.TCFailedMsg("OrganUpdate Test", tc.name, err))
				}
			}

			err = tc.organOwner.UpdateOrgans(tc.updateDb)
			if err != nil {
				t.Errorf(err.Error())
			} else if tc.organ.WeightMg != tc.expectedOrganWeight {
				t.Errorf("Expected size_a1 gene to increase outer shell weight by %f; actual weight is %f", tc.expectedOrganWeight, tc.organ.WeightMg)
			}
		})
	}
}
