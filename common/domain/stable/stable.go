package stable

import (
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"reflect"
)

type Stable struct {
	domain.Model `db:"-"`
	Id           int        `db:"stable_id"`
	OwnerId      int        `db:"owner_id"`
	Name         string     `db:"name"`
	Brand        string     `db:"brand"`
	Description  string     `db:"description"`
	Jars         []*jar.Jar `json:"jars"`
}

// Validator

func (m *Stable) GetRules() []validator.Rule {
	nameLength := validator.NewLengthRule(5, 50, m.Name, "Name")

	rules := []validator.Rule{
		&nameLength,
	}
	return rules
}

func (m *Stable) Validate() error {
	rules := m.GetRules()
	return m.Model.Validate(rules, "Stable")
}

func (m *Stable) ValidateSpecific(fieldsToValidate ...string) []error {
	rules := m.GetRules()
	tagMap := m.BuildDbTagMap(reflect.TypeOf(*m))
	return m.Model.ValidateSpecific(rules, tagMap, fieldsToValidate, "Stable")
}

// End validator

func (m *Stable) GetId() interface{} {
	return m.Id
}

func (m *Stable) SetId(id interface{}) {
	m.Id = id.(int)
}

func (m *Stable) Save() (interface{}, error) {
	return m.Repo.InsertOne(m)
}

func (m *Stable) Update(fields []string) error {
	dbTagMap := m.GetDbTagMap(reflect.TypeOf(*m))
	r := m.Repo.(*Repo)
	return r.updateOneModel(m, dbTagMap, fields)
}

func (m *Stable) setJars(j []*jar.Jar) {
	m.Jars = j
}
