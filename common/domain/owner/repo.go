package owner

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/stable"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"reflect"
	"strconv"
)

type Repo struct {
	domain.Repo
}

func NewRepo(database databases.Database) (repo2.Repo, error) {
	r := Repo{}
	if err := r.Init(database); err != nil {
		return nil, err
	}
	r.Tablename = "owners"
	r.Db = database
	return &r, nil
}

func (r *Repo) NewModel() repo2.Model {
	m := Owner{}
	m.Repo = r
	return &m
}

func (r *Repo) GetOne(id string, fields []string) (repo2.Model, error) {
	row, err := r.Db.GetRow(r.Tablename, id, fields)
	if err != nil {
		error2.HandleErr(err, "GetOne [Get Owner Row]")
		return nil, err
	}

	o, err := GetOwnerFromRowResult(row)
	if err != nil {
		error2.HandleErr(err, "GetOne [Owner]")
		return nil, err
	}
	if util.StrIndexOf(fields, "stables") > -1 || len(fields) == 0 {
		r.loadOwnerStables(o)
	}
	oId := strconv.Itoa(o.Id)
	if oId == id {
		o.Repo = r
		return o, err
	}
	return nil, err
}

// What do we expect here? Name is 20
func (r *Repo) GetMany(where string, params []interface{}, fields []string) (owners []interface{}, err error) {
	fieldsFromDb := make([]string, len(fields))
	copy(fieldsFromDb, fields)
	fieldsFromDb, err = r.filterFields(fieldsFromDb)
	if err != nil {
		return nil, err
	}
	var m = Owner{}
	res, err := r.Repo.GetMany(&m, fieldsFromDb, where, params)
	if err != nil {
		error2.HandleErr(err, "GetMany [Get Owner Rows]")
		return owners, err
	}
	for _, v := range res {
		owner, err := GetOwnerFromRowResult(v)
		if err != nil {
			error2.HandleErr(err, "GetMany[Decode Owner Map]")
			continue
		}
		if util.CaselessStrIndexOf(fields, "stables") > -1 || len(fields) == 0 {
			fmt.Println("Finding stables")
			r.loadOwnerStables(owner)
		}
		owner.Repo = r
		owners = append(owners, owner)
	}
	return owners, err
}

func (r *Repo) InsertOne(item interface{}) (id interface{}, err error) {
	id = -1
	if r.Db == nil {
		return id, &repo2.RepoError{Err: "The database associated with this r could not be found."}
	}
	model, ok := item.(*Owner)
	if !ok {
		msg := fmt.Sprintf("The provided item is not an owner; it is a %v", reflect.TypeOf(item))
		return id, &repo2.RepoError{Err: msg}
	}
	err, id = r.ValidateAndInsertModel(model)
	if err != nil {
		return -1, err
	}
	if _, ok := id.(int); !ok {
		si := fmt.Sprintf("%v", id)
		id, _ = strconv.Atoi(si)
	}
	return id, err
}

func (r *Repo) UpdateOne(itemId interface{}, fields map[string]string) error {
	// Retrieve Model with above id
	m, err := r.GetOne(itemId.(string), []string{"owner_id"})

	model, ok := m.(*Owner)
	if !ok {
		msg := fmt.Sprintf("The provided item is not an Model; it is a %v", reflect.TypeOf(m))
		return &repo2.RepoError{Err: msg}
	}
	modelMap := domain.MakeModelMap(model, model.GetDbTagMap(reflect.TypeOf(*model)), fields)
	newModel, err := GetOwnerFromRowResult(modelMap)
	if err != nil {
		msg := fmt.Sprintf("Could not update Model: %s", err.Error())
		return &repo2.RepoError{Err: msg}
	}
	newModel.Id = model.Id

	err = r.ValidateAndUpdateModel(newModel, modelMap)
	return err
}

func (r *Repo) updateOneModel(m repo2.Model, tagMap map[string]string, fields []string) error {
	id := strconv.Itoa(m.GetId().(int))
	mp := r.UpdateOneModelPrep(m, tagMap, fields)
	return r.UpdateOne(id, mp)
}

func (r *Repo) loadOwnerStables(owner *Owner) {
	sr, _ := stable.NewRepo(r.Db)
	stablesi, err := sr.GetMany("owner_id=?", []interface{}{owner.Id}, []string{})
	error2.HandleErr(err, "getOwnersStables")
	if err != nil {
		return
	}
	var stables []*stable.Stable
	for _, s := range stablesi {
		stables = append(stables, s.(*stable.Stable))
	}
	owner.setStables(stables)
}

func (r *Repo) filterFields(fields []string) ([]string, error) {
	fieldsToFilter := []string{"stables", ""}
	fields = r.Repo.FilterFields(fieldsToFilter, fields)
	return fields, nil
}

func GetOwnerFromRowResult(r databases.RowResult) (m *Owner, err error) {
	var o Owner
	model, err := domain.GetModelFromRowResult(&o, r)
	if err == nil {
		m = model.(*Owner)
	}
	return m, err
}
