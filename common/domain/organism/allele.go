package organism

// AlleleReaderWriter reads and writes allele fields
type AlleleReaderWriter interface {
	AlleleReader
	AlleleWriter
}

// AlleleReader reads allele fields
type AlleleReader interface {
	GetGeneId() string
	GetModifier() float64
	GetDominanceLevel() int
	GetKey() string
}

// AlleleWriter sets allele fields
type AlleleWriter interface {
	SetOwnerId(id int)
}

// Allele is a base struct representing a variation of a gene. It cannot be used on its own
type Allele struct {
	Modifier       float64 `db:"-"`
	DominanceLevel int     `db:"-"`
}

// Alleles is a slice of AlleleReaderWriter interfaces
type Alleles []AlleleReaderWriter

func (a Alleles) Len() int {
	return len(a)
}

func (a Alleles) Less(i, j int) bool {
	return a[i].GetDominanceLevel() > a[j].GetDominanceLevel()
}

func (a Alleles) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}
