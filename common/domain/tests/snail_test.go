package tests

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"sort"
	"strconv"
	"testing"
)

type snailTest struct {
	test
}

func newSnailTest(db databases.Database) *snailTest {
	test := snailTest{}
	test.tablename = "snails"
	test.db = db
	test.idKey = "snail_id"
	return &test
}

func (t *snailTest) createInsertOneTestCases() []insertOneTestCase {
	r, err := snail.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &snail.Snail{Id: 509, Name: "Cool Snail"}
	m1.SetRepo(r)

	m2 := &snail.Snail{Name: "Cool Snail Dva"}
	m2.SetRepo(r)

	m3 := &snail.Snail{Name: "SnailWith Ahome", CurrentJarId: 500}
	m3.SetRepo(r)

	return []insertOneTestCase{
		{"SnailInsertWithId", m1, nil},
		{"SnailInsertWithoutId", m2, nil},
		{"SnailInsertedInJar", m3, nil},
		{"SnailInsertWithBadType", snail.Snail{Name: "Wat a Snail"}, &repo.RepoError{}},
	}
}

func (t *snailTest) createDeleteOneTestCase() []deleteOneTestCase {
	r, err := snail.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &snail.Snail{Name: "Deleted Snail"}
	m1.SetRepo(r)

	return []deleteOneTestCase{
		{
			name:  "SuccessfulSnailDelete",
			model: m1,
			want:  nil,
		},
	}
}

func (t *snailTest) createSaveModelTestCases() []saveModelTestCase {
	var testCases []saveModelTestCase
	for _, tc := range t.createInsertOneTestCases() {
		if m, ok := tc.model.(*snail.Snail); ok {
			m.Id = 0
			m.OwnerId++
			m.Name += "rand"
			testCases = append(testCases, saveModelTestCase(tc))
		}
	}
	repo := t.createRepo(t.db)
	randomSnail, _ := snail.GenerateRandomSnail(0, repo)
	tc := insertOneTestCase{tcname: "SaveRandomSnail", model: randomSnail, want: nil}
	testCases = append(testCases, saveModelTestCase(tc))
	return testCases
}

func (t *snailTest) createUpdateOneTestCases() []updateOneTestCase {
	return []updateOneTestCase{
		{"SnailSuccessfulUpdate", "500", map[string]string{"name": "Bob's Modified Snail"}, nil},
		{"SnailSuccessfulUpdateByField", "500", map[string]string{"name": "Modified Snail 2"}, nil},
		{"SnailValidationFailedUpdate", "500", map[string]string{"name": "o"}, new(validator.ValidationError)},
	}
}

func (t *snailTest) createUpdateModelTestCases() []updateModelTestCase {
	r, err := snail.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &snail.Snail{Id: 500, Name: "Bob's Modified Snail"}
	m1.SetRepo(r)

	m2 := &snail.Snail{Id: 500, Name: "o"}
	m2.SetRepo(r)

	return []updateModelTestCase{
		{"SnailModelSuccessfulUpdate", m1, []string{"Name"}, nil},
		{"SnailModelValidationFailedUpdate", m2, []string{"Name"}, new(validator.ValidationError)},
	}
}

func (t *snailTest) createGetManyTestCases() []getManyTestCase {
	return []getManyTestCase{
		{"SnailGetOneWithAllProperties", "snail_id=?", []interface{}{"500"}, []string{}, 1},
		{"SnailGetOneWithOneProperty", "snail_id=?", []interface{}{"500"}, []string{"name"}, 1},
		{"SnailGetManyWithAllProperties", "name LIKE ?", []interface{}{"%Bob%"}, []string{}, 3},
		{"SnailGetManyWithOneProperty", "name LIKE ?", []interface{}{"%Bob%"}, []string{"name"}, 3},
		{"SnailGetNoneWithAllProperties", "snail_id=?", []interface{}{"303"}, []string{}, 0},
		{"SnailGetNoneWithOneProperty", "snail_id=?", []interface{}{"303"}, []string{"name"}, 0},
	}
}

func (t *snailTest) createGetManyWithDbErrTestCases() []getManyWithErrTestCase {
	return []getManyWithErrTestCase{
		{"SnailGetManyWithDbErr", "snail_id=?", []interface{}{"500"}, []string{}, errors.New("Something went wrong")},
	}
}

func (t *snailTest) createGetOneTestCases() []getOneTestCase {
	return []getOneTestCase{
		{"SnailGetOneWithAllProperties", "500", []string{}, 1},
		{"SnailGetOneWithOneProperty", "500", []string{"name"}, 1},
		{"SnailGetOneOneInvalid", "1033", []string{"name"}, 0},
	}
}

func (t *snailTest) createGetOneWithDbErrTestCases() []getOneWithErrorTestCase {
	return []getOneWithErrorTestCase{
		{"SnailGetOneWithDbErr", "500", []string{}, errors.New("Something went wrong")},
	}
}

func (t *snailTest) prep() error {
	return t.seed()
}

func (t *snailTest) seed() error {
	fmt.Println("seeding snails")
	rows := []map[string]string{
		{
			"snail_id": "500",
			"name":     "Bob's Snail One",
		},
		{
			"name": "Bob's Snail Two",
		},
		{
			"name": "Bob's Snail Three",
		},
	}
	return t.test.seed(rows)
}

func (t *snailTest) createRepo(db databases.Database) repo.Repo {
	repo, _ := snail.NewRepo(db)
	return repo
}

func (t *snailTest) createModel(params map[string]interface{}) (repo.Model, error) {
	model, err := snail.GetSnailFromRowResult(params)
	return model, err
}

func (t *snailTest) getAdditionalTests() []func(tT *testing.T) {
	tests := []func(tT *testing.T){
		t.createRepoAndModelTest,
		t.createGenerateRandomSnailTest,
		t.getOneGeneRetrievalTest,
	}
	return tests
}

func (t *snailTest) getOneGeneRetrievalTest(tT *testing.T) {
	testCases := []struct {
		name        string
		fields      []string
		expectGenes bool
	}{
		{"SnailGetOneWithAllProperties", []string{}, true},
		{"SnailGetOneWithOneProperty", []string{"name"}, false},
	}
	for _, tc := range testCases {
		tT.Run(tc.name, func(tT *testing.T) {
			r, _ := snail.NewRepo(t.db)
			newSnail, _ := snail.GenerateRandomSnail(0, r)
			gene := newSnail.Genes()["size_a1"]
			alleles := gene.GetAlleles()
			sort.Sort(alleles)
			id, err := newSnail.Save()
			defer r.DeleteOne(newSnail.Id)
			if err != nil {
				tT.Error(testing2.TCFailedMsg("GeneRetrievalTest", tc.name, err))
				return
			}
			sId := strconv.Itoa(id.(int))
			retrievedSnail, err := r.GetOne(sId, tc.fields)
			if err != nil {
				tT.Error(testing2.TCFailedMsg("GeneRetrievalTest", tc.name, err))
				return
			}
			retrievedGenes := retrievedSnail.(*snail.Snail).Genes()["size_a1"]
			if retrievedGenes == nil {
				if tc.expectGenes == false {
					return
				}
				tT.Errorf(testing2.TCFailedMsg("GeneRetrievalTest", tc.name, "Expected genes to be retrieved, got nil"))
			}

			retrievedAlleles := retrievedGenes.GetAlleles()
			sort.Sort(retrievedAlleles)

			for _, originalAllele := range alleles {
				modifier := originalAllele.GetModifier()
				var found bool
				for _, retrievedAllele := range retrievedAlleles {
					retrievedModifier := retrievedAllele.GetModifier()
					if modifier == retrievedModifier {
						found = true
						break
					}
				}
				if !found {
					msg := fmt.Sprintf("Could not find allele with expected modifier %f", modifier)
					tT.Error(testing2.TCFailedMsg("GeneRetrievalTest", tc.name, msg))
				}
			}
		})
	}
}

func (t *snailTest) createGenerateRandomSnailTest(tT *testing.T) {
	testCases := []struct {
		tcname string
		db     databases.Database
		want   error
	}{
		{"SnailGenerateRandom", t.db, nil},
	}
	for _, tc := range testCases {
		tT.Run(tc.tcname, func(tT *testing.T) {
			r, _ := snail.NewRepo(tc.db)
			snail, err := snail.GenerateRandomSnail(5, nil)
			snail.Name = "Snailatron"
			if err != nil {
				tT.Error(err)
				return
			}
			id, err := r.InsertOne(snail)
			if err != nil {
				tT.Error(err)
				return
			}
			if id.(int) <= 0 {
				tT.Errorf("Invalid ID: %v", id)
			}
		})
	}
}

func (t *snailTest) createRepoAndModelTest(tT *testing.T) {
	testCases := []struct {
		tcname string
		db     databases.Database
		want   error
	}{
		{"SnailCreateRepoAndModelSuccess", t.db, nil},
		{"SnailCreateRepoNoDb", nil, new(repo.RepoError)},
	}

	for _, tc := range testCases {
		tT.Run(tc.tcname, func(tT *testing.T) {
			r, err := snail.NewRepo(tc.db)
			testerr := testing2.TestErrorType(tc.want, err)
			if testerr != nil {
				tT.Error(testing2.TCFailedMsg("SnailTest", tc.tcname, testerr))
				return
			} else if r != nil {
				m := r.NewModel()
				if m == nil {
					tT.Errorf(testing2.TCFailedMsg("SnailTest", tc.tcname, "model is nil"))
				}
			}
		})
	}
}
