package tests

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	"gitlab.com/drakonka/gosnaillife/common/domain/owner"
	stable2 "gitlab.com/drakonka/gosnaillife/common/domain/stable"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"testing"
)

type ownerTest struct {
	test
}

func newOwnerTest(db databases.Database) *ownerTest {
	test := ownerTest{}
	test.tablename = "owners"
	test.db = db
	test.idKey = "owner_id"
	return &test
}

func (ot *ownerTest) createInsertOneTestCases() []insertOneTestCase {
	r, err := owner.NewRepo(ot.db)
	if err != nil {
		panic(err)
	}
	m1 := &owner.Owner{Id: 10, FirstName: "testname", LastName: "Lan"}
	m1.SetRepo(r)

	m2 := &owner.Owner{FirstName: "testname", LastName: "Lan"}
	m2.SetRepo(r)

	m3 := &owner.Owner{FirstName: "randomname", LastName: "234Test"}
	m3.SetRepo(r)

	m4 := &owner.Owner{FirstName: "a", LastName: "o"}
	m4.SetRepo(r)

	return []insertOneTestCase{
		{"OwnerWithId", m1, nil},
		{"OwnerWithoutId", m2, nil},
		{"OwnerWithoutId2", m3, nil},
		{"OwnerValidationFailed", m4, new(validator.ValidationError)},
		{"OwnerInsertWithBadType", jar.Jar{Name: "Watajar"}, &repo.RepoError{}},
	}
}

func (t *ownerTest) createDeleteOneTestCase() []deleteOneTestCase {
	r, err := owner.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &owner.Owner{FirstName: "to be", LastName: "deleted"}
	m1.SetRepo(r)

	return []deleteOneTestCase{
		{
			name:  "SuccessfulOwnerDelete",
			model: m1,
			want:  nil,
		},
	}
}

func (t *ownerTest) createSaveModelTestCases() []saveModelTestCase {
	var testCases []saveModelTestCase
	for _, tc := range t.createInsertOneTestCases() {
		if m, ok := tc.model.(*owner.Owner); ok {
			m.Id = 0
			testCases = append(testCases, saveModelTestCase(tc))
		}
	}
	return testCases
}

func (ot *ownerTest) createUpdateOneTestCases() []updateOneTestCase {
	return []updateOneTestCase{
		{"SuccessfulUpdate", 50, map[string]string{"lastname": "newlastname"}, nil},
		{"OwnerValidationFailedUpdate", 50, map[string]string{"lastname": "o"}, new(validator.ValidationError)},
	}
}

func (ot *ownerTest) createUpdateModelTestCases() []updateModelTestCase {
	r, err := owner.NewRepo(ot.db)
	if err != nil {
		panic(err)
	}
	m1 := &owner.Owner{Id: 50, LastName: "Smithson"}
	m1.SetRepo(r)

	m2 := &owner.Owner{Id: 50, LastName: "o"}
	m2.SetRepo(r)

	return []updateModelTestCase{
		{"OwnerModelSuccessfulUpdate", m1, []string{"LastName"}, nil},
		{"OwnerModelValidationFailedUpdate", m2, []string{"LastName"}, new(validator.ValidationError)},
	}
}

func (ot *ownerTest) createGetManyTestCases() []getManyTestCase {
	return []getManyTestCase{
		{"OwnerGetOneWithAllProperties", "owner_id=?", []interface{}{50}, []string{}, 1},
		{"OwnerGetOneWithOneProperty", "owner_id=?", []interface{}{50}, []string{"FirstName"}, 1},
		{"OwnerGetManyWithAllProperties", "firstname LIKE ?", []interface{}{"%gettest%"}, []string{}, 3},
		{"OwnerGetManyWithOneProperty", "firstname LIKE ?", []interface{}{"%gettest%"}, []string{"FirstName"}, 3},
		{"OwnerGetNoneWithAllProperties", "owner_id=?", []interface{}{303}, []string{}, 0},
		{"OwnerGetNoneWithOneProperty", "owner_id=?", []interface{}{303}, []string{"FirstName"}, 0},
	}
}

func (ot *ownerTest) createGetManyWithDbErrTestCases() []getManyWithErrTestCase {
	return []getManyWithErrTestCase{
		{"OwnerGetManyWithDbErr", "owner_id=?", []interface{}{"50"}, []string{}, errors.New("Something went wrong")},
	}
}

func (ot *ownerTest) createGetOneTestCases() []getOneTestCase {
	return []getOneTestCase{
		{"OwnerGetOneWithAllProperties", 50, []string{}, 1},
		{"OwnerGetOneWithOneProperty", 50, []string{"FirstName"}, 1},
		{"OwnerGetOneOneInvalid", 1033, []string{"FirstName"}, 0},
	}
}

func (ot *ownerTest) createGetOneWithDbErrTestCases() []getOneWithErrorTestCase {
	return []getOneWithErrorTestCase{
		{"OwnerGetOneWithDbErr", "50", []string{}, errors.New("Something went wrong")},
	}
}

func (ot *ownerTest) prep() error {
	return ot.seed()
}

func (ot *ownerTest) seed() error {
	fmt.Println("Seeding owners")
	rows := []map[string]string{
		{
			"owner_id":  "50",
			"firstname": "gettestname",
			"lastname":  "lastname",
		},
		{
			"firstname": "gettestname2",
			"lastname":  "gettestlast",
		},
		{
			"firstname": "randomname",
			"lastname":  "234gettest",
		},
		{
			"firstname": "gettestname3",
			"lastname":  "wombat",
		},
	}
	return ot.test.seed(rows)
}

func (ot *ownerTest) createRepo(db databases.Database) repo.Repo {
	repo, err := owner.NewRepo(db)
	if err != nil {
		panic(err)
	}
	return repo
}

func (ot *ownerTest) createModel(params map[string]interface{}) (repo.Model, error) {
	model, err := owner.GetOwnerFromRowResult(params)
	return model, err
}

func (ot *ownerTest) getAdditionalTests() []func(t *testing.T) {
	tests := []func(t *testing.T){
		ot.ownerFindStablesTest,
		ot.createRepoAndModelTest,
	}
	return tests
}

func (ot *ownerTest) ownerFindStablesTest(t *testing.T) {
	t.Run("OwnerFindStablesTest", func(t *testing.T) {
		fmt.Println("Running owner find stables test!")
		// Prep step
		orepo := ot.createRepo(ot.db)
		o := owner.Owner{
			UserId:    "0",
			FirstName: "Bob",
			LastName:  "Smith",
		}
		ownerid, err := orepo.InsertOne(&o)
		if err != nil {
			t.Fatalf("Could not create owner: %s", err)
		}

		stable := stable2.Stable{
			OwnerId: ownerid.(int),
			Name:    "Test Stable Name",
		}
		srepo, err := stable2.NewRepo(database)
		if err != nil {
			panic(err)
		}
		_, err = srepo.InsertOne(&stable)
		if err != nil {
			t.Fatalf("Could not create stable: %s", err)
		}

		// Retrieve
		owners, err := orepo.GetMany("owner_id = ?", []interface{}{ownerid}, []string{"owner_id", "firstname"})
		if err != nil {
			t.Fatalf("Could not retrieve owner: %s", err)
		}
		if len(owners) == 0 {
			t.Fatalf("Could not find owner with id %v", ownerid)
		}
		for _, o := range owners {
			ownr := *o.(*owner.Owner)
			stables := ownr.LoadStables()
			if len(stables) < 1 {
				t.Fatalf("Could not find expected stable!")
			}
		}
	})
}

func (t *ownerTest) createRepoAndModelTest(tT *testing.T) {
	testCases := []struct {
		tcname string
		db     databases.Database
		want   error
	}{
		{"OwnerCreateRepoAndModelSuccess", t.db, nil},
		{"OwnerCreateRepoNoDb", nil, new(repo.RepoError)},
	}

	for _, tc := range testCases {
		tT.Run(tc.tcname, func(tT *testing.T) {
			r, err := owner.NewRepo(tc.db)
			testerr := testing2.TestErrorType(tc.want, err)
			if testerr != nil {
				tT.Error(testing2.TCFailedMsg("OwnerTest", tc.tcname, testerr))
				return
			}
			if tc.want != nil {
				return
			}
			m := r.NewModel()
			if m == nil {
				tT.Error(testing2.TCFailedMsg("OwnerTest", tc.tcname, "Model is nil"))
			}
		})
	}
}
