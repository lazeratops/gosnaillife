package tests

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common/domain/owner"
	"gitlab.com/drakonka/gosnaillife/common/domain/stable"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"testing"
)

type stableTest struct {
	test
}

func newStableTest(db databases.Database) *stableTest {
	test := stableTest{}
	test.tablename = "stables"
	test.db = db
	test.idKey = "stable_id"
	return &test
}

func (st *stableTest) prep() error {
	return st.seed()
}

func (st *stableTest) seed() error {
	fmt.Println("Seeding stables")
	rows := []map[string]string{
		{
			"stable_id": "15",
			"owner_id":  "10",
			"name":      "Super Cool Stable",
		},
	}
	return st.test.seed(rows)
}

func (st *stableTest) createRepo(db databases.Database) repo.Repo {
	repo, _ := stable.NewRepo(db)
	return repo
}

func (st *stableTest) createModel(params map[string]interface{}) (repo.Model, error) {
	model, err := stable.GetStableFromRowResult(params)
	return model, err
}

func (st *stableTest) getAdditionalTests() []func(t *testing.T) {
	tests := []func(t *testing.T){
		st.createRepoAndModelTest,
	}
	return tests
}

func (st *stableTest) createInsertOneTestCases() []insertOneTestCase {
	r, err := stable.NewRepo(st.db)
	if err != nil {
		panic(err)
	}

	m1 := &stable.Stable{Id: 10, OwnerId: 50, Name: "Super Cool Stable"}
	m1.SetRepo(r)

	m2 := &stable.Stable{OwnerId: 50, Name: "testnameskdasjfasdkf sadkfjasdkfasdfkjsad fkasjdf kadsjfahsdfkjahdsfa kjfahds jfahdsfkj hajkdfds2"}
	m2.SetRepo(r)

	return []insertOneTestCase{
		{"StableGoodName", m1, nil},
		{"StableBadName", m2, new(validator.ValidationError)},
		{"StableInsertWithBadType", &owner.Owner{}, &repo.RepoError{}},
	}
}

func (t *stableTest) createDeleteOneTestCase() []deleteOneTestCase {
	r, err := stable.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &stable.Stable{Name: "Deleted Stable"}
	m1.SetRepo(r)

	return []deleteOneTestCase{
		{
			name:  "SuccessfulStableDelete",
			model: m1,
			want:  nil,
		},
	}
}

func (t *stableTest) createSaveModelTestCases() []saveModelTestCase {
	var testCases []saveModelTestCase
	for _, tc := range t.createInsertOneTestCases() {
		if m, ok := tc.model.(*stable.Stable); ok {
			m.Id = 0
			m.OwnerId++
			testCases = append(testCases, saveModelTestCase(tc))
		}
	}
	return testCases
}

func (st *stableTest) createUpdateOneTestCases() []updateOneTestCase {
	return []updateOneTestCase{
		{"StableSuccessfulUpdate", 15, map[string]string{"name": "New Stable Name"}, nil},
		{"StableValidationFailedUpdate", 15, map[string]string{"name": "o"}, new(validator.ValidationError)},
	}
}

func (st *stableTest) createUpdateModelTestCases() []updateModelTestCase {
	r, err := stable.NewRepo(st.db)
	if err != nil {
		panic(err)
	}
	m1 := &stable.Stable{Id: 15, Name: "Fancy Stable Name"}
	m1.SetRepo(r)

	m2 := &stable.Stable{Id: 15, Name: "o"}
	m2.SetRepo(r)

	return []updateModelTestCase{
		{"StableModelSuccessfulUpdate", m1, []string{"Name"}, nil},
		{"StableModelValidationFailedUpdate", m2, []string{"Name"}, new(validator.ValidationError)},
	}
}

func (st *stableTest) createGetManyTestCases() []getManyTestCase {
	return []getManyTestCase{
		{"StableGetOneWithAllProperties", "stable_id=?", []interface{}{15}, []string{}, 1},
		{"StableGetOneWithOneProperty", "stable_id=?", []interface{}{15}, []string{"stable_id"}, 1},
		{"StableGetNone", "name LIKE ?", []interface{}{"%elephant%"}, []string{}, 0},
	}
}

func (t *stableTest) createGetManyWithDbErrTestCases() []getManyWithErrTestCase {
	return []getManyWithErrTestCase{
		{"StableGetManyWithDbErr", "jar_id=?", []interface{}{15}, []string{}, errors.New("Something went wrong")},
	}
}

func (st *stableTest) createGetOneTestCases() []getOneTestCase {
	return []getOneTestCase{
		{"StableGetOne", 15, []string{}, 1},
		{"StableGetOneWithField", 15, []string{"name"}, 1},
		{"StableGetOneNone", 234235, []string{}, 0},
	}
}

func (t *stableTest) createGetOneWithDbErrTestCases() []getOneWithErrorTestCase {
	return []getOneWithErrorTestCase{
		{"StableGetOneWithDbErr", "15", []string{}, errors.New("Something went wrong")},
	}
}

func (t *stableTest) createRepoAndModelTest(tT *testing.T) {
	testCases := []struct {
		tcname string
		db     databases.Database
		want   error
	}{
		{"StableCreateRepoAndModelSuccess", t.db, nil},
		{"StableCreateRepoNoDb", nil, new(repo.RepoError)},
	}

	for _, tc := range testCases {
		tT.Run(tc.tcname, func(tT *testing.T) {
			r, err := stable.NewRepo(tc.db)

			testerr := testing2.TestErrorType(tc.want, err)
			if testerr != nil {
				tT.Error(testing2.TCFailedMsg("StableTest", tc.tcname, testerr))
				return
			}
			if tc.want != nil {
				return
			}
			m := r.NewModel()
			if m == nil {
				tT.Errorf(testing2.TCFailedMsg("StableTest", tc.tcname, "model is nil"))
			}
		})
	}
}
