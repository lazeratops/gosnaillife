package user

import (
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"reflect"
)

type User struct {
	domain.Model
	Id          string `db:"_id"`
	Email       string `db:"email"`
	AuthHeaders map[string]string
	Password    string `db:"password"`
	IsGod       bool   `db:"god"`
}

// Validator

func (m *User) GetRules() []validator.Rule {
	emailFormat := validator.NewEmailRule(m.Email, "Email")
	emailLength := validator.NewLengthRule(3, 50, m.Email, "Email")
	rules := []validator.Rule{
		&emailFormat,
		&emailLength,
	}
	return rules
}

func (m *User) Validate() error {
	rules := m.GetRules()
	return m.Model.Validate(rules, "User")
}

func (m *User) ValidateSpecific(fieldsToValidate ...string) []error {
	rules := m.GetRules()
	tagMap := m.BuildDbTagMap(reflect.TypeOf(*m))
	return m.Model.ValidateSpecific(rules, tagMap, fieldsToValidate, "User")
}

// End Validator

func (m *User) GetId() interface{} {
	return m.Id
}

func (m *User) SetId(id interface{}) {
	m.Id = id.(string)
}

func (m *User) Save() (interface{}, error) {
	return m.Repo.InsertOne(m)
}

func (m *User) Update(fields []string) error {
	dbTagMap := m.GetDbTagMap(reflect.TypeOf(*m))
	r := m.Repo.(*Repo)
	return r.updateOneModel(m, dbTagMap, fields)
}
