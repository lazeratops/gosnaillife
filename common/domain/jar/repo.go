package jar

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"reflect"
	"strconv"
)

// Repo is a repository struct to retrieve and manipulate Jars.
type Repo struct {
	domain.Repo
}

// NewRepo creates and returns a new Repo
func NewRepo(database databases.Database) (repo2.Repo, error) {
	r := Repo{}
	if err := r.Init(database); err != nil {
		return nil, err
	}
	r.Tablename = "jars"
	r.Db = database
	return &r, nil
}

// NewModel creates and returns a new Jar
func (r *Repo) NewModel() repo2.Model {
	m := Jar{}
	m.Repo = r
	return &m
}

// GetOne retrieves one jar from the database by ID
func (r *Repo) GetOne(id string, fields []string) (repo2.Model, error) {
	row, err := r.Db.GetRow(r.Tablename, id, fields)
	if err == nil {
		m, err := GetJarFromRowResult(row)
		mId := strconv.Itoa(m.Id)
		if mId == id {
			m.Repo = r
			return m, err
		}
	}
	return nil, err
}

// GetMany retrieves multiple jars that match a where query from the database.
func (r *Repo) GetMany(where string, params []interface{}, fields []string) (models []interface{}, err error) {
	m := Jar{}
	res, err := r.Repo.GetMany(&m.Model, fields, where, params)
	if err != nil {
		error2.HandleErr(err, "GetMany [Get Jar Rows]")
		return models, err
	}
	for _, s := range res {
		model, err := GetJarFromRowResult(s)
		if err != nil {
			error2.HandleErr(err, "GetMany[Decode jar Map]")
			continue
		}
		models = append(models, model)
		if util.StrIndexOf(fields, "snails") > -1 || len(fields) == 0 {
			err = r.loadJarSnails(model)
			error2.HandleErr(err, "")
		}
	}
	return models, err
}

func (r *Repo) loadJarSnails(jar *Jar) error {
	sr, err := snail.NewRepo(r.Db)
	if err != nil {
		return err
	}
	snails, err := sr.GetMany("current_jar_id=?", []interface{}{jar.Id}, []string{})
	error2.HandleErr(err, "loadJarSnails")
	if err != nil {
		return err
	}
	var preppedSnails []snail.Snail
	for _, s := range snails {
		preppedSnails = append(preppedSnails, *s.(*snail.Snail))
	}
	jar.setSnails(preppedSnails)
	return nil
}

// InsertOne inserts one jar into the database and returns the ID of the inserted item.
func (r *Repo) InsertOne(item interface{}) (id interface{}, err error) {
	id = -1
	if r.Db == nil {
		return id, &repo2.RepoError{Err: "The database associated with this r could not be found."}
	}
	model, ok := item.(*Jar)
	if !ok {
		msg := fmt.Sprintf("The provided item is not a jar; it is a %v", reflect.TypeOf(item))
		err = &repo2.RepoError{Err: msg}
		return id, err
	}

	err, id = r.ValidateAndInsertModel(model)
	if err != nil {
		return -1, err
	}
	if _, ok := id.(int); !ok {
		si := fmt.Sprintf("%v", id)
		id, _ = strconv.Atoi(si)
	}
	return id, err
}

// UpdateOne updates one jar in the database by ID.
func (r *Repo) UpdateOne(itemId interface{}, fields map[string]string) error {
	// Retrieve jar with above id
	m, err := r.GetOne(itemId.(string), []string{"jar_id"})

	model, ok := m.(*Jar)
	if !ok {
		msg := fmt.Sprintf("The provided item is not an jar; it is a %v", reflect.TypeOf(m))
		return &repo2.RepoError{Err: msg}
	}
	modelMap := domain.MakeModelMap(model, model.GetDbTagMap(reflect.TypeOf(*model)), fields)

	newModel, err := GetJarFromRowResult(modelMap)
	if err != nil {
		msg := fmt.Sprintf("Could not update jar: %s", err.Error())
		return &repo2.RepoError{Err: msg}
	}
	newModel.Id = model.Id

	err = r.ValidateAndUpdateModel(newModel, modelMap)
	return err
}

func (r *Repo) updateOneModel(m repo2.Model, tagMap map[string]string, fields []string) error {
	id := strconv.Itoa(m.GetId().(int))
	mp := r.UpdateOneModelPrep(m, tagMap, fields)
	return r.UpdateOne(id, mp)
}

// GetJarFromRowResult takes a RowResult and returns a Jar Model.
func GetJarFromRowResult(r databases.RowResult) (m *Jar, err error) {
	var jar Jar
	model, err := domain.GetModelFromRowResult(&jar, r)
	if err == nil {
		m = model.(*Jar)
	}
	return m, err
}
