package jar

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"gitlab.com/drakonka/gosnaillife/common/util/math"
	"reflect"
	"strings"
)

// Jar represents a snail habitat.
type Jar struct {
	domain.Model      `db:"-"`
	Id                int `db:"jar_id"`
	OwnerId           int
	StableId          int    `db:"stable_id"`
	Name              string `db:"name"`
	Description       string `db:"description"`
	IsBreedingEnabled bool   `db:"is_breeding_enabled"`
	Snails            []snail.Snail
	Width             uint16 `db:"width_cm"`
	Height            uint16 `db:"height_cm"`
	Depth             uint16 `db:"depth_cm"`
	SizeLabel         string
	Ascii             string
}

// NewJar generates a new jar.
func NewJar(w, h, d uint16) *Jar {
	j := Jar{
		Width:  w,
		Height: h,
		Depth:  d,
	}
	j.setSizeLabel()
	j.Ascii = j.GenerateAscii(1.0)
	return &j
}

// GetId retrieves the ID of the given jar.
func (m *Jar) GetId() interface{} {
	return m.Id
}

// SetId sets the ID of the given jar.
func (m *Jar) SetId(id interface{}) {
	m.Id = id.(int)
}

// Volume calculates the volume of the jar in liters
func (m *Jar) Volume() float64 {
	return math.CmToL(float64(m.Width), float64(m.Height), float64(m.Depth))
}

// GetRules creates and returns a slice of validation rules
func (m *Jar) GetRules() []validator.Rule {
	nameLength := validator.NewLengthRule(3, 20, m.Name, "Name")
	descLength := validator.NewLengthRule(0, 250, m.Description, "Description")

	rules := []validator.Rule{
		&nameLength,
		&descLength,
	}
	return rules
}

// Validate runs each rule defined for this jar.
func (m *Jar) Validate() error {
	rules := m.GetRules()
	return m.Model.Validate(rules, "Jar")
}

// ValidateSpecific validates only the given fields.
func (m *Jar) ValidateSpecific(fieldsToValidate ...string) []error {
	rules := m.GetRules()
	tagMap := m.GetDbTagMap(reflect.TypeOf(*m))
	return m.Model.ValidateSpecific(rules, tagMap, fieldsToValidate, "Jar")
}

// Save inserts a new Jar into the database.
func (m *Jar) Save() (interface{}, error) {
	return m.Repo.InsertOne(m)
}

// Update updates an existing jar within the database.
func (m *Jar) Update(fields []string) error {
	dbTagMap := m.GetDbTagMap(reflect.TypeOf(*m))
	jr := m.Repo.(*Repo)
	return jr.updateOneModel(m, dbTagMap, fields)
}

func (m *Jar) setSnails(s []snail.Snail) {
	m.Snails = s
}

// Labels and drawing
func (m *Jar) setSizeLabel() {
	m.SizeLabel = fmt.Sprintf("%f litres (%dcm x %dcm x %dcm)", m.Volume(), m.Width, m.Height, m.Depth)
}

// GenerateAscii creates the ascii representation of the given jar.
func (m *Jar) GenerateAscii(scale float32) string {

	width := int(float32(m.Width) * scale)
	height := int(float32(m.Height)*scale) - 2 // minus two to account for ceiling and floor
	depth := int(float32(m.Depth) * scale)

	wCount := 8*width/10 - 2  // 8 '-' for every 10cm, not counting corners
	hCount := 3 * height / 10 // 3 '|' for every 10cm, not counting corners
	dCount := 1 * depth / 10  // 1 '/' for every 10cm, not counting corners

	corner := "+"
	var lines []string
	preSpaces := dCount + 1
	postSpaces := 0

	// Top back
	s := strings.Repeat(" ", preSpaces) + corner + strings.Repeat("-", wCount) + corner
	lines = append(lines, s)
	dWall := hCount

	// Upper depth slashes
	for i := 0; i < dCount; i++ {
		preSpaces--
		s := strings.Repeat(" ", preSpaces) + "/" + strings.Repeat(" ", wCount) + "/" + strings.Repeat(" ", postSpaces)
		if dWall > 0 {
			s += "|"

		} else {
			if dWall == 0 {
				s += "+"
			} else {
				s += "/"
				dCount--
			}
			postSpaces--
		}
		dWall--

		lines = append(lines, s)
		postSpaces++
	}

	// Top front
	s = corner + strings.Repeat("-", wCount) + corner + strings.Repeat(" ", postSpaces)
	if dWall > 0 {
		s += "|"
		dWall--
	} else {
		if dWall == 0 {
			s += "+"
		} else {
			s += "/"
			dCount--
		}
		postSpaces--
	}
	dWall--
	lines = append(lines, s)

	// Height + bottom depth
	for i := 0; i < hCount; i++ {
		s := "|" + strings.Repeat(" ", wCount) + "|"
		if postSpaces >= 0 {
			s += strings.Repeat(" ", postSpaces)
		}
		if dWall > 0 {
			s += "|"
		} else {
			if dWall == 0 {
				s += "+"
			} else {
				s += "/"
				dCount--
			}
			postSpaces--
		}
		lines = append(lines, s)
		dWall--
	}

	// Floor
	s = corner + strings.Repeat("-", wCount) + corner
	lines = append(lines, s)

	var ascii string
	for _, l := range lines {
		ascii += l + "\n"
	}
	return ascii
}
