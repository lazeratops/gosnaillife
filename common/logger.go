package common

import (
	"fmt"
	"go.uber.org/zap"
	"os"
)

var Log *zap.SugaredLogger
var fileLogs []string

type LogLevel int

const (
	Debug LogLevel = iota
	Info  LogLevel = iota
	Error LogLevel = iota
)

/* var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{shortfile} - %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
) */

func deleteAllLogFiles() {
	for _, path := range fileLogs {
		os.Remove(path)
	}
}

func ResetLog() {
	deleteAllLogFiles()
}

func InitLog(logDir string) (err error) {
	if logDir == "" {
		logDir = os.TempDir() + "/snaillife"
	}
	if _, err := os.Stat(logDir); os.IsNotExist(err) {
		os.Mkdir(logDir, os.ModePerm)
	}
	fullFilePath := logDir + "/sl_log.log"
	_, err = os.Create(fullFilePath)
	if err != nil {
		fmt.Println(err)
		return err
	}

	config := zap.NewProductionConfig()
	config.OutputPaths = []string{
		fullFilePath,
		"stdout",
	}
	logger, _ := config.Build()
	defer logger.Sync()
	Log = logger.Sugar()

	fileLogs = append(fileLogs, fullFilePath)
	return err
}
