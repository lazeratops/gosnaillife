package restapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"io/ioutil"
	http3 "net/http"
)

type jarRes struct {
	HttpStatus http.HttpStatus `json:"http_status"`
	Jars       []jar.Jar       `json:"jars"`
}

type JarReq struct {
	request
	Jar jar.Jar `json:"jar"`
}

type ExportedJarRes struct {
	jarRes *jarRes
}

func (r *ExportedJarRes) JsonEncode(w http3.ResponseWriter) error {
	return json.NewEncoder(w).Encode(r.jarRes)
}

func (r *ExportedJarRes) HttpStatus() http.HttpStatus {
	return r.jarRes.HttpStatus
}

func (r *ExportedJarRes) SetHttpStatus(s http.HttpStatus) {
	if r.jarRes == nil {
		r.jarRes = &jarRes{}
	}
	r.jarRes.HttpStatus = s
}

func (r *ExportedJarRes) SetModels(models []repo.Model) {
	if r.jarRes == nil {
		r.jarRes = &jarRes{}
	}
	for _, m := range models {
		r.jarRes.Jars = append(r.jarRes.Jars, *m.(*jar.Jar))
	}
}

func (r *ExportedJarRes) Models() []repo.Model {
	var ms = make([]repo.Model, len(r.jarRes.Jars))
	for i, s := range r.jarRes.Jars {
		ms[i] = &s
	}
	return ms
}

func GetJarRes(res *http3.Response) (*ExportedJarRes, error) {
	defer res.Body.Close()
	jarRes := jarRes{}
	eJarRes := ExportedJarRes{}
	eJarRes.jarRes = &jarRes
	body, err := ioutil.ReadAll(res.Body)
	if err == nil {
		json.Unmarshal(body, &jarRes)
	} else {
		eJarRes.jarRes.HttpStatus.StatusCode = http3.StatusNotFound
		eJarRes.jarRes.HttpStatus.Message = err.Error()
	}
	return &eJarRes, err
}

func GetJarsFromJarRes(res *http3.Response) ([]jar.Jar, error) {
	if res.StatusCode != http3.StatusOK {
		msg := fmt.Sprintf("Invalid http status: %d", res.StatusCode)
		return nil, errors.New(msg)
	}

	eJarRes, err := GetJarRes(res)
	if err == nil && res.StatusCode == http3.StatusOK {
		return eJarRes.jarRes.Jars, err
	}
	error2.HandleErr(err, "GetJarFromJarRes")
	return nil, err
}
