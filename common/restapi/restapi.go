package restapi

import (
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"net/http"
)

type Responder interface {
	JsonEncode(w http.ResponseWriter) error
	HttpStatus() http2.HttpStatus
	SetHttpStatus(s http2.HttpStatus)
	SetModels(m []repo.Model)
	Models() []repo.Model
}

type Requester interface {
}

type request struct {
	Requester
	FieldsToUpdate map[string]string `json:"fields_to_update"`
	AuthProvider   string            `json:"auth_provider"`
}
