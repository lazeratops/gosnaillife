package restapi

import (
	"encoding/json"
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common/domain/owner"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"io/ioutil"
	http2 "net/http"
)

type ownerRes struct {
	HttpStatus http.HttpStatus `json:"http_status"`
	Owners     []owner.Owner   `json:"owners"`
}

type ExportedOwnerRes struct {
	ownerRes *ownerRes
}

type OwnerReq struct {
	request
	Owner owner.Owner `json:"owner"`
}

func (r *ExportedOwnerRes) JsonEncode(w http2.ResponseWriter) error {
	return json.NewEncoder(w).Encode(r.ownerRes)
}

func (r *ExportedOwnerRes) HttpStatus() http.HttpStatus {
	if r.ownerRes == nil {
		r.ownerRes = &ownerRes{}
	}
	return r.ownerRes.HttpStatus
}

func (r *ExportedOwnerRes) SetHttpStatus(s http.HttpStatus) {
	if r.ownerRes == nil {
		r.ownerRes = &ownerRes{}
	}
	r.ownerRes.HttpStatus = s
}

func (r *ExportedOwnerRes) SetModels(models []repo.Model) {
	if r.ownerRes == nil {
		r.ownerRes = &ownerRes{}
	}
	for _, m := range models {
		r.ownerRes.Owners = append(r.ownerRes.Owners, *m.(*owner.Owner))
	}
}

func (r *ExportedOwnerRes) Models() []repo.Model {
	var ms = make([]repo.Model, len(r.ownerRes.Owners))
	for i, s := range r.ownerRes.Owners {
		ms[i] = &s
	}
	return ms
}

func GetOwnerRes(res *http2.Response) (*ExportedOwnerRes, error) {
	defer res.Body.Close()
	ownerRes := ownerRes{}
	eOwnerRes := ExportedOwnerRes{}
	eOwnerRes.ownerRes = &ownerRes
	body, err := ioutil.ReadAll(res.Body)
	if err == nil {
		json.Unmarshal(body, &ownerRes)
	} else {
		eOwnerRes.ownerRes.HttpStatus.StatusCode = http2.StatusNotFound
		eOwnerRes.ownerRes.HttpStatus.Message = err.Error()
	}
	return &eOwnerRes, err
}

func GetOwnersFromOwnerRes(res *http2.Response) ([]owner.Owner, error) {
	if res.StatusCode != http2.StatusOK {
		msg := fmt.Sprintf("Invalid http status: %d", res.StatusCode)
		return nil, errors.New(msg)
	}

	ownerRes, err := GetOwnerRes(res)
	if err == nil && res.StatusCode == http2.StatusOK {
		return ownerRes.ownerRes.Owners, err
	}
	error2.HandleErr(err, "GetOwnerFromOwnerRes")
	return nil, err
}
