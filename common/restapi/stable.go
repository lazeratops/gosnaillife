package restapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain/stable"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"io/ioutil"
	http3 "net/http"
)

type stableRes struct {
	HttpStatus http.HttpStatus `json:"http_status"`
	Stables    []stable.Stable `json:"stables"`
}

type ExportedStableRes struct {
	stableRes *stableRes
}

type StableReq struct {
	request
	Stable stable.Stable `json:"stable"`
}

func (r *ExportedStableRes) JsonEncode(w http3.ResponseWriter) error {
	return json.NewEncoder(w).Encode(r.stableRes)
}

func (r *ExportedStableRes) HttpStatus() http.HttpStatus {
	if r.stableRes == nil {
		r.stableRes = &stableRes{}
	}
	return r.stableRes.HttpStatus
}

func (r *ExportedStableRes) SetHttpStatus(s http.HttpStatus) {
	if r.stableRes == nil {
		r.stableRes = &stableRes{}
	}
	r.stableRes.HttpStatus = s
}

func (r *ExportedStableRes) SetModels(models []repo.Model) {
	if r.stableRes == nil {
		r.stableRes = &stableRes{}
	}
	for _, m := range models {
		r.stableRes.Stables = append(r.stableRes.Stables, *m.(*stable.Stable))
	}
}

func (r *ExportedStableRes) Models() []repo.Model {
	var ms = make([]repo.Model, len(r.stableRes.Stables))
	for i, s := range r.stableRes.Stables {
		ms[i] = &s
	}
	return ms
}

func GetStableRes(res *http3.Response) (*ExportedStableRes, error) {
	defer res.Body.Close()
	stableRes := stableRes{}
	eStableRes := ExportedStableRes{stableRes: &stableRes}
	body, err := ioutil.ReadAll(res.Body)
	if err == nil {
		json.Unmarshal(body, &stableRes)
	} else {
		stableRes.HttpStatus.StatusCode = http3.StatusNotFound
		stableRes.HttpStatus.Message = err.Error()
	}
	return &eStableRes, err
}

func GetStablesFromStableRes(res *http3.Response) ([]stable.Stable, error) {
	if res.StatusCode != http3.StatusOK {
		msg := fmt.Sprintf("Invalid http status: %d", res.StatusCode)
		return nil, errors.New(msg)
	}

	stableRes, err := GetStableRes(res)
	if err == nil && res.StatusCode == http3.StatusOK {
		return stableRes.stableRes.Stables, err
	}
	error2.HandleErr(err, "GetStableFromStableRes")
	return nil, err
}
