package testing

import (
	"fmt"
	"reflect"
	"time"
)

const (
	AggressiveThreshold time.Duration = 0
	ModerateThreshold   time.Duration = time.Second / 2
	LenientThreshold    time.Duration = time.Second
)

func TestErrorType(wantederr, goterr error) *TestingError {
	if wantederr == nil && goterr == nil {
		return nil
	} else if wantederr == nil && goterr != nil {
		msg := fmt.Sprintf("Wanted no error, got %s", goterr.Error())
		return &TestingError{Err: msg}
	} else if wantederr != nil && goterr == nil {
		msg := fmt.Sprintf("Error expected (%s), but not returned", wantederr.Error())
		return &TestingError{Err: msg}
	}
	errTypeMatch := false
	errT := reflect.TypeOf(goterr)
	desT := reflect.TypeOf(wantederr)
	errTypeMatch = errT == desT
	if !errTypeMatch {
		msg := fmt.Sprintf("Expected %s, got %s - %s", desT, errT, goterr.Error())
		return &TestingError{Err: msg}
	}
	return nil
}

type TestingError struct {
	Err string
}

func (err *TestingError) Error() string {
	return err.Err
}

func TimeElapsed(label string, minDuration time.Duration) func() {
	st := time.Now()
	return func() {
		el := time.Since(st)
		if el >= minDuration {
			fmt.Printf("\n%s ran in %v\n", label, el)
		}
	}
}
