package tests

import (
	"fmt"
	"github.com/go-errors/errors"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"testing"
)

func TestTestErrorType(t *testing.T) {
	testCases := []struct {
		name      string
		err1      error
		err2      error
		wantedErr *testing2.TestingError
	}{
		{"ErrTypeMatch", &TestErr1{"Error 1"}, &TestErr1{"Error 2"}, nil},
		{"ErrTypeMismatch", &TestErr1{"Error 1"}, &TestErr2{"Error 2"}, &testing2.TestingError{Err: "Expected *tests.TestErr1, got *tests.TestErr2 - Error 2"}},
		{"ErrTypeMismatchWithGenericErr", &TestErr1{"Error 1"}, errors.New("An Error!"), &testing2.TestingError{Err: "Expected *tests.TestErr1, got *errors.Error - An Error!"}},
		{"Err1Nil", nil, errors.New("An Error!"), &testing2.TestingError{Err: "Wanted no error, got An Error!"}},
		{"Err2Nil", &TestErr1{"Error incoming"}, nil, &testing2.TestingError{Err: "Error expected (Error incoming), but not returned"}},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			resErr := testing2.TestErrorType(tc.err1, tc.err2)

			if tc.wantedErr == nil && resErr == nil {
				return // pass!
			}

			if tc.wantedErr != nil && resErr == nil || tc.wantedErr == nil && resErr != nil || tc.wantedErr.Error() != resErr.Error() {
				ctx := fmt.Sprintf("wanted '%v', got '%v'", tc.wantedErr, resErr)
				t.Error("", tc.name, ctx)
			}
		})
	}
}

type TestErr1 struct {
	Err string
}

func (err *TestErr1) Error() string {
	return err.Err
}

type TestErr2 struct {
	Err string
}

func (err *TestErr2) Error() string {
	return err.Err
}
