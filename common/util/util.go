package util

import (
	"crypto/sha1"
	"encoding/base32"
	"encoding/base64"
	"encoding/json"
	"io"
	"math/rand"
	"time"
)

func Base64Encode(str []byte) string {
	return base32.StdEncoding.EncodeToString([]byte(str))
}

func Sha256(str string) []byte {
	hasher := sha1.New()
	io.WriteString(hasher, str)
	sha := hasher.Sum(nil)
	return sha
}

func GenRandStr(s int) (string, error) {
	b, err := genRandBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}

func GenRandInt(min int, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min) + min
}

func genRandBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}

// Returns a map of desired elements from provided json.
func FindInJson(j []byte, wanted []string) (m map[string]interface{}, err error) {
	var jdata map[string]interface{}
	err = json.Unmarshal(j, &jdata)
	if len(wanted) == 0 {
		return jdata, err
	}

	// Loop through each json element
	result := make(map[string]interface{})
	result = findKeyInMap(jdata, wanted, result)
	return result, err
}

func findKeyInMap(source map[string]interface{}, wantedkeys []string, result map[string]interface{}) map[string]interface{} {
	// Loop through each json element
	for key, val := range source {
		idx := StrIndexOf(wantedkeys, key)
		if idx > -1 {
			result[key] = val
			RemoveFromStrSlice(wantedkeys, idx)
		} else if submap, ok := val.(map[string]interface{}); ok {
			result = findKeyInMap(submap, wantedkeys, result)
		}
	}
	return result
}

func UInt8ToStr(uint []uint8) string {
	b := make([]byte, len(uint))
	for i, v := range uint {
		b[i] = byte(v)
	}
	return string(b)
}
