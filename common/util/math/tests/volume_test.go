package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/util/math"
	"testing"
)

func TestCmToL(t *testing.T) {
	testCases := []struct {
		name string
		w    float64
		d    float64
		h    float64
		res  float64
	}{
		{"5x5x5", 5, 5, 5, 0.125},
		{"5.5x5x100.213", 5.5, 5, 100.213, 2.756},
		{"5.5x5x100.213", 5.5, 5, 100.213, 2.756},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res := math.CmToL(tc.w, tc.h, tc.d)
			if res != tc.res {
				t.Errorf("\n %s error. Expected answer: %f, got %f", tc.name, tc.res, res)
			}
		})
	}
}
