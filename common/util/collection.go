package util

import "strings"

func AppendIfNotWhitespace(ss []string, val string) []string {
	if strings.TrimSpace(val) != "" {
		ss = append(ss, val)
	}
	return ss
}

func StrSliceEquals(ss1 []string, ss2 []string) bool {
	if len(ss1) != len(ss2) {
		return false
	}
	for i, s := range ss1 {
		if s != ss2[i] {
			return false
		}
	}
	return true
}

func StrIndexOf(slice []string, wanted string) int {
	for i := 0; i < len(slice); i++ {
		e := slice[i]
		if e == wanted {
			return i
		}
	}
	return -1
}

func CaselessStrIndexOf(slice []string, wanted string) int {
	wanted = strings.ToLower(wanted)
	for i := 0; i < len(slice); i++ {
		e := strings.ToLower(slice[i])
		if e == wanted {
			return i
		}
	}
	return -1
}

func RemoveFromStrSlice(slice []string, idx int) []string {
	if idx < 0 || idx > len(slice)-1 {
		return slice
	}
	return append(slice[:idx], slice[idx+1:]...)
}

func FindInStrMap(m map[string]string, toFind string) string {
	if val, ok := m[toFind]; ok {
		return val
	}
	return findByValue(m, toFind)
}

func findByValue(m map[string]string, value string) string {
	for key, val := range m {
		if val == value {
			return key
		}
	}
	return ""
}
