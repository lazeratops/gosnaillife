package tests

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/util"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"testing"
)

func TestStrSliceEquals(t *testing.T) {
	testCases := []struct {
		name      string
		slice1    []string
		slice2    []string
		wantedRes bool
	}{
		{"TestIsEqual", []string{"h", "i", "hello"}, []string{"h", "i", "hello"}, true},
		{"TestMissingElement", []string{"h", "", "hello"}, []string{"h", "hello"}, false},
		{"TestWrongOrder", []string{"124", "dogs", "dog", "kitten"}, []string{"dogs", "124", "dog", "kitten"}, false},
		{"TestBlankSlices", []string{}, []string{}, true},
		{"TestWrongCasing", []string{"Dog"}, []string{"dog"}, false},
		{"TestDiffElements", []string{"Cats", "Are", "COOL"}, []string{"Dogs", "are pretty", "nice"}, false},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res := util.StrSliceEquals(tc.slice1, tc.slice2)
			if res != tc.wantedRes {
				t.Error(testing2.TCFailedMsg("CollectionTest", tc.name, fmt.Sprintf("expected %t, got %t", tc.wantedRes, res)))

			}
		})
	}
}

func TestAppendIfNotWhitespace(t *testing.T) {
	testCases := []struct {
		name        string
		toAppend    string
		wantedSlice []string
	}{
		{"TestAppendNoWhitespace", "thirstyc4rr07!!", []string{"thirstyc4rr07!!"}},
		{"TestAppendMultiWhitespace", "       ", []string{}},
		{"TestAppendEmptyStr", "", []string{}},
		{"TestAppendOneWhitespace", " ", []string{}},
		{"TestAppendSomeWhiteSpace", " sdfd sdf ", []string{" sdfd sdf "}},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s := []string{}
			res := util.AppendIfNotWhitespace(s, tc.toAppend)
			if !util.StrSliceEquals(res, tc.wantedSlice) {
				t.Errorf("\n Test case %s failed; expected %s, got %s", tc.name, tc.wantedSlice, res)
			}
		})
	}
}

func TestStrIndexOf(t *testing.T) {
	testCases := []struct {
		name      string
		ss        []string
		toFind    string
		wantedIdx int
	}{
		{"TestElementFound", []string{"a", "b", "abc"}, "b", 1},
		{"TestElementNotFound", []string{"a", "b", "abc"}, "c", -1},
		{"TestDuplicateElement", []string{"a", "a", "ab"}, "a", 0},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res := util.StrIndexOf(tc.ss, tc.toFind)
			if res != tc.wantedIdx {
				t.Error(testing2.TCFailedMsg("CollectionTest", tc.name, fmt.Sprintf("expected idx %d, got %d", tc.wantedIdx, res)))
			}
		})
	}
}

func TestRemoveFromStrSlice(t *testing.T) {
	testCases := []struct {
		name      string
		ss        []string
		toRemove  int
		wantedRes []string
	}{
		{"TestRemoveFromSlice", []string{"a", "b", "abc"}, 1, []string{"a", "abc"}},
		{"TestRemoveNonexistent", []string{"a", "b", "abc"}, 3, []string{"a", "b", "abc"}},
		{"TestRemoveDuplicate", []string{"a", "a", "ab"}, 0, []string{"a", "ab"}},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res := util.RemoveFromStrSlice(tc.ss, tc.toRemove)
			if !util.StrSliceEquals(tc.wantedRes, res) {
				t.Errorf("\n Test case %s failed; expected %s, got %s", tc.name, tc.wantedRes, res)
			}
		})
	}
}

func TestFindInStrMap(t *testing.T) {
	testCases := []struct {
		name      string
		m         map[string]string
		toFind    string
		wantedRes string
	}{
		{"TestElementFoundInVal", map[string]string{"k1": "a", "k2": "b", "k32": "abc"}, "b", "k2"},
		{"TestElementFoundInKey", map[string]string{"k1": "a", "k2": "b", "k32": "abc"}, "k32", "abc"},
		{"TestElementNotFound", map[string]string{"k1": "a", "k2": "b", "k32": "abc"}, "z", ""},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res := util.FindInStrMap(tc.m, tc.toFind)
			if res != tc.wantedRes {
				t.Error(testing2.TCFailedMsg("CollectionTest", tc.name, fmt.Sprintf("expected %s, got %s", tc.wantedRes, res)))
			}
		})
	}
}
