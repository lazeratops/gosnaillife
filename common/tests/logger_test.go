package tests

import (
	"gitlab.com/drakonka/gosnaillife/common"
	"io/ioutil"
	"os"
	"strings"
	"sync"
	"testing"
	"time"
)

func TestInitLogWithCustomLogDir(t *testing.T) {
	logDir := os.TempDir() + "/customTest"
	err := common.InitLog(logDir)
	if err != nil {
		t.Fatalf("TestInitLogWithCustoLogDir error: %v", err)
		return
	}
	fullLogPath := logDir + "/sl_log.log"
	if _, err := os.Stat(fullLogPath); os.IsNotExist(err) {
		t.Fatalf("TestInitLogWithCustoLogDir; expected to find %s, but this location does no texist.", fullLogPath)
	}
	common.ResetLog()
	_, err = os.Stat(fullLogPath)
	if os.IsExist(err) {
		t.Errorf("TestInitLogWithCustomLogDir error; file %s was meant to be deleted, but still exists", fullLogPath)
	}
}

func TestInitLogWithDefaultLogDir(t *testing.T) {
	err := common.InitLog("")
	if err != nil {
		t.Fatalf("TestInitLogWithDefaultLogDir error: %v", err)
		return
	}
	common.ResetLog()
	fullLogPath := os.TempDir() + "/snaillife/sl_log.log"
	_, err = os.Stat(fullLogPath)
	if os.IsExist(err) {
		t.Errorf("TestInitLogWithDefaultLogDir error; file %s was meant to be deleted, but still exists", fullLogPath)
	}
}

func TestInfo(t *testing.T) {
	err := common.InitLog("/tmp/custom")
	if err != nil {
		t.Error(err)
	}
	logContents := "Test log"
	common.Log.Info(logContents)
	common.Log.Sync()
	fullLogPath := "/tmp/custom/sl_log.log"

	var b []byte
	b, err = ioutil.ReadFile(fullLogPath)
	if err != nil {
		t.Errorf("\nTestInfo fail: %v", err)
	} else {
		s := string(b)
		if !strings.Contains(s, logContents) {
			t.Errorf("\n Didn't find expected contents in the file log; expected: %s, received: %s", logContents, s)
		}
	}
	common.ResetLog()
	_, err = os.Stat(fullLogPath)
	if os.IsExist(err) {
		t.Errorf("TestInitLogWithCustomLogDir error; file %s was meant to be deleted, but still exists", fullLogPath)
	}
}

func TestLoggingFromGoroutines(t *testing.T) {
	err := common.InitLog("/tmp/goroutineTest")
	if err != nil {
		t.Error(err)
	}
	wg := sync.WaitGroup{}
	wg.Add(3)
	go func() {
		for i := 0; i < 100; i++ {
			common.Log.Infof("Testing goroutine 1, pass %d", i)
		}
		wg.Done()
	}()

	go func() {
		for i := 0; i < 100; i++ {
			common.Log.Infof("Testing goroutine 2, pass %d", i)
		}
		wg.Done()
	}()

	time.Sleep(time.Second * time.Duration(5))
	go func() {
		for i := 0; i < 100; i++ {
			common.Log.Errorf("Testing goroutine 3, pass %d", i)
			common.Log.Sync()
		}
		wg.Done()
	}()
	wg.Wait()
	common.Log.Sync()
	common.ResetLog()
}
