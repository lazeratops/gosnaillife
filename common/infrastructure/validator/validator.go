package validator

import (
	"fmt"
	"reflect"
)

type Validatable interface {
	GetRules() []Rule
	Validate() error
	ValidateSpecific(fields ...string) []error
}

type Rule interface {
	Validate(name string) error
	GetFieldName() string
}

type rule struct {
	fieldName  string
	objectName string
}

func (r *rule) GetFieldName() string {
	return r.fieldName
}

func isNumeric(value interface{}) bool {
	v := reflect.ValueOf(value)
	switch v.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Float32, reflect.Float64:
		return true
	}
	return false
}

func getNumericAsFloat(value interface{}) (float64, error) {
	v := reflect.ValueOf(value).Interface()
	switch n := v.(type) {
	case int:
		return float64(n), nil
	case int8:
		return float64(n), nil
	case int16:
		return float64(n), nil
	case int32:
		return float64(n), nil
	case int64:
		return float64(n), nil
	case uint:
		return float64(n), nil
	case uint8:
		return float64(n), nil
	case uint16:
		return float64(n), nil
	case uint32:
		return float64(n), nil
	case uint64:
		return float64(n), nil
	case float32:
		return float64(n), nil
	case float64:
		return n, nil
	}
	return 0, &ValidationError{Err: "Not a number", rulename: ""}
}

type ValidationError struct {
	Err      string
	rulename string
}

func (e *ValidationError) Error() string {
	return fmt.Sprintf("Rule %s validation fail: %s", e.rulename, e.Err)
}
