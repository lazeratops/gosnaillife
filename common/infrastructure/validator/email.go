package validator

import (
	"fmt"
	"strings"
)

type EmailRule struct {
	rule
	val interface{}
}

func NewEmailRule(val interface{}, fieldName string) EmailRule {
	r := EmailRule{
		val: val,
	}
	r.fieldName = fieldName
	return r
}

func (r *EmailRule) GetMessage() string {
	return fmt.Sprintf("%s %s invalid - field has to be an email", r.objectName, r.fieldName)
}

func (r *EmailRule) Validate(name string) error {
	r.objectName = name

	if value, ok := r.val.(string); ok {
		parts := strings.Split(value, "@")
		if len(parts) == 1 {
			return &ValidationError{
				rulename: name,
				Err:      r.GetMessage(),
			}
		}
		domain := parts[1]
		if !strings.ContainsRune(domain, '.') {
			return &ValidationError{
				rulename: name,
				Err:      r.GetMessage(),
			}
		}
		return nil
	}
	return fmt.Errorf("Cannot run email validation; the value is not a string.")

}
