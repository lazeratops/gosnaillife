package restapi

import (
	"github.com/gorilla/mux"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

// NewRouter creates a new router and registers all the routes
func NewRouter(app *env.Application) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range getRoutes(app) {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)
		handler = handleCORS(handler)
		router.
			Methods(route.Method, "OPTIONS").
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	return router
}

func handleCORS(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlers.WriteCORSResponse(w)
		if r.Method == http.MethodOptions {
			return
		}
		handler.ServeHTTP(w, r)
	})
}
