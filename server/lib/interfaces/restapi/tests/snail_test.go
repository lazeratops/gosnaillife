package tests

import (
	"errors"
	"fmt"
	"github.com/fatih/structs"
	"github.com/golang/mock/gomock"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo/mocks"
	snail2 "gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers/snail"
	http2 "net/http"
	"testing"
)

type createSnailTestCase struct {
	tcname             string
	snail              snail.Snail
	expectedHttpStatus http.HttpStatus
	authReturnErr      error
	idToReturn         int
}

func prepCreateSnailTestCases() []createSnailTestCase {
	var testCases []createSnailTestCase
	tc1 := createSnailTestCase{
		tcname:             "SuccessfulNonRandomCreation",
		expectedHttpStatus: http.HttpStatus{StatusCode: http2.StatusOK},
		authReturnErr:      nil,
		idToReturn:         5,
	}
	s := snail.Snail{Name: "Hansel"}
	outerShell := organ.NewOuterShell()
	s.Organs = append(s.Organs, &outerShell)
	tc1.snail = s
	testCases = append(testCases, tc1)

	tc2 := createSnailTestCase{
		tcname:             "SuccessfulRandomCreation",
		expectedHttpStatus: http.HttpStatus{StatusCode: http2.StatusOK},
		authReturnErr:      nil,
		snail:              snail.Snail{},
		idToReturn:         7,
	}
	testCases = append(testCases, tc2)

	tc3 := createSnailTestCase{
		tcname:             "RandomCreationUnauthorized",
		expectedHttpStatus: http.HttpStatus{StatusCode: http2.StatusUnauthorized, Message: "Unauthorized"},
		authReturnErr:      errors.New("Unauthorized"),
		snail:              snail.Snail{},
		idToReturn:         0,
	}
	testCases = append(testCases, tc3)

	tc4 := createSnailTestCase{
		tcname:             "RandomCreationOtherFail",
		expectedHttpStatus: http.HttpStatus{StatusCode: http2.StatusBadRequest, Message: "Some Other Fail"},
		authReturnErr:      nil,
		snail:              snail.Snail{},
		idToReturn:         0,
	}
	testCases = append(testCases, tc4)
	return testCases
}

func TestCreateSnail(t *testing.T) {
	testCases := prepCreateSnailTestCases()

	ctrl := gomock.NewController(t)
	mockRepo := mocks.NewMockRepo(ctrl)

	for _, tc := range testCases {
		fmt.Printf("Running TestCreateSnail Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			app := env.NewApplication()
			app.Repos["snail"] = mockRepo
			mockP, mockDb := setMockProviderAndMockDb(ctrl, app)
			mockP.EXPECT().Authenticate(gomock.Any()).Return(nil)

			mockRepo.EXPECT().InsertOne(gomock.Any()).Return(tc.idToReturn, nil)

			mockDb.EXPECT().GetRows(
				gomock.Any(),
				gomock.Any(),
				gomock.Any(),
				gomock.Any()).MaxTimes(1)

			// Create request
			m := tc.snail
			req := restapi.SnailReq{
				Snail: m,
			}
			req.AuthProvider = "MockProvider"

			res, err := makeRequest(&req, t, func(writer http2.ResponseWriter, request *http2.Request) {
				snail2.CreateSnail(writer, request, app)
			})
			if err != nil {
				t.Fatal("Could not make request: " + err.Error())
				return
			}
			insertedSnails, err := restapi.GetSnailsFromSnailRes(res.Result())
			var insertedSnail *snail.Snail
			if len(insertedSnails) == 1 {
				insertedSnail = &insertedSnails[0]
				insertedSnail.Id = int(tc.idToReturn)
			}
			if tc.idToReturn > 0 {
				if err != nil {
					t.Errorf("Test Fail (%s): %v", tc.tcname, err)
				} else if insertedSnail.Id != int(tc.idToReturn) {
					t.Errorf("\nTest fail (%s): Snails don't match! Expected ID %d and name %s, got ID %d and name %s", tc.tcname, tc.idToReturn, tc.snail.Name, insertedSnail.Id, insertedSnail.Name)
				}
			} else if tc.idToReturn <= 0 && insertedSnail != nil {
				t.Errorf("Test fail (%s): Expected invalid snail, but got ID %d", tc.tcname, int(tc.idToReturn))
			}
		})
	}
}

func testGetSnails(testCases []struct {
	tcname        string
	authreturnerr error
	want          []databases.RowResult
},
	t *testing.T,
	handleFunc func(http2.ResponseWriter, *http2.Request, *env.Application)) {

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			app := env.NewApplication()
			provider, mockDb := setMockProviderAndMockDb(ctrl, app)
			provider.EXPECT().Authenticate(gomock.AssignableToTypeOf(&http2.Request{})).Return(tc.authreturnerr)
			mockDb.EXPECT().GetRows(
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf([]interface{}{}),
				gomock.AssignableToTypeOf([]string{})).Return(tc.want, nil).MaxTimes(3)

			url := "http://localhost:42039/api/jars/1/snails?fields=name"

			res, err := makeGetRowsRequest(url, func(writer http2.ResponseWriter, request *http2.Request) {
				handleFunc(writer, request, app)
			})
			if err != nil {
				t.Fatal(err)
			}

			snails, err := restapi.GetSnailsFromSnailRes(res)

			wantedSnails := getWantedSnails(tc, t)

			if !cmp.Equal(
				snails,
				wantedSnails,
				cmp.Options{
					cmpopts.IgnoreTypes(map[string]organism.Gene{}),
					cmpopts.IgnoreUnexported(domain.Model{}),
				}) {
				t.Errorf("\n Error getting snails; wanted: %v, got %v", wantedSnails, snails)
			}
		})
	}
}

func getWantedSnails(tc struct {
	tcname        string
	authreturnerr error
	want          []databases.RowResult
}, t *testing.T) []snail.Snail {

	var wantedSnails []snail.Snail
	for _, s := range tc.want {
		snail, err := snail.GetSnailFromRowResult(s)
		if err != nil {
			t.Error("Failed getting jar from row result")
		}
		wantedSnails = append(wantedSnails, *snail)
	}

	return wantedSnails
}

func TestUpdateSnail(t *testing.T) {
	testCases := []struct {
		tcname             string
		id                 int
		fieldsToUpdate     map[string]string
		expectedStatusCode int
		errToReturn        error
	}{
		{
			"SuccessfulUpdate",
			2,
			map[string]string{"name": "newname"},
			http2.StatusOK,
			nil,
		},
		{
			"FailedValidationUpdate",
			2,
			map[string]string{"name": "_"},
			http2.StatusBadRequest,
			nil,
		},
	}
	for _, tc := range testCases {
		fmt.Printf("Running TestUpdateSnail Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			app := env.NewApplication()
			ctrl := gomock.NewController(t)
			mockProvider, mockDb := setMockProviderAndMockDb(ctrl, app)
			app.Authenticator.Providers["mockprovider"] = mockProvider
			app.Database = mockDb
			mockProvider.EXPECT().Authenticate(gomock.AssignableToTypeOf(&http2.Request{}))
			mockDb.EXPECT().Update(
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf(make(map[string]string)),
			).Return(tc.errToReturn)

			mockDb.EXPECT().GetRows(
				gomock.Any(),
				gomock.Any(),
				gomock.Any(),
				gomock.Any()).MaxTimes(3)
			originalSnail := snail.Snail{Id: 2}
			s := structs.New(originalSnail)
			s.TagName = "db"

			snailMap := s.Map()

			var rowRes databases.RowResult
			rowRes = snailMap
			mockDb.EXPECT().GetRow(
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf([]string{}),
			).Return(rowRes, nil).MaxTimes(2)

			req := restapi.SnailReq{}
			req.FieldsToUpdate = tc.fieldsToUpdate
			req.AuthProvider = "MockProvider"

			res, err := makeRequest(&req, t, func(writer http2.ResponseWriter, request *http2.Request) {
				snail2.Update(writer, request, app)
			})
			if err != nil {
				t.Fatal("Could not make request: " + err.Error())
				return
			}
			snailRes, err := restapi.GetSnailRes(res.Result())
			if err != nil {
				t.Errorf("Error: %s", err.Error())
			} else if snailRes.HttpStatus().StatusCode != tc.expectedStatusCode {
				t.Errorf("Unexpected status code; expected %d, got %d: %s", tc.expectedStatusCode, snailRes.HttpStatus().StatusCode, snailRes.HttpStatus().Message)
			}
		})
	}
}
