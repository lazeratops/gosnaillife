package tests

import (
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	jar2 "gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers/jar"
	http2 "net/http"
	"testing"
)

func TestCreateJar(t *testing.T) {
	testCases := []struct {
		tcname             string
		jar                jar.Jar
		expectedHttpStatus http.HttpStatus
		authReturnErr      error
		idToReturn         int64
	}{
		{
			tcname: "SuccessfulCreation",
			jar: jar.Jar{
				Name:     "Oakwood Jar",
				OwnerId:  2,
				StableId: 1,
			},
			expectedHttpStatus: http.HttpStatus{StatusCode: http2.StatusOK},
			authReturnErr:      nil,
			idToReturn:         1,
		},
		{
			tcname: "FailedAuthCreation",
			jar: jar.Jar{
				Name:     "FailAuth Jar",
				OwnerId:  3,
				StableId: 1,
			},
			expectedHttpStatus: http.HttpStatus{StatusCode: http2.StatusUnauthorized, Message: "Unauthorized"},
			authReturnErr:      errors.New("Unauthorized yo"),
			idToReturn:         0,
		},
		{
			tcname: "FailedOtherCreation",
			jar: jar.Jar{
				Name:     "OtherFail Test",
				OwnerId:  1,
				StableId: 1,
			},
			expectedHttpStatus: http.HttpStatus{StatusCode: http2.StatusBadRequest, Message: "Some Other Fail"},
			authReturnErr:      nil,
			idToReturn:         0,
		},
	}
	app := env.NewApplication()

	ctrl := gomock.NewController(t)
	mockProvider, mockDb := setMockProviderAndMockDb(ctrl, app)
	app.Authenticator.Providers["mockprovider"] = mockProvider

	for _, tc := range testCases {
		fmt.Printf("Running TestCreateJar Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {

			mockProvider.EXPECT().Authenticate(gomock.AssignableToTypeOf(&http2.Request{})).Return(tc.authReturnErr)
			mockDb.EXPECT().Insert(
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf(make(map[string]string))).Return(tc.idToReturn, nil)

			// Create owner request
			j := tc.jar
			jarReq := restapi.JarReq{
				Jar: j,
			}
			jarReq.AuthProvider = "MockProvider"
			res, err := makeRequest(&jarReq, t, func(writer http2.ResponseWriter, request *http2.Request) {
				jar2.CreateJar(writer, request, app)
			})
			if err != nil {
				t.Fatal("Could not make request: " + err.Error())
				return
			}
			insertedJars, err := restapi.GetJarsFromJarRes(res.Result())
			var insertedJar *jar.Jar
			if len(insertedJars) == 1 {
				insertedJar = &insertedJars[0]
			}
			if tc.idToReturn > 0 {
				if err != nil {
					t.Error(err)
				} else if insertedJar.Id != int(tc.idToReturn) {
					t.Errorf("\nJars don't match! Expected ID %d, got %d", tc.idToReturn, insertedJar.Id)
				}
			} else if tc.idToReturn <= 0 && insertedJar != nil {
				t.Errorf("Expected invalid jar, but got ID %d", int(tc.idToReturn))
			}
		})
	}
}

func TestGetSnailsInJar(t *testing.T) {

	testCases := []struct {
		tcname        string
		authreturnerr error
		want          []databases.RowResult
	}{
		{"SnailsFoundInJar",
			nil,
			[]databases.RowResult{
				{"Name": "Snaily Snail"},
			},
		},
		{
			"SnailsNotFound",
			errors.New("invalid scope"),
			[]databases.RowResult{},
		},
	}

	testGetSnails(testCases, t, jar2.GetSnails)
}

func testGetJars(testCases []struct {
	tcname        string
	authreturnerr error
	want          []databases.RowResult
},
	t *testing.T,
	url string,
	handleFunc func(http2.ResponseWriter, *http2.Request, *env.Application)) {
	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			app := env.NewApplication()
			p, db := setMockProviderAndMockDb(ctrl, app)
			p.EXPECT().Authenticate(gomock.Any()).Return(nil)
			db.EXPECT().GetRows(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(tc.want, nil).MaxTimes(2)
			res, err := makeGetRowsRequest(url, func(writer http2.ResponseWriter, request *http2.Request) {
				handleFunc(writer, request, app)
			})
			if err != nil {
				t.Fatalf(testing2.TCFailedMsg("JarTest", tc.tcname, err))
			}
			jars, err := restapi.GetJarsFromJarRes(res)

			wantedJars := getWantedJars(tc, t)

			if !cmp.Equal(
				jars,
				wantedJars,
				cmp.Options{
					cmpopts.IgnoreUnexported(jar.Jar{}),
					cmpopts.IgnoreUnexported(domain.Model{}),
					cmpopts.IgnoreFields(jar.Jar{}, "Snails"),
				}) {
				t.Errorf("\n Error getting jars; wanted: %v, got %v", wantedJars, jars)
			}
		})
	}
}

func getWantedJars(tc struct {
	tcname        string
	authreturnerr error
	want          []databases.RowResult
}, t *testing.T) []jar.Jar {

	var wantedJars []jar.Jar
	for _, s := range tc.want {
		jar, err := jar.GetJarFromRowResult(s)
		if err != nil {
			t.Error("Failed getting jar from row result")
		}
		wantedJars = append(wantedJars, *jar)
	}

	return wantedJars
}
