package tests

import (
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	stable2 "gitlab.com/drakonka/gosnaillife/common/domain/stable"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	stable3 "gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers/stable"
	http2 "net/http"
	"testing"
)

func TestCreateStable(t *testing.T) {

	testCases := []struct {
		tcname             string
		stableName         string
		ownerId            int
		expectedHttpStatus http.HttpStatus
		authReturnErr      error
		idToReturn         int64
	}{
		{"SuccessfulCreation",
			"Oakwood Stables",
			2,
			http.HttpStatus{StatusCode: http2.StatusOK},
			nil,
			1,
		},
		{
			"FailedAuthCreation",
			"FailAuth Stables",
			3,
			http.HttpStatus{StatusCode: http2.StatusUnauthorized, Message: "Unauthorized"},
			errors.New("Unauthorized yo"),
			0,
		},
		{
			"FailedOtherCreation",
			"OtherFail Test",
			1,
			http.HttpStatus{StatusCode: http2.StatusBadRequest, Message: "Some Other Fail"},
			nil,
			0,
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestCreateStable Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			ctrl := gomock.NewController(t)

			app := env.NewApplication()
			mockProvider, mockDb := setMockProviderAndMockDb(ctrl, app)
			// Prep auth call
			mockProvider.EXPECT().Authenticate(gomock.AssignableToTypeOf(&http2.Request{})).Return(tc.authReturnErr)

			// Prep db call
			mockDb.EXPECT().Insert(gomock.AssignableToTypeOf(""), gomock.AssignableToTypeOf(make(map[string]string))).Return(tc.idToReturn, nil)

			// Create owner request
			stable := stable2.Stable{
				Name:    tc.stableName,
				OwnerId: tc.ownerId,
			}
			stableReq := restapi.StableReq{
				Stable: stable,
			}
			stableReq.AuthProvider = "MockProvider"
			h := func(writer http2.ResponseWriter, request *http2.Request) {
				stable3.CreateStable(writer, request, app)
			}
			res, err := makeRequest(stableReq, t, h)
			if err != nil {
				t.Fatal(err)
			}
			if res.Code != tc.expectedHttpStatus.StatusCode {
				t.Fatalf("Expected status code %d, got %d", tc.expectedHttpStatus.StatusCode, res.Code)
			}
			if res.Code != http2.StatusOK {
				return
			}
			insertedStables, err := restapi.GetStablesFromStableRes(res.Result())
			if err != nil {
				t.Fatal(err)
			}
			var insertedStable *stable2.Stable
			if len(insertedStables) == 1 {
				insertedStable = &insertedStables[0]
			}
			if tc.idToReturn > 0 {
				if err != nil {
					t.Error(err)
				} else if insertedStable.Id != int(tc.idToReturn) {
					t.Errorf("\nStables don't match! Expected it %d, got %d", tc.idToReturn, insertedStable.Id)
				}
			} else if tc.idToReturn <= 0 && insertedStable != nil {
				t.Errorf("Expected invalid stable, but got ID %d", int(tc.idToReturn))
			}
		})
	}
}

func TestGetJarsWithFieldsByStable(t *testing.T) {
	testCases := []struct {
		tcname        string
		authreturnerr error
		want          []databases.RowResult
	}{
		{"JarsFound",
			nil,
			[]databases.RowResult{
				{"Name": "The One"},
			},
		},
		{
			"JarsNotFound",
			errors.New("Invalid scope"),
			[]databases.RowResult{},
		},
	}
	url := "http://localhost:42039/api/stables/1/jars?fields=name"
	testGetJars(testCases, t, url, stable3.GetJars)
}
