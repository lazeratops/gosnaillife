package tests

import (
	"encoding/json"
	"fmt"
	"github.com/go-errors/errors"
	"github.com/golang/mock/gomock"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/owner"
	restapi2 "gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers/user"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestUserRegister(t *testing.T) {
	testCases := []struct {
		tcname   string
		username string
		password string
		toreturn restapi2.UserRes
		want     int
	}{
		{"ValidRegistration",
			"test@test.com",
			"testpass",
			restapi2.UserRes{Httpstatus: http2.HttpStatus{StatusCode: 200}},
			http.StatusOK,
		},
		{
			"InvalidRegistration",
			"test@test.com",
			"testpass",
			restapi2.UserRes{Httpstatus: http2.HttpStatus{StatusCode: 401, Message: "Login Failed"}},
			http.StatusUnauthorized,
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestUserRegistration Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			app := env.NewApplication()
			mockProvider := setMockProvider(ctrl, app)
			mockProvider.EXPECT().SetResToMarshalTo(gomock.AssignableToTypeOf(&restapi2.UserRes{}))
			marshaledReturn, _ := json.Marshal(tc.toreturn)
			mockProvider.EXPECT().Register(gomock.AssignableToTypeOf(""), gomock.AssignableToTypeOf("")).Return(marshaledReturn, nil)

			fmt.Println("Mock provider done")
			var reader io.Reader

			reqStr := fmt.Sprintf("{\"username\":\"%s\",\"password\":\"%s\",\"auth_provider\":\"MockProvider\",\"connection\":\"Username-Password-Authentication\"}", tc.username, tc.password)
			reader = strings.NewReader(reqStr) //Convert string to reader
			request, err := http.NewRequest("POST", "", reader)

			handler := http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
				user.CreateUser(writer, request, app)
			})
			rr := httptest.NewRecorder()
			handler.ServeHTTP(rr, request)

			if err != nil {
				t.Error(err) //Something is wrong while sending request
			}

			if rr.Code != tc.want {
				var msg = fmt.Sprintf("Unexpected response status code; Received: %d, Expected: %d", rr.Code, tc.want)
				t.Error(msg)
			}
		})
	}
}

func TestUserLogin(t *testing.T) {

	testCases := []struct {
		tcname   string
		username string
		password string
		toreturn restapi2.UserRes
		want     int
	}{
		{"ValidLogin",
			"test@test.com",
			"testpass",
			restapi2.UserRes{Httpstatus: http2.HttpStatus{StatusCode: 200}},
			http.StatusOK,
		},
		{
			"InvalidLogin",
			"test@test.com",
			"testpass",
			restapi2.UserRes{Httpstatus: http2.HttpStatus{StatusCode: http.StatusUnauthorized, Message: "Unauthorized"}},
			http.StatusUnauthorized,
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestUserLogin Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			marshaledReturn, _ := json.Marshal(tc.toreturn)
			ctrl := gomock.NewController(t)
			app := env.NewApplication()
			mockProvider, _ := setMockProviderAndMockDb(ctrl, app)
			mockProvider.EXPECT().Login(gomock.AssignableToTypeOf(""), gomock.AssignableToTypeOf("")).Return(marshaledReturn, nil)

			var reader io.Reader
			reqStr := fmt.Sprintf("{\"username\":\"%s\",\"password\":\"%s\",\"auth_provider\":\"MockProvider\",\"connection\":\"Username-Password-Authentication\"}", tc.username, tc.password)
			reader = strings.NewReader(reqStr) //Convert string to reader
			request, err := http.NewRequest("POST", "", reader)
			if err != nil {
				t.Fatal(err)
			}

			h := func(writer http.ResponseWriter, request *http.Request) {
				user.LoginUser(writer, request, app)
			}
			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(h)
			handler.ServeHTTP(rr, request)

			if rr.Code != tc.want {
				var msg = fmt.Sprintf("Unexpected response status code; Received: %d, Expected: %d", rr.Code, tc.want)
				t.Error(msg)
			}
		})
	}
}

func TestGetAuthStatus(t *testing.T) {

	testCases := []struct {
		tcname    string
		returnerr error
		want      int
	}{
		{"AuthenticatedStatus",
			nil,
			http.StatusOK,
		},
		{
			"InvalidLogin",
			errors.New("Invalid scope"),
			http.StatusUnauthorized,
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestGetAuthStatus - %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			app := env.NewApplication()
			mockProvider := setMockProvider(ctrl, app)

			var reader io.Reader

			request, err := http.NewRequest("GET", "", reader)
			if err != nil {
				t.Fatal(err)
			}
			request.Header.Add("auth_provider", "MockProvider")
			q := request.URL.Query()
			q.Add("connection", "Username-Password-Authentication")
			request.URL.RawQuery = q.Encode()

			mockProvider.EXPECT().Authenticate(gomock.Any()).Return(tc.returnerr)

			h := func(writer http.ResponseWriter, request *http.Request) {
				user.TestAuth(writer, request, app)
			}
			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(h)
			handler.ServeHTTP(rr, request)

			if rr.Code != tc.want {
				var msg = fmt.Sprintf("Unexpected response status code; Received: %d, Expected: %d", rr.Code, tc.want)
				t.Fatal(msg)
			}
		})
	}
}

func TestGetOwner(t *testing.T) {

	testCases := []struct {
		tcname        string
		authreturnerr error
		want          []databases.RowResult
	}{
		{"OwnerFound",
			nil,
			[]databases.RowResult{
				{"firstname": "Jimmy"},
			},
		},
		{
			"OwnerNotFound",
			errors.New("Invalid scope"),
			[]databases.RowResult{},
		},
	}
	url := "http://localhost:42039/api/users/2/owners?fields=name"

	for _, tc := range testCases {
		fmt.Printf("Running TestGetOwner - %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			app := env.NewApplication()
			mockProvider, mockDb := setMockProviderAndMockDb(ctrl, app)
			var reader io.Reader
			request, err := http.NewRequest("GET", url, reader)
			if err != nil {
				t.Fatal(err)
			}

			request.Header.Add("auth_provider", "MockProvider")

			mockProvider.EXPECT().Authenticate(gomock.Any()).Return(tc.authreturnerr)

			mockDb.EXPECT().GetRows(
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf([]interface{}{}),
				gomock.AssignableToTypeOf([]string{})).Return(tc.want, nil).MaxTimes(6)

			h := func(writer http.ResponseWriter, request *http.Request) {
				user.GetOwner(writer, request, app)
			}

			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(h)
			handler.ServeHTTP(rr, request)

			owners, err := restapi2.GetOwnersFromOwnerRes(rr.Result())
			var o *owner.Owner
			if len(owners) == 1 {
				o = &owners[0]
			}
			wantedOwner := getWantedOwner(tc, t)

			if !cmp.Equal(
				o,
				wantedOwner,
				cmp.Options{
					cmpopts.IgnoreUnexported(owner.Owner{}),
					cmpopts.IgnoreUnexported(domain.Model{}),
					cmpopts.IgnoreFields(owner.Owner{}, "Stables"),
				}) {
				t.Errorf("\n Error getting owner; wanted: %v, got %v", wantedOwner, o)
			}
		})
	}
}

func getWantedOwner(tc struct {
	tcname        string
	authreturnerr error
	want          []databases.RowResult
}, t *testing.T) *owner.Owner {
	var wantedOwner *owner.Owner
	if len(tc.want) > 0 {
		wo, err := owner.GetOwnerFromRowResult(tc.want[0])
		if err != nil {
			t.Error("Failed getting owner from row result")
		}
		wantedOwner = wo
	} else {
		wantedOwner = nil
	}
	return wantedOwner
}
