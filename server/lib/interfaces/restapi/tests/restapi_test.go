package tests

import (
	"encoding/json"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth/mocks"
	mocks2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases/mocks"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

func TestMain(m *testing.M) {
	defer testing2.TimeElapsed("RestAPI", testing2.ModerateThreshold)()
	ret := m.Run()
	os.Exit(ret)
}

func makeRequest(req restapi.Requester, t *testing.T, h func(http.ResponseWriter, *http.Request)) (*httptest.ResponseRecorder, error) {
	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(h)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method

	reqStr, err := json.Marshal(req)
	if err != nil {
		t.Fatal("Could not marshal request: " + err.Error())
		return nil, err
	}
	var reader io.Reader
	reader = strings.NewReader(string(reqStr))
	//Convert string to reader
	request, err := http.NewRequest("POST", "/url", reader)
	request = mux.SetURLVars(request, map[string]string{
		"id": "2",
	})
	request.Header.Add("auth_provider", "MockProvider")
	// Send request
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, request)
	return rr, nil
}

func makeGetRowsRequest(url string, handleFunc http.HandlerFunc) (*http.Response, error) {
	var reader io.Reader
	request, err := http.NewRequest("GET", url, reader)
	request.Header.Add("auth_provider", "MockProvider")
	request = mux.SetURLVars(request, map[string]string{
		"id": "2",
	})

	rr := httptest.NewRecorder()
	handleFunc.ServeHTTP(rr, request)

	return rr.Result(), err
}

func setMockProvider(ctrl *gomock.Controller, app *env.Application) *mocks.MockProvider {
	mockProvider := mocks.NewMockProvider(ctrl)
	app.Authenticator.Providers["mockprovider"] = mockProvider
	return mockProvider
}

func setMockDb(ctrl *gomock.Controller, app *env.Application) *mocks2.MockDatabase {
	mockDb := mocks2.NewMockDatabase(ctrl)
	app.Database = mockDb
	return mockDb
}

func setMockProviderAndMockDb(ctrl *gomock.Controller, app *env.Application) (*mocks.MockProvider, *mocks2.MockDatabase) {
	p := setMockProvider(ctrl, app)
	db := setMockDb(ctrl, app)
	return p, db
}
