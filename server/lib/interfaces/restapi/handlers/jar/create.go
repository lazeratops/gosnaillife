package jar

import (
	"fmt"
	jar2 "gitlab.com/drakonka/gosnaillife/common/domain/jar"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func CreateJar(w http.ResponseWriter, r *http.Request, app *env.Application) {
	// Authenticate
	ap := r.Header.Get("auth_provider")
	authErr := app.Authenticator.AuthenticateUser(ap, r)

	if authErr != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	jarReq, err := getJarReq(r)
	handlers.HandleReqErr(err, w)
	jarRes := createJar(jarReq, app)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(jarRes.HttpStatus().StatusCode)
	if handlers.BodyAllowedForStatus(jarRes.HttpStatus().StatusCode) {
		err = jarRes.JsonEncode(w)
		if err != nil {
			fmt.Printf("\n Error when encoding res: %v", err)
		}
	}
}

func createJar(jarReq restapi.JarReq, app *env.Application) (jarRes restapi.ExportedJarRes) {
	jar := jarReq.Jar
	eRes := restapi.ExportedJarRes{}

	repo, err := app.GetOrMakeRepo("jar", jar2.NewRepo)
	if err != nil {
		status := http2.HttpStatus{StatusCode: http.StatusInternalServerError, Message: "Couldn't find or generate repo"}
		eRes.SetHttpStatus(status)
		return eRes
	}

	res := handlers.CreateModel(&jar, &eRes, repo)
	return *res.(*restapi.ExportedJarRes)
}
