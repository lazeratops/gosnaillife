package jar

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func GetSnails(w http.ResponseWriter, r *http.Request, app *env.Application) {
	handlers.GetModels(w, r, app, findSnails)
}

func findSnails(jarId string, fields []string, app *env.Application) (restapi.Responder, error) {
	res := &restapi.ExportedSnailRes{}
	repo, _ := snail.NewRepo(app.Database)
	snails, err := repo.GetMany("current_jar_id=?", []interface{}{jarId}, fields)
	var status http2.HttpStatus
	if len(snails) == 0 {
		status.StatusCode = http.StatusNoContent
		status.Message = "This jra doesn't have any snails."
	} else {
		var models []repo2.Model
		for _, s := range snails {
			snail := s.(*snail.Snail)
			models = append(models, snail)
		}
		res.SetModels(models)
		status.StatusCode = http.StatusOK
	}
	res.SetHttpStatus(status)
	return res, err
}
