package snail

import (
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func getSnailReq(r *http.Request) (restapi.SnailReq, error) {
	var snail restapi.SnailReq
	err := handlers.GetModelReq(r, &snail)
	return snail, err
}
