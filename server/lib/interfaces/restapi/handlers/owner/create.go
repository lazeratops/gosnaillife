package owner

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain/owner"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func CreateOwner(w http.ResponseWriter, r *http.Request, app *env.Application) {
	// Authenticate
	ap := r.Header.Get("auth_provider")
	authErr := app.Authenticator.AuthenticateUser(ap, r)

	if authErr != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	ownerReq, err := getOwnerReq(r)
	handlers.HandleReqErr(err, w)
	ownerRes := createOwner(ownerReq, app)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(ownerRes.HttpStatus().StatusCode)
	if ownerRes.HttpStatus().StatusCode == 200 {
		err = ownerRes.JsonEncode(w)
		if err != nil {
			fmt.Printf("\n Error when encoding owner res: %v", err)
		}
	}
}

func createOwner(ownerReq restapi.OwnerReq, app *env.Application) (ownerRes restapi.ExportedOwnerRes) {
	m := ownerReq.Owner
	eRes := restapi.ExportedOwnerRes{}

	repo, err := app.GetOrMakeRepo("owner", owner.NewRepo)
	if err != nil {
		status := http2.HttpStatus{StatusCode: http.StatusInternalServerError, Message: "Couldn't find or generate repo"}
		eRes.SetHttpStatus(status)
		return eRes
	}

	res := handlers.CreateModel(&m, &eRes, repo)
	return *res.(*restapi.ExportedOwnerRes)
}
