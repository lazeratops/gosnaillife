package owner

import (
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func getOwnerReq(r *http.Request) (restapi.OwnerReq, error) {
	var owner restapi.OwnerReq
	err := handlers.GetModelReq(r, &owner)
	return owner, err
}
