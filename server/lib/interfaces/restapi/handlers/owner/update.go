package owner

import (
	"github.com/gorilla/mux"
	"gitlab.com/drakonka/gosnaillife/common/domain/owner"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"net/http"
)

func Update(w http.ResponseWriter, r *http.Request, app *env.Application) {

	// Authenticate
	ap := r.Header.Get("auth_provider")
	authErr := app.Authenticator.AuthenticateUser(ap, r)

	if authErr != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	vars := mux.Vars(r)
	ownerId, _ := vars["id"]

	ownerRes := restapi.ExportedOwnerRes{}

	var sc int
	var msg string

	// Try to update
	sc, msg = updateOwner(r, ownerId, &ownerRes, app.Database)
	status := http2.HttpStatus{StatusCode: sc, Message: msg}
	ownerRes.SetHttpStatus(status)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(sc)
	if err := ownerRes.JsonEncode(w); err != nil {
		error2.HandleErr(err, "UpdateOwnerErr")
		return
	}
}

func updateOwner(r *http.Request, ownerId string, ownerRes *restapi.ExportedOwnerRes, db databases.Database) (int, string) {
	repo, err := owner.NewRepo(db)

	var sc int
	var msg string

	ownerReq, err := getOwnerReq(r)
	if err != nil {
		sc = http.StatusBadRequest
		msg = "Could not decode POST body"
		return sc, msg
	}

	defer r.Body.Close()
	err = repo.UpdateOne(ownerId, ownerReq.FieldsToUpdate)
	if err != nil {
		sc = http.StatusBadRequest
		msg = "Could not update owner: " + err.Error()
		return sc, msg
	}

	// Retrieve updated owner again and include in owner res
	updatedOwner, err := repo.GetOne(ownerId, []string{})
	if err != nil {
		sc = http.StatusInternalServerError
		msg = "Could not retrieve just updated owner..."
		return sc, msg
	}
	ownerRes.SetModels([]repo2.Model{updatedOwner.(*owner.Owner)})
	sc = http.StatusOK
	return sc, msg
}
