package handlers

import (
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"net/http"
)

func WriteCORSResponse(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "x-authentication-token, content-type, auth_provider")
}

func WriteResponse(w http.ResponseWriter, res restapi.Responder) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	status := res.HttpStatus().StatusCode
	w.WriteHeader(status)
	if BodyAllowedForStatus(status) {
		if err := res.JsonEncode(w); err != nil {
			error2.HandleErr(err, "WriteResponse")
		}
	}
}
