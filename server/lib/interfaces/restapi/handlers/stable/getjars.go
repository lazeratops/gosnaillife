package stable

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func GetJars(w http.ResponseWriter, r *http.Request, app *env.Application) {
	handlers.GetModels(w, r, app, findJars)
}

func findJars(stableId string, fields []string, app *env.Application) (restapi.Responder, error) {
	jarRes := &restapi.ExportedJarRes{}
	repo, _ := jar.NewRepo(app.Database)
	jars, err := repo.GetMany("stable_id=?", []interface{}{stableId}, fields)
	var status http2.HttpStatus
	if len(jars) == 0 {
		status.StatusCode = http.StatusNoContent
		status.Message = "This stable doesn't have any jars."
	} else {
		var models []repo2.Model
		for _, j := range jars {
			jar := j.(*jar.Jar)
			models = append(models, jar)
		}
		jarRes.SetModels(models)
		status.StatusCode = http.StatusOK
	}
	jarRes.SetHttpStatus(status)
	return jarRes, err
}
