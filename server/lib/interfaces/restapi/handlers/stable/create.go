package stable

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain/stable"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func CreateStable(w http.ResponseWriter, r *http.Request, app *env.Application) {
	// Authenticate
	ap := r.Header.Get("auth_provider")
	authErr := app.Authenticator.AuthenticateUser(ap, r)

	if authErr != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	stableReq, err := getStableReq(r)
	handlers.HandleReqErr(err, w)
	stableRes := createStable(stableReq, app)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(stableRes.HttpStatus().StatusCode)
	if stableRes.HttpStatus().StatusCode == 200 {
		err = stableRes.JsonEncode(w)
		if err != nil {
			fmt.Printf("\n Error when encoding owner res: %v", err)
		}
	}
}

func createStable(stableReq restapi.StableReq, app *env.Application) (stableRes restapi.ExportedStableRes) {
	m := stableReq.Stable
	eRes := restapi.ExportedStableRes{}

	repo, err := app.GetOrMakeRepo("stable", stable.NewRepo)
	if err != nil {
		status := http2.HttpStatus{StatusCode: http.StatusInternalServerError, Message: "Couldn't find or generate repo"}
		eRes.SetHttpStatus(status)
		return eRes
	}

	res := handlers.CreateModel(&m, &eRes, repo)
	return *res.(*restapi.ExportedStableRes)
}
