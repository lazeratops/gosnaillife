package user

import (
	"encoding/json"
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func LoginUser(w http.ResponseWriter, r *http.Request, app *env.Application) {
	user, err := getUserReq(r)
	handlers.HandleReqErr(err, w)

	userRes, err := loginUser(user, app)
	if err != nil {
		error2.HandleErr(err, "LoginUserErr")
	}

	handlers.WriteResponse(w, &userRes)
}

func loginUser(user restapi.UserReq, app *env.Application) (userRes restapi.UserRes, err error) {
	provider, err := app.Authenticator.FindProvider(user.AuthProvider)
	if err != nil {
		common.Log.With(err).Error("failed to find provider")
		userRes.Httpstatus.StatusCode = http.StatusUnprocessableEntity
		userRes.Httpstatus.Message = fmt.Sprintf("Could not find requested auth provider %s + %v", user.AuthProvider, err)
	}
	var resBody []byte
	if err == nil {
		resBody, err = provider.Login(user.Username, user.Password)
		if err != nil {
			common.Log.With(err).Error("failed to log in user")
			return userRes, err
		}
		err = json.Unmarshal(resBody, &userRes)
		userRes.Username = user.Username
	}
	return userRes, err
}
