package user

import (
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func getUserReq(r *http.Request) (restapi.UserReq, error) {
	var user restapi.UserReq
	err := handlers.GetModelReq(r, &user)
	return user, err
}
