package user

import (
	owner2 "gitlab.com/drakonka/gosnaillife/common/domain/owner"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func GetOwner(w http.ResponseWriter, r *http.Request, app *env.Application) {
	handlers.GetModels(w, r, app, findOwner)
}

func findOwner(userid string, fields []string, app *env.Application) (restapi.Responder, error) {
	repo, err := owner2.NewRepo(app.Database)
	owners, err := repo.GetMany("user_id=?", []interface{}{userid}, fields)
	var owner owner2.Owner
	var status http2.HttpStatus
	if len(owners) > 0 {
		owner = *owners[0].(*owner2.Owner)
		status.StatusCode = http.StatusOK
	} else {
		status.StatusCode = http.StatusNoContent
		status.Message = "This user doesn't have an associated owner"
	}
	ownerRes := &restapi.ExportedOwnerRes{}
	ownerRes.SetHttpStatus(status)
	ownerRes.SetModels([]repo2.Model{&owner})
	return ownerRes, err
}
