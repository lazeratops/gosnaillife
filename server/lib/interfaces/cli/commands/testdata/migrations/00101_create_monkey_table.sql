-- +goose Up
CREATE TABLE monkeys (
  monkey_name varchar(10) UNIQUE
);

-- +goose Down
DROP TABLE monkeys;