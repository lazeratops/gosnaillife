-- +goose Up
CREATE TABLE IF NOT EXISTS owners (
  owner_id int UNSIGNED NOT NULL PRIMARY KEY,
  user_id varchar(50),
  firstname varchar (10),
  lastname varchar(10) UNIQUE,
  description varchar(500),
  sek int UNSIGNED,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW()
);

INSERT INTO owners VALUES
  (0, null, 'John', 'Bobface', 'Friendly neighborhood snail owner', 0, NOW(), NOW());

-- +goose Down
DROP TABLE owners;