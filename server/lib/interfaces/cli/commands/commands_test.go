package commands

import (
	"errors"
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure"
	"net"
	"os"
	"sync"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	ret := 1
	defer func() {
		os.Exit(ret)
	}()
	defer testing2.TimeElapsed("Server Commands Test", testing2.ModerateThreshold)()
	ret = m.Run()
}

func TestStartServer(t *testing.T) {
	testCases := []struct {
		tcname   string
		args     []string
		err      error
		doDropDb bool
	}{
		{
			tcname: "StartLiveMode",
			args:   []string{},
			err:    errors.New("http: Server closed"),
		},
		{
			tcname: "StartLiveModeAndShutdownPrior",
			args:   []string{},
			err:    errors.New("http: Server closed"),
		},
		{
			tcname: "StartTestMode",
			args:   []string{"test", "--skipOsExit"},
			err:    errors.New("http: Server closed"),
		},
		{
			tcname: "StartTestModeAndShutdownPrior",
			args:   []string{"--test", "--skipOsExit"},
			err:    errors.New("http: Server closed"),
		},
		{
			tcname: "StartTestModeWithMigrationsSkipAlLDefaults",
			args:   []string{"--test", "--skipOsExit", "--skipDefaultMigrations", "--skipDefaultSeed", "--migrations", "/server/lib/interfaces/cli/commands/testdata/migrations"},
			err:    errors.New("http: Server closed"),
		},
		{
			tcname: "StartTestModeWithFrozenWorld",
			args:   []string{"--test", "--skipOsExit", "--frozenWorld", "--skipDefaultSeed", "--skipDefaultMigrations"},
			err:    errors.New("http: Server closed"),
		},
	}
	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			app, err := infrastructure.Init(common.CLI)
			if err != nil {
				t.Fatal(err)
			}
			listener, err := net.Listen("tcp", ":0")
			if err != nil {
				t.Fatalf("Failed to create listener: %v", err)
			}
			port := listener.Addr().(*net.TCPAddr).Port
			err = listener.Close()
			if err != nil {
				t.Fatal(err)
			}
			tc.args = append(tc.args, "--port")
			tc.args = append(tc.args, fmt.Sprintf("%d", port))
			go func() {
				mx := &sync.RWMutex{}
				err = startServer(tc.args, app, mx)
			}()
			time.Sleep(time.Second * 5)
			now := time.Now()
			for !running(port) {
				if time.Now().After(now.Add(time.Second * 5)) {
					t.Fatal("Failed to start server")
				}
			}
			app.Quit()
			now = time.Now()
			for running(port) {
				if time.Now().After(now.Add(time.Second * 5)) {
					t.Fatal("Failed to shutdown server")
				}
			}

			if e2 := testing2.TestErrorType(tc.err, err); e2 != nil {
				t.Fatal(e2)
			}
		})
	}
}

func running(port int) bool {
	timeout := 1 * time.Second
	_, err := net.DialTimeout("tcp", fmt.Sprintf("localhost:%d", port), timeout)
	if err != nil {
		return false
	}
	return true
}
