package http

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"sync"
	"time"
)

// HttpServerWithShutdown is a server which supports and tracks shutdown
type HttpServerWithShutdown struct {
	http.Server
	isShuttingDown bool
}

// StartShutdown initializes shutdown and registers the server as shutting down
func (srv *HttpServerWithShutdown) StartShutdown() error {
	srv.isShuttingDown = true
	return srv.Shutdown(context.Background())
}

// ConfigureServer creates and returns a new server with the given router.
func ConfigureServer(router *mux.Router) (srv *HttpServerWithShutdown) {
	srv = &HttpServerWithShutdown{}
	srv.Handler = router
	srv.WriteTimeout = 15 * time.Second
	return srv
}

// StartServer starts the server
func (srv *HttpServerWithShutdown) StartServer(port int, mx *sync.RWMutex) error {
	fmt.Printf("\nStarting server on port %d", port)
	mx.Lock()
	srv.Addr = fmt.Sprintf(":%d", port)
	shuttingDown := srv.isShuttingDown
	mx.Unlock()
	if !shuttingDown {
		return srv.ListenAndServe()
	}

	return http.ErrServerClosed
}
