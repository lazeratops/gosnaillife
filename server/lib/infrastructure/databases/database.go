package databases

import "database/sql"

type RowResult map[string]interface{}

// Database is used by the Application to connect to and manipulate the db
type Database interface {
	Configure(username, userpass, dhost, dbport, dbname string)
	Connect() error
	Disconnect() error
	Insert(tablename string, fields map[string]string) (int64, error)
	Update(tablename string, pkeyval string, fields map[string]string) error
	GetRow(tablename string, pkeyval string, fields []string) (results RowResult, err error)
	GetRows(tablename string, where string, params []interface{}, fields []string) (results []RowResult, err error)
	GetCount(tablename string, where string, params []interface{}) (count int, err error)
	DeleteRows(tablename string, where string, params []interface{}) (rowcount int64, err error)
	DeleteRow(tablename string, pkeyval string) (rowcount int64, err error)
	CreateTable(tablename string, pkey string, cols []string) error
	Transact(f func(*sql.Tx) error) error
}
