package mysql

import "fmt"

// DeleteRows deletes multiple rows from a table.
func (mysql *MySql) DeleteRows(tablename string, where string, params []interface{}) (rowcount int64, err error) {
	if where == "" {
		return 0, nil
	}
	if len(params) == 0 {
		return 0, &MySqlError{"No parameters specified"}
	}
	query := fmt.Sprintf(`DELETE FROM %s
	WHERE %s`, tablename, where)
	res, err := mysql.db.Exec(query, params...)
	if err != nil {
		return -1, err
	}
	rowcount, err = res.RowsAffected()
	return rowcount, err
}

// DeleteRow deletes one row from a table.
func (mysql *MySql) DeleteRow(tablename, pkeyval string) (rowcount int64, err error) {
	pkey := mysql.getPrimaryKey(tablename)
	query := fmt.Sprintf(`DELETE FROM %s
	WHERE %s = ?`, tablename, pkey)
	res, err := mysql.db.Exec(query, pkeyval)
	if err != nil {
		return -1, err
	}
	rowcount, err = res.RowsAffected()
	return rowcount, err
}
