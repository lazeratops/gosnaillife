package mysql

import (
	"fmt"
	"strings"
)

// Update updates a row in the database based on given table name and primary key.
func (mysql *MySql) Update(tablename string, pkeyval string, fields map[string]string) (err error) {
	if pkeyval == "" {
		return &MySqlError{"Primary key not specified."}
	}
	fieldQuant := len(fields)
	if fieldQuant < 1 {
		return nil
	}

	pkey := mysql.getPrimaryKey(tablename)

	setQ := prepUpdateQuery(fields)
	query := fmt.Sprintf(
		`UPDATE %s
	 	SET %s
	 	WHERE %s = ?`, tablename, setQ, pkey)

	_, err = mysql.db.Exec(query, pkeyval)
	return err
}

func prepUpdateQuery(fields map[string]string) string {

	setParts := make([]string, 0, len(fields))
	setQ := ""

	for k, v := range fields {
		escapedV := escapeStr(v)

		element := fmt.Sprintf("%s = '%s'", k, escapedV)
		setParts = append(setParts, element)
	}
	setQ = strings.Join(setParts, ",")

	return setQ
}
