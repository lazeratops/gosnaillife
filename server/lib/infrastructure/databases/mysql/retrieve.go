package mysql

import (
	"database/sql"
	"fmt"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"strings"
)

// GetRow retrieves one row from the database
func (mysql *MySql) GetRow(tablename string, pkeyval string, fields []string) (result databases.RowResult, err error) {
	err = mysql.Connect()
	if err != nil {
		return result, err
	}
	pkey := mysql.getPrimaryKey(tablename)

	if mysql.fieldsToRetrieveEmpty(fields) {
		fields = []string{"*"}
	} else if util.StrIndexOf(fields, pkey) == -1 {
		fields = append(fields, pkey)
	}

	selectQ := prepRetrieveQuery(fields)
	query := fmt.Sprintf(`
	SELECT %s
	FROM %s
	WHERE %s = ?
	LIMIT 1`, selectQ, tablename, pkey)
	row := mysql.db.QueryRowx(query, pkeyval)
	result = databases.RowResult{}
	err = row.MapScan(result)
	if err != nil {
		// If no rows found we will handle this ourselves.
		if err == sql.ErrNoRows {
			err = nil
		} else {
			error2.HandleErr(err, "MySQL MapScan")
		}
	}
	finresult := databases.RowResult{}
	for k, v := range result {
		if _, ok := v.([]uint8); ok {
			v = util.UInt8ToStr(v.([]uint8))
		}
		finresult[k] = v
	}
	return finresult, err
}

// GetRows retrieves multiple rows from the database.
func (mysql *MySql) GetRows(tablename string, where string, params []interface{}, fields []string) (results []databases.RowResult, err error) {
	err = mysql.Connect()
	if err != nil {
		return results, err
	}
	if mysql.fieldsToRetrieveEmpty(fields) {
		fields = []string{"*"}
	}

	selectQ := prepRetrieveQuery(fields)
	query := fmt.Sprintf(`
		SELECT %s
		FROM %s
		WHERE %s`, selectQ, tablename, where)

	rows, err := mysql.db.Queryx(query, params...)
	if err != nil {
		error2.HandleErr(err, "GetRows")
		return results, err
	} else if rows == nil {
		return results, err
	}
	defer rows.Close()

	results = []databases.RowResult{}
	for rows.Next() {
		result := databases.RowResult{}
		err = rows.MapScan(result)
		if err != nil {
			error2.HandleErr(err, "MySQL MapScan")
		}
		finresult := databases.RowResult{}
		for k, v := range result {
			if _, ok := v.([]uint8); ok {
				v = util.UInt8ToStr(v.([]uint8))
			}
			finresult[k] = v
		}
		/* pkeyname := mysql.getPrimaryKey(tablename)
		pkey := finresult[pkeyname]
		if _, ok := pkey.(int64); ok {
			pkey = strconv.Itoa(int(pkey.(int64)))
		} */
		// results[fmt.Sprintf("%v", pkey)] = finresult
		results = append(results, finresult)
	}

	return results, err
}

// GetCount retrieves the number of rows matching the given query.
func (mysql *MySql) GetCount(tablename string, where string, params []interface{}) (count int, err error) {
	err = mysql.Connect()
	if err != nil {
		return -1, err
	}
	query := fmt.Sprintf(`
		SELECT count(*)
		FROM %s
		WHERE %s`, tablename, where)
	err = mysql.db.QueryRowx(query, params...).Scan(&count)
	return count, err
}

func prepRetrieveQuery(fields []string) string {
	selectQ := strings.Join(fields, ",")
	return selectQ
}

func (mysql *MySql) fieldsToRetrieveEmpty(fields []string) bool {
	fieldsEmpty := false
	if len(fields) == 0 || len(fields) == 1 && fields[0] == "" {
		fieldsEmpty = true
	}
	return fieldsEmpty
}
