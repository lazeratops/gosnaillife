package mysql

import (
	"fmt"
	"strings"
)

// CreateTable creates a table in the database.
func (mysql *MySql) CreateTable(tablename string, pkey string, cols []string) error {
	err := mysql.Connect()
	if err != nil {
		return err
	}

	query := prepCreateQuery(tablename, pkey, cols)
	_, err = mysql.PrepAndExecStmt(query)
	return err
}

func prepCreateQuery(tablename string, pkey string, cols []string) string {
	colstr := strings.Join(cols, ",")
	query := fmt.Sprintf(`CREATE TABLE %s(%s, PRIMARY KEY (%s));`, tablename, colstr, pkey)
	return query
}
