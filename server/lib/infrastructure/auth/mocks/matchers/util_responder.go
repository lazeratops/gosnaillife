// Code generated by pegomock. DO NOT EDIT.
package matchers

import (
	"github.com/petergtz/pegomock"
	util "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth/util"
	"reflect"
)

func AnyUtilResponder() util.Responder {
	pegomock.RegisterMatcher(pegomock.NewAnyMatcher(reflect.TypeOf((*(util.Responder))(nil)).Elem()))
	var nullValue util.Responder
	return nullValue
}

func EqUtilResponder(value util.Responder) util.Responder {
	pegomock.RegisterMatcher(&pegomock.EqMatcher{Value: value})
	var nullValue util.Responder
	return nullValue
}
