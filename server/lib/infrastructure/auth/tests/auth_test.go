package tests

import (
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth"
	"testing"
)

// Some generic tests without any sort of environment setup
func TestAuthenticateWithoutProvider(t *testing.T) {
	t.Parallel()
	a := auth.NewAuthenticator(common.CLI)
	err := a.AuthenticateUser("Imaginary Provider", nil)
	wantedMsg := "Provider imaginary provider not found"
	if err == nil || err.Error() != wantedMsg {
		t.Errorf("\nTestGetNonexistentProvider failed. Expected a '%s' error, got %v", wantedMsg, err)
	}
}
