package auth

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth/cli/auth0"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth/cli/mysql"
	"net/http"
	"strings"
)

// Authenticator contains a map of valid authentication providers.
type Authenticator struct {
	Providers map[string]Provider
}

// PrepAuthProvider instantiates and configures an authentication provider based on client type.
func (a *Authenticator) PrepAuthProvider(clientType common.ClientType, name string, properties map[string]interface{}) (provider Provider, err error) {
	if clientType == common.CLI {
		switch name {
		case "auth0":
			provider = &auth0.Auth0{}

			break
		case "mysql":
			provider = &mysql.MySqlAuth{}
			break
		}
	}
	if provider != nil {
		provider.Configure(properties)
	}
	return provider, err
}

// FindProvider finds a provider by name from the map of valid providers.
func (a *Authenticator) FindProvider(name string) (provider Provider, err error) {
	name = strings.ToLower(name)
	if p, ok := a.Providers[name]; ok {
		provider = p
	} else {
		err = errors.New(fmt.Sprintf("Provider %s not found", name))
	}
	return provider, err
}

// AuthenticateUser authenticates a user with the given provider.
func (a *Authenticator) AuthenticateUser(pname string, r *http.Request) error {
	provider, err := a.FindProvider(pname)
	if err != nil {
		return err
	}
	authErr := provider.Authenticate(r)
	return authErr
}
