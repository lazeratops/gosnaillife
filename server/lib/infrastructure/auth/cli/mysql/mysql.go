package mysql

import (
	"encoding/json"
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/common/util"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	authutil "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth/util"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strconv"
	time2 "time"
)

// MySqlAuth is a MySQL authentication provider.
// This provider should be used only for local dev testing.
// In the live environment the Auth0 provider is to be used.
type MySqlAuth struct {
	tableName         string
	sessionLengthMins int
	db                databases.Database
	resToMarshalTo    authutil.Responder
}

// Configure configures the MySql auth provider with given database, table name, and session length.
func (ap *MySqlAuth) Configure(config map[string]interface{}) error {
	db, ok := config["database"]
	if !ok {
		return fmt.Errorf("failed to find datbase in configuration")
	}
	ap.db = db.(databases.Database)

	tableName, ok := config["table_name"]
	if !ok {
		return fmt.Errorf("failed to find table_name in configuration")
	}

	ap.tableName = tableName.(string)

	sessionLength, ok := config["session_length"]
	if !ok {
		return fmt.Errorf("failed to find session_length in configuration")
	}
	ap.sessionLengthMins, _ = strconv.Atoi(sessionLength.(string))
	return nil
}

func (ap *MySqlAuth) SetResToMarshalTo(res authutil.Responder) {
	ap.resToMarshalTo = res
}

// Register registers a user in the MySql database
func (ap *MySqlAuth) Register(email, password string) (res []byte, err error) {
	err = ap.db.Connect()
	if err != nil {
		return res, err
	}
	userRes := ap.resToMarshalTo
	// TODO: Check for a duplicate registration
	where := "email = ?"
	params := []interface{}{email}
	count, err := ap.db.GetCount(ap.tableName, where, params)
	if count > 0 {
		s := http2.HttpStatus{StatusCode: http.StatusForbidden, Message: "Email already exists"}
		userRes.SetHttpStatus(s)
		res, err = json.Marshal(userRes)
		return res, err
	}
	// Hash password
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	httpstatus := authutil.HandleAuthErr(err, "MySqlRegister")
	if httpstatus.StatusCode != 200 {
		userRes.SetHttpStatus(httpstatus)
		res, err = json.Marshal(userRes)
		return res, err
	}

	hashedPass := string(hash)
	m := make(map[string]string)
	m["email"] = email
	m["password"] = hashedPass

	_, err = ap.db.Insert(ap.tableName, m)
	httpstatus = authutil.HandleAuthErr(err, "MySqlRegister")
	if httpstatus.StatusCode != 200 {
		userRes.SetHttpStatus(httpstatus)
		res, err = json.Marshal(userRes)
		return res, err
	}

	// Retrieve the just-stored ID
	where = "email = ?"
	params = []interface{}{email}
	rows, err := ap.db.GetRows(ap.tableName, where, params, []string{"_id"})
	httpstatus = authutil.HandleAuthErr(err, "MySqlRegister")
	if httpstatus.StatusCode != 200 {
		userRes.SetHttpStatus(httpstatus)
		res, err = json.Marshal(userRes)
		return res, err
	}

	userRowM := make(map[string]interface{})
	for _, v := range rows {
		userRowM = v
		break
	}
	id := fmt.Sprintf("%d", userRowM["_id"].(int64))
	userRes.SetId(id)
	httpstatus = http2.HttpStatus{StatusCode: 200}
	userRes.SetHttpStatus(httpstatus)
	res, err = json.Marshal(userRes)
	return res, err
}

// Login logs in a user using the MySQL database.
func (ap *MySqlAuth) Login(username, password string) (res []byte, err error) {
	where := "email = ?"
	params := []interface{}{username}
	rows, err := ap.db.GetRows(ap.tableName, where, params, []string{"password", "_id"})
	if len(rows) == 0 {
		err = errors.New("your credentials are invalid")
	}
	userRes := ap.resToMarshalTo
	httpstatus := authutil.HandleAuthErr(err, "MySqlLogin")
	if httpstatus.StatusCode != 200 {
		err = nil
		userRes.SetHttpStatus(httpstatus)
		res, err = json.Marshal(userRes)
		return res, err
	}

	userRowM := make(map[string]interface{})
	for _, v := range rows {
		userRowM = v
		break
	}

	hashedPass := userRowM["password"]
	err = bcrypt.CompareHashAndPassword([]byte(hashedPass.(string)), []byte(password))
	httpstatus = authutil.HandleAuthErr(err, "MySqlLogin")
	if httpstatus.StatusCode != 200 {
		err = nil
		userRes.SetHttpStatus(httpstatus)
		res, err = json.Marshal(userRes)
		return res, err
	}
	msg := fmt.Sprintf("Generating access token from ID %v", userRowM["_id"])
	common.Log.Debug(msg)

	// No error above, so generate an access token
	accessToken, err := ap.generateAccessToken(strconv.Itoa(int(userRowM["_id"].(int64))))
	httpstatus = authutil.HandleAuthErr(err, "MySqlLogin")
	if httpstatus.StatusCode != 200 {
		err = nil
		userRes.SetHttpStatus(httpstatus)
		res, err = json.Marshal(userRes)
		return res, err
	}
	httpstatus = http2.HttpStatus{StatusCode: 200}
	userRes.SetHttpStatus(httpstatus)

	userRes.SetAccessToken(accessToken)
	id := fmt.Sprintf("%d", userRowM["_id"].(int64))
	userRes.SetId(id)
	res, err = json.Marshal(userRes)
	return res, err
}

// Logout logs out a uesr (obviously not currently implemented...)
func (ap *MySqlAuth) Logout(token string) (resBody []byte, err error) {
	return resBody, err
}

// Authenticate checks if a user is logged in (not currently implemented...)
func (ap *MySqlAuth) Authenticate(request *http.Request) error {
	fmt.Println("checking mysql auth")
	return nil
}

func (ap *MySqlAuth) generateAccessToken(id string) (token string, err error) {
	unhashedToken, err := util.GenRandStr(32)
	hashedToken, err := bcrypt.GenerateFromPassword([]byte(unhashedToken), bcrypt.DefaultCost)
	now := time2.Now()
	expiryTime := now.AddDate(0, 0, 1).Unix()
	expTimeStr := strconv.FormatInt(expiryTime, 10)
	token = string(hashedToken) + "." + expTimeStr
	err = ap.db.Update(ap.tableName, id, map[string]string{"token": token})
	return token, err
}
