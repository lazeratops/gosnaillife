package tests

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth/cli/mysql"
	"golang.org/x/crypto/bcrypt"
	"os"
	"testing"

	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/tests"
	"strings"
)

var dbName = "sltestdb_auth"
var tblName = "users"
var userEmail = "mysqlTest@test.com"
var userPass = "superawesometestpassword23423"

var database databases.Database

func TestMain(m *testing.M) {
	tu := tests.NewTestUtil()
	var err error
	database, err = tu.CreateTestDB(dbName)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	err = tu.RunDbMigrations(false, "")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	hash, _ := bcrypt.GenerateFromPassword([]byte(userPass), bcrypt.DefaultCost)
	seedData := []map[string]string{
		{
			"email":    userEmail,
			"password": string(hash),
		},
	}
	err = tu.Seed(dbName, "users", seedData)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	err = seedDb(tu)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	ret := m.Run()

	tu.DeleteTestDB(true, dbName)
	os.Exit(ret)
}

func seedDb(tu *tests.TestUtil) error {
	fmt.Println("Seeding users")
	rows := []map[string]string{
		{
			"email":    "test@test.com",
			"password": "testpass",
		},
	}
	return tu.Seed(dbName, tblName, rows)
}

func TestMySqlSignup(t *testing.T) {
	testCases := []struct {
		tcname string
		email  string
		pass   string
		want   map[string]string
	}{
		{"ValidRegistration", "testing@test.com.au", "testpass101", map[string]string{"_id": "3"}},
		{"DuplicateRegistration", "testing@test.com.au", "testpass2", map[string]string{"message": "Email already exists"}},
		{"SecondValidRegistration", "test2@test.com", "testingpasswords243", map[string]string{"_id": "4"}},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestMySqlSignup Test case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			fmt.Printf("Creating user with ID: %s, pass: %s\n", tc.email, tc.pass)
			provider := getProvider(tblName)
			userResJson, err := provider.Register(tc.email, tc.pass)
			// Find all the elements we wanted to get in the want map.
			for wantedKey, wantedVal := range tc.want {
				if err != nil && !strings.Contains(err.Error(), wantedVal) {
					t.Errorf("Register failed: %s", err.Error())
				}

				m, err := util.FindInJson(userResJson, []string{wantedKey})
				var val string
				if i, ok := m[wantedKey]; ok {
					val = fmt.Sprintf("%v", i)
				}

				if err != nil {
					t.Errorf("Signup test failed: %s", err.Error())
				} else if !strings.Contains(val, wantedVal) {
					t.Errorf("Signup test failed; unexpected value " + val + ". Expected " + wantedVal)
				}
			}
		})
	}
}

func TestMySqlLogin(t *testing.T) {
	testCases := []struct {
		tcname string
		email  string
		pass   string
		want   map[string]string
	}{
		{"ValidLogin", userEmail, userPass, map[string]string{"access_token": "*", "_id": "*"}},
		{"WrongPassLogin", userEmail, "testpass2", map[string]string{"message": "Your credentials could not be found"}},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestMySqlLogin Test case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			provider := getProvider(tblName)
			userResJson, err := provider.Login(tc.email, tc.pass)
			for wantedKey, wantedVal := range tc.want {
				if err != nil && !strings.Contains(err.Error(), wantedVal) {
					t.Errorf("Login failed: %s", err.Error())
					break
				}

				m, err := util.FindInJson(userResJson, []string{wantedKey})
				var val string
				if i, ok := m[wantedKey]; ok {
					val = fmt.Sprintf("%v", i)
				}

				if err != nil {
					t.Errorf("Login test failed: %s", err.Error())
				} else if len(val) == 0 || !strings.Contains(val, wantedVal) && wantedVal != "*" {
					t.Errorf("Login test failed; unexpected value " + val + ". Expected " + wantedVal)
				}
			}
		})
	}
}

func getProvider(tablename string) *mysql.MySqlAuth {
	provider := mysql.MySqlAuth{}
	configCreds := make(map[string]interface{})
	configCreds["table_name"] = tablename
	configCreds["session_length"] = "5"
	configCreds["database"] = database
	provider.Configure(configCreds)
	provider.SetResToMarshalTo(&restapi.UserRes{})
	return &provider
}
