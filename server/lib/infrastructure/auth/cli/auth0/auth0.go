package auth0

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/common/util"
	httputil "gitlab.com/drakonka/gosnaillife/common/util/http"
	authutil "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth/util"
	"net/http"
)

// Auth0 is an authentication provider
type Auth0 struct {
	clientId       string
	clientSecret   string
	domain         string
	resToMarshalTo authutil.Responder
}

// Configure sets the provider's client ID, client secret, and domain
func (ap *Auth0) Configure(credentials map[string]interface{}) error {
	id, ok := credentials["client_id"]
	if !ok {
		return fmt.Errorf("failed to find client_id in configuration")
	}
	ap.clientId = id.(string)

	secret, ok := credentials["client_secret"]
	if !ok {
		return fmt.Errorf("failed to find client_secret in configuration")
	}
	ap.clientSecret = secret.(string)

	domain, ok := credentials["domain"]
	if !ok {
		return fmt.Errorf("failed to find domain in configuration")
	}
	ap.domain = domain.(string)
	return nil
}

func (ap *Auth0) SetResToMarshalTo(res authutil.Responder) {
	ap.resToMarshalTo = res
}

// Register registers a user with the Auth0 provider
func (ap *Auth0) Register(email, password string) (res []byte, err error) {
	url := ap.domain + "dbconnections/signup"
	postSpec := fmt.Sprintf(
		`{"client_id":"%s",
		"email":"%s",
		"password":"%s",
		"connection":"Username-Password-Authentication"}`, ap.clientId, email, password)

	userRes := ap.resToMarshalTo
	resBody, err := httputil.PostAndGetResBody(url, postSpec)
	httpstatus := authutil.HandleAuthErr(err, "Auth0Register")
	if httpstatus.StatusCode != 200 {
		userRes.SetHttpStatus(httpstatus)
		res, err = json.Marshal(userRes)
		return res, err
	}

	m, err := util.FindInJson(resBody, []string{"_id", "description", "error"})
	httpstatus = authutil.HandleAuthErr(err, "Auth0Register")
	if httpstatus.StatusCode != 200 {
		userRes.SetHttpStatus(httpstatus)
		res, err = json.Marshal(userRes)
		return res, err
	}

	if m["_id"] == nil {
		msg := "Registration failed "
		if m["error"] != nil {
			msg += fmt.Sprintf("error: %s; ", m["error"])
		}
		if m["description"] != nil {
			msg += m["description"].(string)
		}
		err = errors.New(msg)
		httpstatus = authutil.HandleAuthErr(err, "Auth0Register")
		if httpstatus.StatusCode != 200 {
			userRes.SetHttpStatus(httpstatus)
			res, _ = json.Marshal(userRes)
			return res, err
		}
	} else {
		id := m["_id"].(string)
		userRes.SetId(id)
	}
	res, err = json.Marshal(userRes)
	return res, err
}

// Login logs in a user using the Auth0 provider
func (ap *Auth0) Login(username, password string) (res []byte, err error) {
	url := ap.domain + "oauth/token"
	audience := "https://snaillife"
	postSpec := fmt.Sprintf(`{"grant_type":"password",
	"username":"%s",
	"password":"%s",
	"client_secret": "%s",
	"audience": "%s",
	"client_id":"%s"}`, username, password, ap.clientSecret, audience, ap.clientId)

	// Post to Auth0
	body, err := httputil.PostAndGetResBody(url, postSpec)
	userRes := ap.resToMarshalTo
	httpstatus := authutil.HandleAuthErr(err, "Auth0Login")
	if httpstatus.StatusCode != 200 {
		userRes.SetHttpStatus(httpstatus)
		res, _ = json.Marshal(userRes)
		return res, err
	}

	userRes, err = ap.buildLoginResponse(body)
	res, _ = json.Marshal(userRes)
	return res, err
}

// Logout logs out a user using the Auth0 provider.
func (ap *Auth0) Logout(token string) (resBody []byte, err error) {
	url := ap.domain + "v2/logout"
	getSpec := fmt.Sprintf(`?client_id=%s`, ap.clientId)
	fullUrl := url + "/" + getSpec
	_, err = httputil.GetAndGetResBody(fullUrl)
	httpstatus := authutil.HandleAuthErr(err, "Auth0Logout")
	if httpstatus.StatusCode != 200 {
		err = nil
		return []byte(httpstatus.Message), err
	}
	return resBody, err
}

func (ap *Auth0) buildLoginResponse(resBody []byte) (authutil.Responder, error) {
	m := make(map[string]interface{})
	userRes := ap.resToMarshalTo
	err := json.Unmarshal(resBody, &m)
	httpstatus := authutil.HandleAuthErr(err, "Auth0Login")
	userRes.SetHttpStatus(httpstatus)
	if _, ok := m["access_token"]; !ok {
		msg := "Login Failure; access token not found in response"

		if m["error_description"] != nil {
			msg += fmt.Sprintf(" error: %s; ", m["error_description"])
		}
		status := httputil.HttpStatus{StatusCode: http.StatusUnauthorized, Message: msg}
		userRes.SetHttpStatus(status)
		return userRes, err
	}
	userRes.SetAccessToken(m["access_token"].(string))

	httpstatus = authutil.HandleAuthErr(err, "Auth0Login")
	userRes.SetHttpStatus(httpstatus)
	return userRes, err

}

// Authenticate checks the given JSON Web Token from the request.
func (ap *Auth0) Authenticate(request *http.Request) error {
	common.Log.Info("Authenticating...")
	return authutil.CheckJwt(request, ap.domain)
}

// GetManagementAccessToken gets a more permissive access token from Auth0
func (ap *Auth0) GetManagementAccessToken() (token string, err error) {
	url := ap.domain + "oauth/token"
	audienceUrl := ap.domain + "api/v2/"
	postSpec := fmt.Sprintf(`{"client_id":"%s",
	"client_secret":"%s",
	"audience":"%s",
	"grant_type":"client_credentials"}`, ap.clientId, ap.clientSecret, audienceUrl)

	body, err := httputil.PostAndGetResBody(url, postSpec)
	if err != nil {
		common.Log.Error(err.Error())
		return token, err
	}
	var data map[string]interface{}
	err = json.Unmarshal([]byte(string(body)), &data)

	if e, ok := data["error"]; ok {
		msg := fmt.Sprintf("%s - %s", e, data["error_description"])
		err = errors.New(msg)
		return token, err
	}

	if val, ok := data["access_token"]; ok {
		token = val.(string)
	}
	return token, err
}
