package tests

import (
	"encoding/json"
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth/cli/auth0"
	"math/rand"
	http2 "net/http"
	"os"
	"strconv"
	"testing"
	"time"
)

var userId string
var accessToken string
var creds testCredentials

type testCredentials struct {
	email    string
	password string
	domain   string
}

func TestMain(m *testing.M) {
	defer testing2.TimeElapsed("Auth0", testing2.ModerateThreshold)()
	seed := rand.NewSource(time.Now().UnixNano())
	r := rand.New(seed)

	creds = testCredentials{
		email:    "testuser" + strconv.Itoa(r.Intn(100)) + "@snaillifetest.com",
		password: "testpass" + strconv.Itoa(r.Intn(100)),
		domain:   "https://snaillife.eu.auth0.com/",
	}

	ret := m.Run()

	fmt.Println("User id: " + userId)
	if userId != "" {
		fmt.Println("Deleting user")
		deleteTestUser()
	}

	os.Exit(ret)
}

func TestGetManagementToken(t *testing.T) {
	t.Parallel()
	provider := getProvider(creds)
	_, err := provider.GetManagementAccessToken()
	if err != nil {
		fmt.Println(err.Error())
		t.Fatalf(err.Error())
	}
}

func TestAuth0Signup(t *testing.T) {
	t.Parallel()
	provider := getProvider(creds)

	testCases := []struct {
		name     string
		email    string
		password string
		want     error
	}{
		{"GoodSignup", creds.email, creds.password, nil},
		{"BadToken", "a", "b", errors.New("")},
	}
	t.Run("TestAuth0Signup", func(t *testing.T) {

		for _, tc := range testCases {
			fmt.Printf("Running TestAuth0Signup Test Case %s.", tc.name)
			t.Run(fmt.Sprintf("%s", tc.name), func(t *testing.T) {
				t.Parallel()
				fmt.Printf("Creating user with ID: %s, pass: %s\n", tc.email, tc.password)
				userResJson, err := provider.Register(tc.email, tc.password)
				if tc.want == nil {
					if err != nil {
						t.Errorf("Signup test failed: %s", err.Error())
					} else {
						m, _ := util.FindInJson(userResJson, []string{"_id"})
						var id string
						if id, ok := m["_id"]; ok {
							userId = id.(string)
						}
						if userId == "0" || userId == "" {
							t.Errorf("Signup test failed; unexpected user ID: " + string(id))
						}
					}
				} else if tc.want != nil && err == nil {
					t.Errorf("Signup test failed; expected error, got nil")
				}

			})
		}
	})
}

func TestAuth0Login(t *testing.T) {
	t.Parallel()
	provider := getProvider(creds)

	testCases := []struct {
		name     string
		username string
		password string
		want     int
	}{
		{"GoodLogin", creds.email, creds.password, http2.StatusOK},
		{"BadLogin", "a", "b", http2.StatusUnauthorized},
	}
	t.Run("TestAuth0Login", func(t *testing.T) {

		for _, tc := range testCases {
			fmt.Printf("Running TestAuth0Login Test Case %s.", tc.name)
			t.Run(fmt.Sprintf("%s", tc.name), func(t *testing.T) {
				t.Parallel()
				fmt.Printf("Logging in with ID: %s, pass: %s\n", tc.username, tc.password)
				res, err := provider.Login(tc.username, tc.password)
				var userRes restapi.UserRes
				err = json.Unmarshal(res, &userRes)
				if err != nil {
					t.Fatalf("Login test failed: " + err.Error())
				}
				if userRes.Httpstatus.StatusCode != tc.want {
					t.Fatalf("Login test failed; Expected %d status code, got %d", tc.want, userRes.Httpstatus.StatusCode)
				}
				if userRes.Httpstatus.StatusCode == http2.StatusOK && userRes.AccessToken == "" {
					t.Fatal("Login test failed: Access token not found")
				}
				if tc.want == http2.StatusOK {
					r, _ := http2.NewRequest("GET", "", nil)
					r.Header.Add("authorization", "Bearer "+userRes.AccessToken)
					if err = provider.Authenticate(r); err != nil {
						t.Errorf("Faied to authenticate: %s", err.Error())
					}
				}
			})
		}
	})
}

func TestAuth0FailAuthenticate(t *testing.T) {
	t.Parallel()
	provider := getProvider(creds)
	token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgF"

	r, _ := http2.NewRequest("GET", "", nil)
	r.Header.Add("authorization", "Bearer "+token)
	if err := provider.Authenticate(r); err == nil {
		t.Fatal("Auth test failed: expected error but got none")
	}

}

func TestAuth0Logout(t *testing.T) {
	fmt.Printf("Logging out user %s\n", creds.email)
	provider := getProvider(creds)
	_, err := provider.Logout(accessToken)
	if err != nil {
		t.Errorf("Logout test failed: " + err.Error())
	}
}

func getProvider(creds testCredentials) *auth0.Auth0 {
	provider := auth0.Auth0{}
	configCreds := make(map[string]interface{})

	app, err := infrastructure.Init(common.CLI)
	if err != nil {
		return nil
	}
	p, err := app.Authenticator.FindProvider("Auth0")
	if err == nil {
		return p.(*auth0.Auth0)
	}
	clientId := os.Getenv("AUTH0_CLIENTID")
	clientSecret := os.Getenv("AUTH0_CLIENTSECRET")
	configCreds["client_id"] = clientId
	configCreds["client_secret"] = clientSecret
	configCreds["domain"] = creds.domain
	provider.Configure(configCreds)
	provider.SetResToMarshalTo(&restapi.UserRes{})
	return &provider
}

func deleteTestUser() {
	provider := getProvider(creds)
	token, err := provider.GetManagementAccessToken()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	deleteUser(token)
}

func deleteUser(token string) {
	urlSuffix := fmt.Sprintf("api/v2/users/auth0|%s", userId)
	url := creds.domain + urlSuffix
	authHeader := fmt.Sprintf("Bearer %s", token)
	body, err := http.Delete(url, authHeader)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println(string(body))
}
