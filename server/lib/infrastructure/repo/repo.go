package repo

import (
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"reflect"
)

// Repo is a repository for a single model type
type Repo interface {
	NewModel() Model
	GetOne(id string, fields []string) (Model, error)
	GetMany(where string, params []interface{}, fields []string) ([]interface{}, error)
	InsertOne(item interface{}) (interface{}, error)
	UpdateOne(itemId interface{}, fields map[string]string) error
	DeleteOne(itemId interface{}) error
	GetDb() databases.Database
}

// Model represents a single in-world object in the simulation,
// the data for which is stored in a database or other storage medium.
type Model interface {
	Save() (interface{}, error)   // Create new row
	Update(fields []string) error // Update existing row
	GetId() interface{}
	SetId(interface{})
	BuildDbTagMap(t reflect.Type) map[string]string
}
