package repo

// RepoErr is a repository error
type RepoError struct {
	Err string
}

func (e *RepoError) Error() string {
	return e.Err
}
