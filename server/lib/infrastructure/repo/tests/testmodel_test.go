package tests

type TestItem struct {
	schema *TestItemSchema
	repo   *TestItemRepo
}

func (item *TestItem) Save() (err error) {
	err = item.repo.InsertOne(item.schema)
	return err
}

func (item *TestItem) Update(fields map[string]string) (err error) {
	err = item.repo.UpdateOne(item.schema, fields)
	return err
}
