package tests

import (
	"fmt"
	"github.com/fatih/structs"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"strconv"
)

type TestItemRepo struct {
	database databases.Database
	table    string
}

func NewTestItemRepo(db databases.Database) *TestItemRepo {

	repo := TestItemRepo{}
	repo.database = db
	repo.table = "test_table"
	repo.connectToDb()
	return &repo
}

func (repo *TestItemRepo) connectToDb() {
	repo.database.Connect()
}

func (repo *TestItemRepo) GetOne(id string, fields []string) (item TestItem, err error) {
	row, err := repo.database.GetRow(repo.table, id, fields)

	var schema TestItemSchema
	config := &mapstructure.DecoderConfig{
		WeaklyTypedInput: true,
		Result:           &schema,
	}

	decoder, err := mapstructure.NewDecoder(config)
	if err != nil {
		return item, err
	}

	err = decoder.Decode(row)
	if err != nil {
		return item, err
	}

	item.schema = &schema
	item.repo = repo
	return item, err
}

func (repo *TestItemRepo) GetMany(where string, params []interface{}, fields []string) (items []TestItem, err error) {
	rows, err := repo.database.GetRows(repo.table, where, params, fields)
	for _, v := range rows {
		var i TestItem
		var schema TestItemSchema

		config := &mapstructure.DecoderConfig{
			WeaklyTypedInput: true,
			Result:           &schema,
		}
		decoder, err := mapstructure.NewDecoder(config)
		if err != nil {
			return items, err
		}
		err = decoder.Decode(v)
		if err != nil {
			return items, err
		}

		i.schema = &schema
		items = append(items, i)
	}
	return items, err
}

func (repo *TestItemRepo) InsertOne(schema *TestItemSchema) (err error) {
	m := structs.Map(schema)
	m2 := make(map[string]string)
	tm := getTypeMap(schema)

	for k, v := range tm.Names {
		dbColName := k
		structVarName := v.Field.Name
		structVal := m[structVarName]
		convertedVal := fmt.Sprintf("%v", structVal)
		if structVal != nil {
			m2[dbColName] = convertedVal
		}
	}

	_, err = repo.database.Insert(repo.table, m2)
	return err
}

func (repo *TestItemRepo) UpdateOne(schema *TestItemSchema, fields map[string]string) (err error) {
	if fields == nil {
		repo.updateAllFields(schema)
	} else {
		repo.updateGivenFields(schema, fields)
	}
	return err
}

func (repo *TestItemRepo) updateAllFields(schema *TestItemSchema) (err error) {
	m := structs.Map(schema)
	m2 := make(map[string]string)

	tm := getTypeMap(schema)

	for k, v := range tm.Names {
		dbColName := k
		structVarName := v.Field.Name
		structVal := m[structVarName]
		convertedVal := fmt.Sprintf("%v", structVal)
		if structVal != nil {
			m2[dbColName] = convertedVal
		}
	}

	err = repo.database.Update(repo.table, strconv.Itoa(schema.Id), m2)
	return err
}

func (repo *TestItemRepo) updateGivenFields(schema *TestItemSchema, fields map[string]string) (err error) {
	err = repo.database.Update(repo.table, strconv.Itoa(schema.Id), fields)
	return err
}
