package env

import (
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"time"
)

var ProjectRoot string

// App is the main application instance.
type Application struct {
	ClientType    common.ClientType
	Configuration *Configuration
	Database      databases.Database
	Authenticator *auth.Authenticator
	HttpServer    *http.HttpServerWithShutdown
	Repos         map[string]repo.Repo
	quitChannels  []quitChan
	Quitting      bool
}

type quitChan struct {
	closed bool
	c      chan struct{}
}

func NewApplication() *Application {
	a := Application{
		Repos: make(map[string]repo.Repo),
	}
	a.Authenticator = auth.NewAuthenticator(a.ClientType)
	common.InitLog("")
	return &a
}

type Configuration struct {
	DBconfig   DBconfig
	AuthConfig AuthConfig
}

type DBconfig struct {
	DBType string
	DBUser string
	DBPass string
	DBHost string
	DBPort string
	DBName string
}

type AuthConfig struct {
	Providers []AuthProvider
}

type AuthProvider struct {
	Name       string
	Properties map[string]interface{}
}

func (a *Application) NewQuitChannel() chan struct{} {
	ch := make(chan struct{})
	qc := quitChan{c: ch}
	a.quitChannels = append(a.quitChannels, qc)
	return ch
}

func (a *Application) Quit() {
	a.Quitting = true
	for i := 0; i < 5; i++ {
		if a.HttpServer != nil {
			err := a.HttpServer.StartShutdown()
			error2.HandleErr(err, "Quit")
			break
		}
		if common.Log == nil {
			common.InitLog("")
		}
		msg := "Trying to shut down HttpServer, but it doesn't seem to have been started yet. Retrying in 5 seconds."
		common.Log.Info(msg)
		time.Sleep(time.Second * time.Duration(5))
	}

	for i := 0; i < len(a.quitChannels); i++ {
		c := a.quitChannels[i]
		if !c.closed {
			c.closed = true
			close(c.c)
		}
		a.quitChannels = append(a.quitChannels[:i], a.quitChannels[i+1:]...)
		i--
	}
}

func (a *Application) GetOrMakeRepo(name string, repoFunc func(databases.Database) (repo.Repo, error)) (repo.Repo, error) {
	if r, ok := a.Repos[name]; ok {
		return r, nil
	}
	if repoFunc == nil {
		return nil, errors.New("No repo func specified.")
	}
	r, err := repoFunc(a.Database)
	if r != nil && err == nil {
		if a.Repos == nil {
			a.Repos = make(map[string]repo.Repo)
		}
		a.Repos[name] = r
	}
	return r, err
}
