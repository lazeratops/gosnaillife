package infrastructure

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/common"
	jar2 "gitlab.com/drakonka/gosnaillife/common/domain/jar"
	owner2 "gitlab.com/drakonka/gosnaillife/common/domain/owner"
	snail2 "gitlab.com/drakonka/gosnaillife/common/domain/snail"
	gene2 "gitlab.com/drakonka/gosnaillife/common/domain/snail/gene"
	organ2 "gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	stable2 "gitlab.com/drakonka/gosnaillife/common/domain/stable"
	user2 "gitlab.com/drakonka/gosnaillife/common/domain/user"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	restapi2 "gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases/mysql"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi"
	"io/ioutil"
	"math/rand"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

// Init initializes the application by reading the Configuration files
// in the config folder.
func Init(clientType common.ClientType) (*env.Application, error) {
	rand.Seed(time.Now().UnixNano())
	setProjectRootPath()
	confdir := env.ProjectRoot + "/config"
	app := env.NewApplication()
	if common.Log == nil {
		common.InitLog("")
	}
	app.ClientType = clientType
	app.Configuration = &env.Configuration{}
	if err := configure(app.Configuration, confdir); err != nil {
		return nil, err
	}

	prepDb(app)
	prepAuthenticator(app)
	prepHttpServer(app)
	prepRepos(app)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt)

	go func() {
		select {
		case sig := <-c:
			fmt.Printf("Got %s signal. Aborting everything \n", sig)
			app.Quit()
		}
	}()
	return app, nil
}

func setProjectRootPath() {
	_, b, _, _ := runtime.Caller(0)
	folders := strings.Split(b, "/")
	folders = folders[:len(folders)-3]
	path := strings.Join(folders, "/")
	basepath := filepath.Dir(path) + "/server"
	env.ProjectRoot = basepath
}

func prepDb(app *env.Application) {
	dbc := &app.Configuration.DBconfig
	switch dbc.DBType {
	case "mysql":
		inTestMode := os.Getenv("SNAILLIFE_TESTMODE")
		if inTestMode == "true" && !strings.Contains(dbc.DBName, "_test") {
			dbc.DBName += "_test"
		}
		app.Database = &mysql.MySql{}
		msg := fmt.Sprintf("Configuring db on host %s, port %s", dbc.DBHost, dbc.DBPort)
		common.Log.Info(msg)
		app.Database.Configure(dbc.DBUser, dbc.DBPass, dbc.DBHost, dbc.DBPort, dbc.DBName)
		break
	}
}

func prepRepos(app *env.Application) {
	app.Repos = make(map[string]repo.Repo)
	db := app.Database
	user, _ := user2.NewRepo(db)
	app.Repos["user"] = user
	owner, _ := owner2.NewRepo(db)
	app.Repos["owner"] = owner
	stable, _ := stable2.NewRepo(db)
	app.Repos["stable"] = stable
	jar, _ := jar2.NewRepo(db)
	app.Repos["jar"] = jar
	snail, _ := snail2.NewRepo(db)
	app.Repos["snail"] = snail
	organ, _ := organ2.NewRepo(db)
	app.Repos["organ"] = organ
	gene, _ := gene2.NewRepo(db)
	app.Repos["gene"] = gene
}

func ReloadDb(app *env.Application) {
	prepDb(app)
}

func prepAuthenticator(app *env.Application) {
	authenticator := auth.NewAuthenticator(app.ClientType)
	app.Authenticator = authenticator
	authenticator.Providers = getAuthProviders(app)
}

func prepHttpServer(app *env.Application) {
	router := restapi.NewRouter(app)
	server := http.ConfigureServer(router)
	app.HttpServer = server
}

func getAuthProviders(app *env.Application) map[string]auth.Provider {
	authenticator := app.Authenticator
	providers := map[string]auth.Provider{}
	ac := app.Configuration.AuthConfig
	for _, p := range ac.Providers {
		name := strings.ToLower(p.Name)
		if name == "mysql" {
			p.Properties["database"] = app.Database
		}
		provider, err := authenticator.PrepAuthProvider(app.ClientType, name, p.Properties)
		error2.HandleErr(err, "Auth Provider Prep")
		provider.SetResToMarshalTo(&restapi2.UserRes{})
		providers[name] = provider
	}
	return providers
}

// configure reads the current environment from env.conf
// and loads the associated Configuration files.
func configure(c *env.Configuration, confdir string) (err error) {
	envname := os.Getenv("envname")
	if len(envname) == 0 {
		en, err := ioutil.ReadFile(confdir + "/env.conf")
		if err != nil {
			error2.HandleErr(err, "")
			return err
		}
		envname = string(en)
	}
	configPath := confdir + "/" + envname
	viper.AddConfigPath(configPath)
	cwd, _ := os.Getwd()
	viper.AddConfigPath(cwd)
	// db
	dbc, err := configureDB()
	c.DBconfig = dbc

	// auth
	authconfig, err := configureAuthProviders()
	c.AuthConfig = authconfig
	return err
}

// configureDB configures the database associated with this environment
// from database.json
func configureDB() (env.DBconfig, error) {
	viper.SetConfigName("database")
	err := viper.ReadInConfig()
	if err != nil {
		error2.HandleErr(err, "")
	}
	dbc := env.DBconfig{}
	err = viper.Unmarshal(&dbc)
	if err != nil {
		error2.HandleErr(err, "")
	}

	// Check if we want to override with any env vars
	envDBName := os.Getenv("SNAILLIFE_DBNAME")
	if len(envDBName) > 0 {
		dbc.DBName = envDBName
	}
	envDBHost := os.Getenv("SNAILLIFE_DBHOST")
	if len(envDBHost) > 0 {
		dbc.DBHost = envDBHost
	}
	envDBPort := os.Getenv("SNAILLIFE_DBPORT")
	if len(envDBPort) > 0 {
		dbc.DBPort = envDBPort
	}
	envDBPass := os.Getenv("SNAILLIFE_DBPASS")
	if len(envDBPass) > 0 {
		dbc.DBPass = envDBPass
	}
	envDBUser := os.Getenv("SNAILLIFE_DBUSER")
	if len(envDBUser) > 0 {
		dbc.DBUser = envDBUser
	}
	return dbc, err
}

func configureAuthProviders() (env.AuthConfig, error) {
	viper.SetConfigName("auth")
	err := viper.ReadInConfig()
	if err != nil {
		error2.HandleErr(err, "")
	}

	conf := env.AuthConfig{}
	err = viper.Unmarshal(&conf)
	if err != nil {
		error2.HandleErr(err, "")
	}
	return conf, err
}
