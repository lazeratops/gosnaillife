package main

import "fmt"
import (
	"github.com/manifoldco/promptui"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli/commands"
	"os"
)

func main() {
	fmt.Println("Welcome to SnailLife! The world is your oyster.")
	infrastructure.Init()
	if err := commands.RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	promptForCommand()
}

func promptForCommand() {
	prompt := promptui.Prompt{
		Label: ">",
	}
	result, err := prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return
	}

	cmd, err := cli.TryGetCmd(result)
	if err != nil {
		fmt.Println(err)
	} else {
		err := cmd.Execute()
		if err != nil {
			fmt.Println("ERROR: " + err.Error())
		}
	}
	promptForCommand()
}
