package cli

import (
	"errors"
	"fmt"
	"strings"
)

// AllCmds is a map of all valid client commands.
var AllCmds = make(map[string]*Cmd)

// TryGetCmd tries to get a specified commend by name.
func TryGetCmd(name string) (c *Cmd, err error) {
	fullInput := strings.TrimSpace(name)
	splitInput := strings.Split(fullInput, " ")
	name = strings.Split(fullInput, " ")[0]
	args := splitInput[1:]
	if c, ok := AllCmds[name]; ok {
		c.SetArgs(args)
		return c, err
	}
	msg := fmt.Sprintf("Command %s not found", name)
	err = errors.New(msg)
	return c, err
}
