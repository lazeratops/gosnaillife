package commands

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/common/util"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"os"
	"strings"
	"testing"
)

func TestGetOwner(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname        string
		email         string
		pass          string
		userIdToQuery string
		fieldsToGet   []string
		wantedErr     error
	}{
		{
			tcname:        "OwnerFoundWithAllFields",
			email:         "user1WithOwner@test.com",
			pass:          "testpass",
			userIdToQuery: "4",
			fieldsToGet:   []string{},
			wantedErr:     nil,
		},
		{
			tcname:        "OwnerFoundWithSomeFields",
			email:         "user1WithOwner@test.com",
			pass:          "testpass",
			userIdToQuery: "4",
			fieldsToGet:   []string{"FirstName"},
			wantedErr:     nil,
		},
		{
			tcname:        "OwnerNotFound",
			email:         "user1@test.com",
			pass:          "testpass",
			userIdToQuery: "5",
			fieldsToGet:   []string{},
			wantedErr:     new(cliErr),
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestGetOwner Test Case %s.", tc.tcname)
		t.Run(tc.tcname, func(t *testing.T) {
			var stdInFile = []string{}

			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.email)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.pass)

			fsb, err := populateNewTempFile("stdin", stdInFile...)
			if err != nil {
				t.Fatal(err)
				return
			}

			args := []string{
				"--userid",
				tc.userIdToQuery,
				"--fields",
				strings.Join(tc.fieldsToGet, " "),
			}

			getOwnerCParams.userId = tc.userIdToQuery
			getOwnerCParams.fieldsToGet = tc.fieldsToGet
			// Prep by logging in
			err = loginUser(fsb)
			if err != nil {
				t.Fatal(err)
				fsb.File.Close()
				os.Remove(fsb.File.Name())
				return
			}
			err = getOwner(fsb, args)
			testerr := testing2.TestErrorType(tc.wantedErr, err)
			if testerr != nil {
				t.Error(testing2.TCFailedMsg("OwnerTest", tc.tcname, testerr))
			}

			env.AuthenticatedUser.AuthHeaders = nil
		})
	}
}

func TestMakeOwner(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname          string
		email           string
		pass            string
		ownerFirstName  string
		ownerLastName   string
		ownerConfRes    string
		ownerFirstName2 string
		ownerLastName2  string
		ownerConfRes2   string
		err             error
	}{
		{
			tcname:          "NeedNewOwnerTryAndCancel",
			email:           "user1@test.com",
			pass:            "testpass",
			ownerFirstName:  "Registered",
			ownerLastName:   "Owner",
			ownerConfRes:    "No",
			ownerFirstName2: "COol",
			ownerLastName2:  "Ownerface",
			ownerConfRes2:   "Cancel",
			err:             new(cliErr),
		},
		{
			tcname:         "NeedNewOwnerMake",
			email:          "user1@test.com",
			pass:           "testpass",
			ownerFirstName: "Bob",
			ownerLastName:  "Thorton",
			ownerConfRes:   "Yes",
			err:            nil,
		},
		{
			tcname:         "OwnerExists",
			email:          "user1WithOwner@test.com",
			pass:           "testpass",
			ownerFirstName: "",
			ownerLastName:  "",
			err:            new(cliErr),
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestMakeOwner Test Case %s.", tc.tcname)
		t.Run(tc.tcname, func(t *testing.T) {
			var stdInFile = []string{
				tc.email,
				tc.pass,
			}

			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerFirstName)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerLastName)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerConfRes)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerFirstName2)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerLastName2)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerConfRes2)

			fsb, err := populateNewTempFile("stdin", stdInFile...)
			if err != nil {
				t.Fatal(err)
				return
			}
			// Prep by logging in
			err = loginUser(fsb)
			if err != nil {
				t.Fatal(err)
				fsb.File.Close()
				os.Remove(fsb.File.Name())
				return
			}
			err = makeOwner(fsb)
			testerr := testing2.TestErrorType(tc.err, err)
			if testerr != nil {
				t.Error(testing2.TCFailedMsg("OwnerTest", tc.tcname, testerr))

			}

			env.AuthenticatedUser.AuthHeaders = nil
			fsb.File.Close()
			os.Remove(fsb.File.Name())
		})
	}
}
