package commands

import (
	"fmt"
	"github.com/fatih/structs"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli"
	owner2 "gitlab.com/drakonka/gosnaillife/common/domain/owner"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
	http2 "net/http"
	"strings"
)

var getOwnerCobraC = &cobra.Command{
	Use:   "getowner",
	Short: "Get an owner.",
	Long:  `Get some information about an owner.`,
	Run: func(cmd *cobra.Command, args []string) {
		getOwner(nil, args)
	},
}

func initGetOwnerCobraCParams() {
	getOwnerCobraC.Flags().StringVarP(&getOwnerCParams.userId, "userid", "u", "", "ID of user whose owner to retrieve")
	getOwnerCobraC.Flags().StringVarP(&getOwnerCParams.userEmail, "useremail", "e", "", "Email of user whose owner to retrieve")
	getOwnerCobraC.Flags().StringSliceVarP(&getOwnerCParams.fieldsToGet, "fields", "f", []string{}, "Fields to retrieve (all by default)")
	cli.NewCmd(getOwnerCobraC)

}

type getOwnerCobraCParams struct {
	userId      string
	userEmail   string
	fieldsToGet []string
}

var getOwnerCParams = getOwnerCobraCParams{}

func getOwner(fsb *testing.FileScanBundle, args []string) error {
	var uId = getOwnerCParams.userId
	var uE = getOwnerCParams.userEmail
	var fields = getOwnerCParams.fieldsToGet

	if uId == "" && uE == "" {
		msg := "You must specify either the --userid or --useremail parameters to find an owner."
		fmt.Println(msg)
		return &cliErr{msg: msg}
	}
	var owner *owner2.Owner
	var err error
	if uId != "" {
		owner, err = findOwner(uId, fields...)
	} else {
		// NOT IMPLEMENTED YET
		panic("findOwner by user email is not yet implemented")
	}
	if err != nil {
		error2.HandleErr(err, "")
		return &cliErr{msg: err.Error()}
	}
	if owner == nil {
		msg := "Could not find owner."
		fmt.Println(msg)
		return &cliErr{msg: msg}
	}
	fmt.Printf("\n Found owner %s %s!", owner.FirstName, owner.LastName)
	st := structs.New(owner)
	ownerMap := st.Map()

	if len(fields) == 0 {
		for k, v := range ownerMap {
			fmt.Printf("\n%s - %v", k, v)
		}
	} else {
		for _, n := range fields {
			val := ownerMap[n]
			fmt.Printf("\n %s - %v", n, val)
		}
	}
	getOwnerCParams = getOwnerCobraCParams{}
	return nil
}

func findOwner(userId string, fields ...string) (*owner2.Owner, error) {
	surl := viper.GetString("server_url")
	f := strings.Join(fields, ",")

	url := fmt.Sprintf("%s/api/users/%s/owners?fields=%s", surl, userId, f)
	ap := viper.GetString("auth_provider")
	res, err := http.GetAuthenticatedAndGetRes(url, ap, env.AuthenticatedUser.AuthHeaders)
	owner, err := processOwnerResponse(err, res)
	if err == nil {
		if len(fields) == 0 || util.StrIndexOf(fields, "stables") > -1 {
			s, _ := findStables(owner.Id)
			if s != nil {
				owner.Stables = s
			}
		}
	}
	return owner, err
}

func processOwnerResponse(e error, res *http2.Response) (owner *owner2.Owner, err error) {
	if e != nil {
		error2.HandleErr(err, "getOwner")
		return nil, e
	}

	ownerRes, err := restapi.GetOwnerRes(res)
	if err == nil && res.StatusCode == http2.StatusOK {
		models := ownerRes.Models()
		if len(models) == 1 {
			model := models[0]
			return model.(*owner2.Owner), err
		}
	}
	error2.HandleErr(err, "getOwner")
	err = &cliErr{fmt.Sprintf("No owner was found: %v", err)}
	return owner, err
}
