package commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
)

var logoutCobraC = &cobra.Command{
	Use:   "logout",
	Short: "Log out.",
	Long:  `Log out of your SnailLife account.`,
	Run: func(cmd *cobra.Command, args []string) {
		logoutUser()
	},
}

func logoutUser() error {
	// Just client side atm, need to finish this.
	if len(env.AuthenticatedUser.AuthHeaders) == 0 {
		msg := notLoggedInMsg
		fmt.Println(msg)
		return &cliErr{msg}
	}
	env.Owner = nil
	for k := range env.AuthenticatedUser.AuthHeaders {
		delete(env.AuthenticatedUser.AuthHeaders, k)
	}

	fmt.Println("You have been logged out. Come back soon!")
	return nil
}
