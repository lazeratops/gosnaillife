package commands

import (
	"fmt"
	"github.com/fatih/structs"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
	http2 "net/http"
	"strconv"
	"strings"
)

var getSnailsCobraC = &cobra.Command{
	Use:   "getsnails",
	Short: "Get all snails in a jar",
	Long:  `Get some information about jars belonging to a jar.`,
	Run: func(cmd *cobra.Command, args []string) {
		getSnails(nil)
	},
}

func initGetSnailsCobraCParams() {
	getSnailsCobraC.Flags().StringVarP(&getSnailsCParams.jarId, "jarid", "j", "", "ID of jra whose snails to retrieve")
	getSnailsCobraC.Flags().StringSliceVarP(&getSnailsCParams.fieldsToGet, "fields", "f", []string{}, "Fields to retrieve (all by default)")
	cli.NewCmd(getSnailsCobraC)

}

type getSnailsCobraCParams struct {
	jarId       string
	stableId    string
	ownerId     string
	fieldsToGet []string
}

var getSnailsCParams = getSnailsCobraCParams{}

func getSnails(fsb *testing.FileScanBundle) error {
	var jId = getSnailsCParams.jarId
	var fields = getSnailsCParams.fieldsToGet

	if jId == "" {
		msg := "You must specify either the --jarid parameter to find snails."
		fmt.Println(msg)
		return &cliErr{msg: msg}
	}

	var snails []snail.Snail
	var err error
	id, _ := strconv.Atoi(jId)
	if jId != "" {
		snails, err = findSnailsByJarId(id, fields...)
	}
	if err != nil {
		error2.HandleErr(err, "")
		return &cliErr{msg: err.Error()}
	}
	if len(snails) == 0 {
		msg := "Could not find any snails."
		fmt.Println(msg)
		return &cliErr{msg: msg}
	}
	printSnailsInfo(snails, fields)
	return nil
}

func printSnailsInfo(snails []snail.Snail, fields []string) {
	fmt.Printf("\n Found %d snails!", len(snails))
	for _, s := range snails {
		ss := structs.New(s)
		snailMap := ss.Map()

		if len(fields) == 0 {
			for k, v := range snailMap {
				fmt.Printf("\n%s - %v", k, v)
			}
		} else {
			for _, n := range fields {
				val := snailMap[n]
				fmt.Printf("\n %s - %v", n, val)
			}
		}
	}
}

func findSnailsByJarId(jarId int, fields ...string) ([]snail.Snail, error) {
	surl := viper.GetString("server_url")
	f := strings.Join(fields, ",")

	url := fmt.Sprintf("%s/api/jars/%d/snails?fields=%s", surl, jarId, f)
	fmt.Println(url)
	ap := viper.GetString("auth_provider")

	res, err := http.GetAuthenticatedAndGetRes(url, ap, env.AuthenticatedUser.AuthHeaders)
	return processSnailResponse(err, res)
}

func processSnailResponse(e error, res *http2.Response) (snails []snail.Snail, err error) {
	if e != nil {
		error2.HandleErr(err, "getSnails")
		return nil, e
	}

	snails, err = restapi.GetSnailsFromSnailRes(res)
	return snails, err
}
