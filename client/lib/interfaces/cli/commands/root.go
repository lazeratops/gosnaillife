package commands

import (
	"github.com/spf13/cobra"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli"
)

var rootCobraC = &cobra.Command{
	Use: "snaillife",
}

// RootCmd registers the core "snaillife" command
var RootCmd = cli.NewCmd(rootCobraC)

func init() {
	cli.NewCmd(registerCobraC)
	cli.NewCmd(loginCobraC)
	cli.NewCmd(logoutCobraC)
	cli.NewCmd(makeStableCobraC)
	cli.NewCmd(authStatusCobraC)
	cli.NewCmd(makeJarCobraC)
	cli.NewCmd(makeOwnerCobraC)
	cli.NewCmd(meCobraC)
	cli.NewCmd(findSnailCobraC)
	initGetOwnerCobraCParams()
	initGetStablesCobraCParmas()
	initGetJarsCobraCParams()
	initGetSnailsCobraCParams()
}

// Execute runs the root command.
func Execute() {
	RootCmd.Execute()
}

type cliErr struct {
	msg string
}

func (e *cliErr) Error() string {
	return e.msg
}
