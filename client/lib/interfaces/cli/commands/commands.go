package commands

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
)

const (
	notLoggedInMsg = "You are not logged in."
	yesMsg         = "YES"
	noMsg          = "NO"
)

func findOrCreateOwner(fsb *testing.FileScanBundle) error {
	owner, err := findOwner(env.AuthenticatedUser.Id, "Id", "Stables", "FirstName", "LastName")
	if err != nil {
		msg := fmt.Sprintf("Error finding owner: %s", err.Error())
		fmt.Println(msg)
		// We need to prompt the user to create a new owner
		fmt.Println("You need to create a new owner!")
		owner, err = createOwner(fsb)
		if err != nil {
			error2.HandleErr(err, "MakeStable")
			return err
		}
	}
	env.Owner = owner
	return nil
}

func findOrCreateStable(fsb *testing.FileScanBundle) error {
	if env.Owner.Stables == nil || len(env.Owner.Stables) == 0 {
		stable, err := createStable(fsb)
		if err != nil {
			return err
		}
		if stable != nil {
			env.Owner.Stables = append(env.Owner.Stables, stable)
		}
	}
	return nil
}

func findOrCreateJar(fsb *testing.FileScanBundle) error {
	var hasAJar bool
	for _, stable := range env.Owner.Stables {
		if stable.Jars != nil && len(stable.Jars) > 0 {
			hasAJar = true
			break
		}
	}
	if !hasAJar {
		jar, err := createJar(fsb)
		if err != nil {
			return err
		}
		env.Owner.Stables[0].Jars = append(env.Owner.Stables[0].Jars, jar)
	}
	return nil
}
