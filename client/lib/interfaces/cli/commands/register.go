package commands

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli/inputprompts"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
	"io/ioutil"
	http2 "net/http"
	"strings"
)

var registerCobraC = &cobra.Command{
	Use:   "register",
	Short: "Create a SnailLife account.",
	Long:  `Register to own your own snail stable.`,
	Run: func(cmd *cobra.Command, args []string) {
		registerUser(nil)
	},
}

func registerUser(fsb *testing.FileScanBundle) error {
	var username, password string
	var err error
	if fsb == nil {
		username, err = inputprompts.PromptForEmail()
		if err != nil {
			fmt.Println(err)
			return err
		}
		username = strings.TrimSpace(username)
		password, err = inputprompts.PromptForPass()
		if err != nil {
			fmt.Println(err)
			return err
		}
	} else {
		scanner := fsb.Scanner
		scanner.Scan()
		username = scanner.Text()
		scanner.Scan()
		password = scanner.Text()

		if err := scanner.Err(); err != nil {
			fmt.Println("Registration failed: " + err.Error())
			return err
		}
	}
	return doRegister(username, password)

}
func doRegister(username string, password string) error {
	rule := validator.NewEmailRule(username, "Email")
	err := rule.Validate("Email")
	if err != nil {
		error2.HandleErr(err, "Registration")
		return err
	}
	uReq := restapi.UserReq{
		Username:   username,
		Password:   password,
		Token:      "",
		Connection: "Username-Password-Authentication",
	}
	uReq.AuthProvider = viper.GetString("auth_provider")
	url := viper.GetString("server_url") + "/api/users"
	j, err := json.Marshal(uReq)
	if err != nil {
		return err
	}
	postSpec := string(j)
	res, err := http.PostAndGetRes(url, postSpec)
	if err != nil {
		error2.HandleErr(err, "Registration")
		return err
	}
	return processRegistrationResponse(res, username)
}

func processRegistrationResponse(res *http2.Response, username string) error {
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}
	userResp := restapi.UserRes{}
	err = json.Unmarshal(body, &userResp)
	if err != nil {
		fmt.Println(err)
		return err
	}
	var output []string
	if res.StatusCode != http2.StatusOK {
		output = prepRegistrationFailedOutput(res, userResp)
		err = &cliErr{"Registration failed"}
	} else {
		output = prepRegistrationSucceededOutput(username)
	}
	for _, line := range output {
		fmt.Println(line)
	}
	return err
}
func prepRegistrationSucceededOutput(username string) []string {
	output := []string{
		"",
		fmt.Sprintf("Welcome to SnailLife, %s!", username),
		fmt.Sprintf("Use the 'login' command to log in and create your very own snail stable."),
	}
	return output
}

func prepRegistrationFailedOutput(res *http2.Response, userResp restapi.UserRes) []string {
	output := []string{
		"",
		"Oops, registration failed.",
		fmt.Sprintf("%d - %s",
			res.StatusCode,
			userResp.HttpStatus().Message,
		),
	}
	return output
}
