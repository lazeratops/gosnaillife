package commands

import (
	"encoding/json"
	"fmt"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli/inputprompts"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
	"reflect"
	"strconv"
)

var updateSnailCobraC = &cobra.Command{
	Use:   "updatesnail",
	Short: "Update a snail.",
	Long:  `Update an existing snail..`,
	Run: func(cmd *cobra.Command, args []string) {
		updateSnail(nil)
	},
}

func updateSnail(fsb *testing.FileScanBundle) (updatedSnail *snail.Snail, err error) {
	if len(env.AuthenticatedUser.AuthHeaders) == 0 {
		msg := notLoggedInMsg
		err := &cliErr{msg}
		error2.HandleErr(err, "")
		return nil, err
	}
	// Print out a list of all snails, prompt user for which to update
	snails := []*snail.Snail{}
	for _, stable := range env.Owner.Stables {
		for _, jar := range stable.Jars {
			for _, snail := range jar.Snails {
				snails = append(snails, &snail)
			}
		}
	}

	var selected_snail *snail.Snail
	var selection string
	var new_val string
	if fsb == nil {
		template := promptui.SelectTemplates{
			Active:   `{{ .Name | green | bold }}`,
			Inactive: `   {{ .Name }}`,
			Selected: `{{ ">" | green | bold }} {{ .Name | green | bold}}`,
			Details: `Details:
{{ .Id }}`,
		}

		idx, _, _ := inputprompts.PromptSelectionWithTemplate("Select snail to update", snails, &template)
		// Snail to edit
		selected_snail = snails[idx]

		// Select which property to edit. Loop through all properties with a db tag...
		field_names := []string{}
		tag_map := selected_snail.GetDbTagMap(reflect.TypeOf(selected_snail))
		for _, v := range tag_map {
			field_names = append(field_names, v)
		}

		selection, err = inputprompts.PromptSelection("Field to update", field_names)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}

		// Get user to input new value
		new_val, err = inputprompts.PromptForString("New value:", nil)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
	} else {
		scanner := fsb.Scanner

		scanner.Scan()
		snailIdx, _ := strconv.Atoi(scanner.Text())
		selected_snail = snails[snailIdx]
		scanner.Scan()
		selection = scanner.Text()
		scanner.Scan()
		new_val = scanner.Text()
	}

	fields_to_update := map[string]string{
		selection: new_val,
	}
	snail, err := doUpdate(selected_snail, fields_to_update, fsb)
	return snail, err
}

func doUpdate(s *snail.Snail, fields map[string]string, fsb *testing.FileScanBundle) (*snail.Snail, error) {
	surl := viper.GetString("server_url")
	url := fmt.Sprintf("%s/api/snails/%d", surl, s.Id)
	oReq := restapi.SnailReq{}
	oReq.Snail = *s
	oReq.FieldsToUpdate = fields
	oReq.AuthProvider = viper.GetString("auth_provider")

	j, err := json.Marshal(oReq)
	if err != nil {
		fmt.Printf("\n Error: %s", err)
		return nil, err
	}
	postSpec := string(j)
	ap := viper.GetString("auth_provider")

	res, err := http.PostAuthenticatedAndGetRes(url, postSpec, ap, env.AuthenticatedUser.AuthHeaders)
	if err != nil {
		error2.HandleErr(err, "Update snail")
		return nil, err
	}

	snailRes, err := restapi.GetSnailRes(res)

	if snailRes.HttpStatus().StatusCode != 200 {
		msg := fmt.Sprintf("%d - %s", snailRes.HttpStatus().StatusCode, snailRes.HttpStatus().Message)
		err = &cliErr{msg}
		return nil, err
	}

	new_snails := []*snail.Snail{}
	for _, m := range snailRes.Models() {
		snail := m.(*snail.Snail)
		new_snails = append(new_snails, snail)
	}
	if err != nil {
		return nil, err
	}
	return new_snails[0], nil

}
