package commands

import (
	"encoding/json"
	"fmt"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli/inputprompts"
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
	"strconv"
	"strings"
)

var makeJarCobraC = &cobra.Command{
	Use:   "makejar",
	Short: "Get a jar.",
	Long:  `Set up a jar to put in your stable.`,
	Run: func(cmd *cobra.Command, args []string) {
		makeJar(nil)
	},
}

func makeJar(fsb *testing.FileScanBundle) error {
	if len(env.AuthenticatedUser.AuthHeaders) == 0 {
		msg := notLoggedInMsg
		err := &cliErr{msg}
		error2.HandleErr(err, "")
		return err
	}
	if env.Owner == nil {
		err := findOrCreateOwner(fsb)
		if err != nil {
			return err
		}
	}
	err := findOrCreateStable(fsb)
	if err != nil {
		return err
	}

	jar, err := createJar(fsb)
	if jar != nil && err == nil {
		// run getjars
	}
	return err
}

func createJar(fsb *testing.FileScanBundle) (*jar.Jar, error) {
	var jarName, doContinue string

	j1 := jar.NewJar(10, 10, 10)
	j2 := jar.NewJar(20, 10, 15)
	j3 := jar.NewJar(50, 50, 25)
	jarOptions := []*jar.Jar{j1, j2, j3}
	var selectedJar *jar.Jar

	if fsb == nil {
		jarName, _ = inputprompts.PromptForString("Jar name", nil)
		msg := fmt.Sprintf("Your jar will be known as %s - do you want to continue (y/N)?", jarName)

		template := promptui.SelectTemplates{
			Active:   `{{ .SizeLabel | green | bold }}`,
			Inactive: `   {{ .SizeLabel }}`,
			Selected: `{{ ">" | green | bold }} {{ .SizeLabel | green | bold}}`,
			Details: `Details:
{{ .Ascii }}`,
		}

		idx, _, _ := inputprompts.PromptSelectionWithTemplate("Pick jar size", jarOptions, &template)
		selectedJar = jarOptions[idx]
		doContinue, _ = inputprompts.PromptSelection(msg, []string{"Yes", "No", "Cancel"})
	} else {
		scanner := fsb.Scanner

		scanner.Scan()
		jarName = scanner.Text()
		scanner.Scan()
		jarIdx, _ := strconv.Atoi(scanner.Text())
		selectedJar = jarOptions[jarIdx]
		scanner.Scan()
		doContinue = scanner.Text()

		if err := scanner.Err(); err != nil {
			fmt.Println("Jar creation failed: " + err.Error())
			return nil, err
		}
	}
	selectedJar.Name = jarName
	selectedJar.OwnerId = env.Owner.Id
	selectedJar.StableId = env.Owner.Stables[0].Id

	res := strings.ToUpper(doContinue)
	if res == yesMsg {
		return doCreateJar(selectedJar)
	} else if res == noMsg {
		return createJar(fsb)
	}
	err := &cliErr{"Jar creation cancelled by user"}
	return nil, err
}

func doCreateJar(jarToCreate *jar.Jar) (*jar.Jar, error) {
	fmt.Println("Making Jar!")
	// create owner!nil
	surl := viper.GetString("server_url")
	url := fmt.Sprintf("%s/api/jars", surl)
	oReq := restapi.JarReq{
		Jar: *jarToCreate,
	}
	oReq.AuthProvider = viper.GetString("auth_provider")

	j, err := json.Marshal(oReq)
	if err != nil {
		fmt.Printf("\n Error: %s", err)
		return jarToCreate, err
	}
	postSpec := string(j)
	ap := viper.GetString("auth_provider")

	res, err := http.PostAuthenticatedAndGetRes(url, postSpec, ap, env.AuthenticatedUser.AuthHeaders)
	if err != nil {
		error2.HandleErr(err, "Make stable [owner creation]")
		return nil, err
	}

	jarRes, err := restapi.GetJarRes(res)

	if jarRes.HttpStatus().StatusCode != 200 {
		msg := fmt.Sprintf("%d - %s", jarRes.HttpStatus().StatusCode, jarRes.HttpStatus().Message)
		err = &cliErr{msg}
		return nil, err
	}

	var createdJar *jar.Jar
	if len(jarRes.Models()) == 1 {
		createdJar = jarRes.Models()[0].(*jar.Jar)
	}
	return createdJar, err
}
