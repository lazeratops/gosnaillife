package commands

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/common/util"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"os"
	"strings"
	"testing"
)

func TestGetStables(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname      string
		email       string
		pass        string
		ownerId     string
		fieldsToGet []string
		wantedErr   error
	}{
		{
			tcname:      "GetOwnStablesWithAllFields",
			email:       "user1WithOwnerStable@test.com",
			pass:        "testpass",
			ownerId:     "2",
			fieldsToGet: []string{},
			wantedErr:   nil,
		},
		{
			tcname:      "GetOwnStablesWithName",
			email:       "user1WithOwnerStable@test.com",
			pass:        "testpass",
			ownerId:     "2",
			fieldsToGet: []string{"Name"},
			wantedErr:   nil,
		},
		{
			tcname:      "StablesNotFound",
			email:       "user1WithOwner@test.com",
			pass:        "testpass",
			ownerId:     "5",
			fieldsToGet: []string{},
			wantedErr:   new(cliErr),
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestGetStables Test Case %s.", tc.tcname)
		t.Run(tc.tcname, func(t *testing.T) {
			logoutUser()
			var stdInFile []string

			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.email)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.pass)

			fsb, err := populateNewTempFile("stdin", stdInFile...)
			if err != nil {
				t.Fatal(err)
				return
			}

			args := []string{
				"--ownerid",
				tc.ownerId,
				"--fields",
				strings.Join(tc.fieldsToGet, " "),
			}

			getStablesCParams.ownerId = tc.ownerId
			getStablesCParams.fieldsToGet = tc.fieldsToGet
			// Prep by logging in
			err = loginUser(fsb)
			if err != nil {
				t.Fatal(err)
				fsb.File.Close()
				os.Remove(fsb.File.Name())
				return
			}
			err = getStables(fsb, args)
			testerr := testing2.TestErrorType(tc.wantedErr, err)
			if testerr != nil {
				t.Error(testing2.TCFailedMsg("StableTest", tc.tcname, testerr))
			}
		})
	}
}

func TestMakeStable(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname          string
		email           string
		pass            string
		ownerFirstName  string
		ownerLastName   string
		ownerConfRes    string
		ownerFirstName2 string
		ownerLastName2  string
		ownerConfRes2   string
		stableName      string
		stableName2     string
		stableConfRes   string
		stableConfRes2  string
		err             error
	}{
		{
			tcname:          "NeedNewOwnerTryAndCancel",
			email:           "user1@test.com",
			pass:            "testpass",
			ownerFirstName:  "Kim",
			ownerLastName:   "Test",
			ownerConfRes:    "No",
			ownerFirstName2: "Krim",
			ownerLastName2:  "Testr",
			ownerConfRes2:   "Cancel",
			err:             new(cliErr),
		},
		{
			tcname:         "NeedNewOwnerMake",
			email:          "user2@test.com",
			pass:           "testpass",
			ownerFirstName: "Bob",
			ownerLastName:  "Thorton",
			ownerConfRes:   "Yes",
			stableName:     "Banjo Barn",
			stableConfRes:  "Yes",
			err:            nil,
		},
		{
			tcname:         "OwnerExistsCreateStableCancel",
			email:          "user1WithOwner@test.com",
			pass:           "testpass",
			ownerFirstName: "",
			ownerLastName:  "",
			stableName:     "Emma's Snailtoia",
			stableConfRes:  "No",
			stableName2:    "Emma's Snailtopia",
			stableConfRes2: "Cancel",
			err:            new(cliErr),
		},
		{
			tcname:         "OwnerExistsCreateStableFailValidation",
			email:          "user1WithOwner@test.com",
			pass:           "testpass",
			ownerFirstName: "",
			ownerLastName:  "",
			stableName:     "Yo",
			stableConfRes:  "Yes",
			err:            new(cliErr),
		},
		{
			tcname:         "OwnerExistsCreateStableTryTwice",
			email:          "user1WithOwner@test.com",
			pass:           "testpass",
			ownerFirstName: "",
			ownerLastName:  "",
			stableName:     "Emma's Sniltopia",
			stableConfRes:  "No",
			stableName2:    "Emma's SnailTOPIA",
			stableConfRes2: "Yes",
			err:            nil,
		},
		{
			tcname:         "OwnerAlreadyHasStable",
			email:          "user1WithOwnerStable@test.com",
			pass:           "testpass",
			ownerFirstName: "Felix",
			ownerLastName:  "K",
			stableName:     "Testest's Snailtopia",
			stableConfRes:  "Yes",
			err:            new(cliErr),
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestMakeStable Test Case %s.", tc.tcname)
		t.Run(tc.tcname, func(t *testing.T) {
			logoutUser()
			var stdInFile = []string{
				tc.email,
				tc.pass,
			}

			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerFirstName)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerLastName)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerConfRes)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerFirstName2)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerLastName2)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerConfRes2)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.stableName)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.stableConfRes)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.stableName2)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.stableConfRes2)

			fsb, err := populateNewTempFile("stdin", stdInFile...)
			if err != nil {
				t.Fatal(err)
				return
			}
			// Prep by logging in
			err = loginUser(fsb)
			if err != nil {
				t.Fatal(err)
				fsb.File.Close()
				os.Remove(fsb.File.Name())
				return
			}
			err = startStable(fsb)
			testerr := testing2.TestErrorType(tc.err, err)
			if testerr != nil {
				t.Error(testing2.TCFailedMsg("StableTest", tc.tcname, testerr))
			}

			env.AuthenticatedUser.AuthHeaders = nil
			fsb.File.Close()
			os.Remove(fsb.File.Name())
		})
	}
}
