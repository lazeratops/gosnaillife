package commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
)

var authStatusCobraC = &cobra.Command{
	Use:   "authstatus",
	Short: "Get authentication status.",
	Long:  `Check if you are authenticated.`,
	Run: func(cmd *cobra.Command, args []string) {
		getAuthStatus()
	},
}

func getAuthStatus() bool {
	if len(env.AuthenticatedUser.AuthHeaders) == 0 {
		fmt.Println("You are not logged in.")
		return false
	}

	url := viper.GetString("server_url") + "/api/session/status"
	ap := viper.GetString("auth_provider")
	res, err := http.GetAuthenticatedAndGetRes(url, ap, env.AuthenticatedUser.AuthHeaders)
	error2.HandleErr(err, "Get auth status")
	defer res.Body.Close()

	statusCode := res.StatusCode
	message := "You are logged in!"
	if statusCode != 200 {
		message = notLoggedInMsg
		return false
	}

	msg := fmt.Sprintf("%d - %s", res.StatusCode, message)
	fmt.Println(msg)
	return true
}
