-- +goose Up
INSERT INTO users VALUES
  (2, 'user1WithOwnerStableJar@test.com', '$2a$10$zYiktNu4zc.GuqipUqXvpevUyK0n1OYLUAHVMat1BxNH1H3i67H6a', NULL, NOW(), 0),
  (3, 'user1WithOwnerStable@test.com', '$2a$10$zYiktNu4zc.GuqipUqXvpevUyK0n1OYLUAHVMat1BxNH1H3i67H6a', NULL, NOW(), 0),
  (4, 'user1WithOwner@test.com', '$2a$10$zYiktNu4zc.GuqipUqXvpevUyK0n1OYLUAHVMat1BxNH1H3i67H6a', NULL, NOW(), 0),
  (5, 'user1@test.com', '$2a$10$zYiktNu4zc.GuqipUqXvpevUyK0n1OYLUAHVMat1BxNH1H3i67H6a', NULL, NOW(), 0),
  (6, 'user2WithOwnerStableJar@test.com', '$2a$10$zYiktNu4zc.GuqipUqXvpevUyK0n1OYLUAHVMat1BxNH1H3i67H6a', NULL, NOW(), 0),
  (7, 'user2WithOwnerStable@test.com', '$2a$10$zYiktNu4zc.GuqipUqXvpevUyK0n1OYLUAHVMat1BxNH1H3i67H6a', NULL, NOW(), 0),
  (8, 'user2WithOwner@test.com', '$2a$10$zYiktNu4zc.GuqipUqXvpevUyK0n1OYLUAHVMat1BxNH1H3i67H6a', NULL, NOW(), 0),
  (9, 'user2@test.com', '$2a$10$zYiktNu4zc.GuqipUqXvpevUyK0n1OYLUAHVMat1BxNH1H3i67H6a', NULL, NOW(), 0),
  (10, 'user3@test.com', '$2a$10$zYiktNu4zc.GuqipUqXvpevUyK0n1OYLUAHVMat1BxNH1H3i67H6a', NULL, NOW(), 0),
  (11, 'user3WithOwnerStableJarSnail@test.com', '$2a$10$zYiktNu4zc.GuqipUqXvpevUyK0n1OYLUAHVMat1BxNH1H3i67H6a', NULL, NOW(), 0);
