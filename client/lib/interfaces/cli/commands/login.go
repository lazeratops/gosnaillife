package commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli/inputprompts"
	user2 "gitlab.com/drakonka/gosnaillife/clientlib/user"
	user3 "gitlab.com/drakonka/gosnaillife/common/domain/user"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
	http2 "net/http"
)

var loginCobraC = &cobra.Command{
	Use:   "login",
	Short: "Log in to your SnailLife account.",
	Long:  `Log in to manage your snail stable.`,
	Run: func(cmd *cobra.Command, args []string) {
		loginUser(nil)
	},
}

func loginUser(fsb *testing.FileScanBundle) error {
	if len(env.AuthenticatedUser.AuthHeaders) > 0 {
		msg := fmt.Sprintf("You are already logged in as %s(%s)", env.AuthenticatedUser.Email, env.AuthenticatedUser.Id)
		fmt.Println(msg)
		return &cliErr{msg}
	}

	var username, password string
	var err error
	if fsb == nil {
		username, err = inputprompts.PromptForEmail()
		if err != nil {
			fmt.Println("Login failed: " + err.Error())
			return err
		}
		password, err = inputprompts.PromptForPass()
		if err != nil {
			fmt.Println("Login failed: " + err.Error())
			return err
		}
	} else {
		scanner := fsb.Scanner

		scanner.Scan()
		username = scanner.Text()
		scanner.Scan()
		password = scanner.Text()
	}

	return doLogin(username, password)
}

func doLogin(username string, password string) error {
	authProvider := viper.GetString("auth_provider")
	url := viper.GetString("server_url")
	res, err := user2.Login(url, authProvider, username, password)
	err = processLoginResponse(err, res, username)
	if err == nil {
		o, _ := findOwner(env.AuthenticatedUser.Id)
		if o != nil {
			env.Owner = o
		}
	}
	return err
}

func processLoginResponse(err error, res *http2.Response, username string) error {
	var output []string
	if err != nil {
		output = append(output, "Login Error: "+err.Error())
	} else if res == nil {
		output = append(output, "Login Error: Did not get a response from the server")
	} else {
		defer res.Body.Close()
		var user *user3.User
		user, err = restapi.GetUserFromUserRes(res)
		if err != nil {
			output = prepLoginFailedOutput(res)
			err = &cliErr{err.Error()}
		} else {
			env.AuthenticatedUser = *user
			output = prepLoginSucceededOutput(username)
		}
	}

	for _, line := range output {
		fmt.Println(line)
	}
	return err
}

func prepLoginSucceededOutput(username string) []string {
	output := []string{
		"",
		fmt.Sprintf("Welcome to SnailLife, %s!", username),
		fmt.Sprintf("Further commands here..."),
	}
	return output
}

func prepLoginFailedOutput(res *http2.Response) []string {
	output := []string{
		"",
		"Login Error",
		"Oops, login failed.",
		fmt.Sprintf("%d - %s",
			res.StatusCode,
			res.Body,
		),
	}
	return output
}
