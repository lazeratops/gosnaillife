package commands

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli/inputprompts"
	"gitlab.com/drakonka/gosnaillife/common/domain/owner"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
	"strings"
)

var makeOwnerCobraC = &cobra.Command{
	Use:   "makeowner",
	Short: "Make an owner.",
	Long:  `Become an official owner of snails.`,
	Run: func(cmd *cobra.Command, args []string) {
		makeOwner(nil)
	},
}

func makeOwner(fsb *testing.FileScanBundle) error {
	if len(env.AuthenticatedUser.AuthHeaders) == 0 {
		msg := notLoggedInMsg
		err := &cliErr{msg}
		error2.HandleErr(err, "")
		return err
	}
	owner, err := findOwner(env.AuthenticatedUser.Id, "firstname", "lastname")
	if err == nil && owner != nil {
		msg := fmt.Sprintf("\nYou are already a snail owner, %s %s.", owner.FirstName, owner.LastName)
		fmt.Println(msg)
		return &cliErr{msg: msg}
	}
	_, err = createOwner(fsb)
	return err
}

func createOwner(fsb *testing.FileScanBundle) (*owner.Owner, error) {
	fmt.Println("Pick a name! It can be made up.")
	var firstName, lastName, doContinue string
	if fsb == nil {
		firstName, _ = inputprompts.PromptForString("First name", nil)
		lastName, _ = inputprompts.PromptForString("Last name", nil)
		msg := fmt.Sprintf("Your owner will be known as %s %s - do you want to continue (y/N)?", firstName, lastName)
		doContinue, _ = inputprompts.PromptSelection(msg, []string{"Yes", "No", "Cancel"})
	} else {
		scanner := fsb.Scanner

		scanner.Scan()
		firstName = scanner.Text()
		scanner.Scan()
		lastName = scanner.Text()
		scanner.Scan()
		doContinue = scanner.Text()

		if err := scanner.Err(); err != nil {
			fmt.Println("Owner creation failed: " + err.Error())
			return nil, err
		}
	}

	res := strings.ToUpper(doContinue)
	if res == yesMsg {
		return doCreateOwner(firstName, lastName)
	} else if res == noMsg {
		return createOwner(fsb)
	}
	err := &cliErr{"Owner creation cancelled by user"}
	return nil, err
}

func doCreateOwner(firstname, lastname string) (*owner.Owner, error) {
	o := owner.Owner{}
	o.UserId = env.AuthenticatedUser.Id
	o.FirstName = strings.TrimSpace(firstname)
	o.LastName = strings.TrimSpace(lastname)
	fmt.Println("Making owner!")
	// create owner!nil
	surl := viper.GetString("server_url")
	url := fmt.Sprintf("%s/api/owners", surl)
	oReq := restapi.OwnerReq{
		Owner: o,
	}
	oReq.AuthProvider = viper.GetString("auth_provider")

	j, err := json.Marshal(oReq)
	if err != nil {
		fmt.Printf("\n Error: %s", err)
		return &o, err
	}
	postSpec := string(j)
	ap := viper.GetString("auth_provider")

	res, err := http.PostAuthenticatedAndGetRes(url, postSpec, ap, env.AuthenticatedUser.AuthHeaders)
	if err != nil {
		error2.HandleErr(err, "Make stable [owner creation]")
		return nil, err
	} else if res.StatusCode != 200 {
		msg := fmt.Sprintf("%d - %s", res.StatusCode, res.Status)
		err = &cliErr{msg}
		return nil, err
	}

	ownerRes, err := restapi.GetOwnerRes(res)

	var createdOwner *owner.Owner
	if len(ownerRes.Models()) == 1 {
		createdOwner = ownerRes.Models()[0].(*owner.Owner)
	}
	return createdOwner, err
}
