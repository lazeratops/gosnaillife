package tests

import (
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli/output"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	"testing"
)

// This will basically just make sure we don't panic, no guarantee of correct output atm
func TestPrintCompleteSnailStats(t *testing.T) {

	testCases := []struct {
		name         string
		snailToPrint *snail.Snail
	}{
		{
			name:         "PrintRandomlyGeneratedSnail",
			snailToPrint: getRandomSnail(t),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			output.PrintCompleteSnailStats(*tc.snailToPrint)
		})
	}
}

func getRandomSnail(t *testing.T) *snail.Snail {
	s, err := snail.GenerateRandomSnail(0, nil)
	if err != nil {
		t.Fatal(t)
	}
	return s
}
