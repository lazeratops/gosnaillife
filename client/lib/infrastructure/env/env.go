package env

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/owner"
	"gitlab.com/drakonka/gosnaillife/common/domain/user"
)

var AuthenticatedUser user.User
var Owner *owner.Owner
