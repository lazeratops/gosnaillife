package infrastructure

import (
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/common"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

func Init() {
	common.InitLog("")
	configureClient()
}
func configureClient() {
	projectRoot := getProjectRootPath()
	confPath := projectRoot + "/config"
	envFile := os.Getenv("envname")
	var err error
	if len(envFile) == 0 {
		var env []byte
		env, err = ioutil.ReadFile(confPath + "/env.conf")
		envFile = string(env)
	}
	if err != nil {
		error2.HandleErr(err, "")
		return
	}
	configPath := confPath + "/" + envFile

	viper.AddConfigPath(configPath)

	// Config client
	viper.SetConfigName("server")
	err = viper.ReadInConfig()
	if err != nil {
		error2.HandleErr(err, "")
	}
}

func getProjectRootPath() string {
	_, b, _, _ := runtime.Caller(0)
	folders := strings.Split(b, "/")
	folders = folders[:len(folders)-3]
	path := strings.Join(folders, "/")
	basepath := filepath.Dir(path) + "/client"
	return basepath
}
