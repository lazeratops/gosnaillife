module gitlab.com/drakonka/gosnaillife

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/auth0-community/go-auth0 v1.0.0
	github.com/chzyer/logex v1.1.10 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/chzyer/test v0.0.0-20180213035817-a1ea475d72b1 // indirect
	github.com/fatih/structs v1.0.0
	github.com/go-errors/errors v1.0.1
	github.com/go-sql-driver/mysql v1.4.0
	github.com/golang/mock v1.4.3
	github.com/google/go-cmp v0.2.0
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jmoiron/sqlx v0.0.0-20180614180643-0dae4fefe7c0
	github.com/juju/ansiterm v0.0.0-20180109212912-720a0952cc2a // indirect
	github.com/lib/pq v1.0.0 // indirect
	github.com/lunixbochs/vtclean v0.0.0-20180621232353-2d01aacdc34a // indirect
	github.com/manifoldco/promptui v0.3.1
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mattn/go-sqlite3 v1.9.0 // indirect
	github.com/mitchellh/mapstructure v1.0.0
	github.com/onsi/gomega v1.4.2 // indirect
	github.com/petergtz/pegomock v2.8.0+incompatible
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pressly/goose v2.3.0+incompatible
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.2
	github.com/spf13/viper v1.2.1
	github.com/stretchr/testify v1.6.1 // indirect
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	golang.org/x/text v0.3.1-0.20180807135948-17ff2d5776d2 // indirect
	google.golang.org/appengine v1.2.0 // indirect
	gopkg.in/square/go-jose.v2 v2.1.9
)

go 1.15
