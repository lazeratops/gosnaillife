package user

import (
	"encoding/json"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"net/http"
)

func Login(serverUrl, authProvider, username, password string) (res *http.Response, err error) {
	postSpec, err := prepUserReq(authProvider, username, password)
	if err != nil {
		return nil, err
	}
	url := serverUrl + "/api/session"
	res, err = http2.PostAndGetRes(url, postSpec)
	return res, err
}

func prepUserReq(authProvider, username, password string) (string, error) {
	uReq := restapi.UserReq{
		Username:   username,
		Password:   password,
		Connection: "Username-Password-Authentication",
	}
	uReq.AuthProvider = authProvider
	reqjson, err := json.Marshal(uReq)
	if err != nil {
		return "", err
	}
	j := string(reqjson)
	return j, err
}
