-- +goose Up
DROP TABLE genes;

-- +goose Down
CREATE TABLE genes (
  gene_id int UNSIGNED NOT NULL PRIMARY KEY,
  gene_type_id int UNSIGNED NOT NULL,
  snail_id int UNSIGNED NOT NULL,
  allele1 varchar(1),
  allele2 varchar(1),
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW()
);

