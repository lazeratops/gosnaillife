-- +goose Up
ALTER TABLE owners DROP COLUMN stable_id;

-- +goose Down
ALTER TABLE owners ADD COLUMN stable_id int UNSIGNED;
