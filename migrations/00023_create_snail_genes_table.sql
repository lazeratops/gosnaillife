-- +goose Up
CREATE TABLE snail_genes (
  allele_id int UNSIGNED NOT NULL PRIMARY KEY,
  gene_id int UNSIGNED NOT NULL,
  owner_id int UNSIGNED NOT NULL,
  allele_key varchar(1) NOT NULL,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW()
);

-- +goose Down
DROP TABLE snail_genes