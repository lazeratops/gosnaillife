-- +goose Up
ALTER TABLE organs CHANGE COLUMN current_efficienty_perc current_efficiency_perc tinyint UNSIGNED DEFAULT 100;

-- +goose Down
ALTER TABLE organs CHANGE COLUMN current_efficiency_perc current_efficienty_perc tinyint UNSIGNED DEFAULT 100;

