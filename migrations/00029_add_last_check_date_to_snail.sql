-- +goose Up
ALTER TABLE snails ADD COLUMN last_check_date TIMESTAMP DEFAULT NOW();


-- +goose Down
ALTER TABLE snails DROP COLUMN last_check_date;
