-- +goose Up
ALTER TABLE organs ADD COLUMN last_check_date TIMESTAMP DEFAULT NOW();


-- +goose Down
ALTER TABLE organs DROP COLUMN last_check_date;
