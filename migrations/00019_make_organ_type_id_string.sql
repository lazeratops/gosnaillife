-- +goose Up
ALTER TABLE organs MODIFY COLUMN organ_type_id varchar(25);

-- +goose Down
ALTER TABLE organs MODIFY COLUMN organ_type_id int(10);

