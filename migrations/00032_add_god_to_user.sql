-- +goose Up
ALTER TABLE users ADD COLUMN god BOOL DEFAULT FALSE;


-- +goose Down
ALTER TABLE users DROP COLUMN god;
