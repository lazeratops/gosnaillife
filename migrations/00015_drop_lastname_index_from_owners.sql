-- +goose Up
ALTER TABLE owners DROP INDEX lastname;

-- +goose Down
ALTER TABLE owners ADD UNIQUE lastname(`lastname`)
