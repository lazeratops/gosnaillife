-- +goose Up
ALTER TABLE owners MODIFY COLUMN firstname varchar(20);
ALTER TABLE owners MODIFY COLUMN lastname varchar(20);

-- +goose Down
ALTER TABLE owners MODIFY COLUMN firstname varchar(10);
ALTER TABLE owners MODIFY COLUMN lastname varchar(10);

