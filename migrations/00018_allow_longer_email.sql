-- +goose Up
ALTER TABLE users MODIFY COLUMN email varchar(70);

-- +goose Down
ALTER TABLE users MODIFY COLUMN email varchar(30);

