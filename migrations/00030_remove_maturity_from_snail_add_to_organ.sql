-- +goose Up
ALTER TABLE snails DROP COLUMN maturity_rate;
ALTER TABLE snails DROP COLUMN current_maturity;
ALTER TABLE organs ADD COLUMN maturity_rate FLOAT;
ALTER TABLE organs ADD COLUMN current_maturity FLOAT;


-- +goose Down
ALTER TABLE snails ADD COLUMN maturity_rate FLOAT;
ALTER TABLE snails ADD COLUMN current_maturity FLOAT;
