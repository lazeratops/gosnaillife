-- +goose Up
CREATE TABLE users (
  _id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  email varchar(30) NOT NULL UNIQUE,
  password varchar(150) NOT NULL,
  token varchar(100),
  created_at TIMESTAMP DEFAULT NOW()
);

-- +goose Down
DROP TABLE users;
